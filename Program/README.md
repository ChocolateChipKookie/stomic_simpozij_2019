# stomic_simpozij_2019

A genetic algorithm for assigning lectures to participants in the symposium.
Made for Zagreb dental medicine symposium organized by students of the "Stomatološki fakultet Zagreb"
Made by Adi Čaušević

So, how it works

Firstly, it has to have a config.txt file in the same directory.
The file may be empty, because there default values hardcoded, but the absence of the file will cause a crash of the program.
There is a backup config.txt file in the resources file, in case you lose the original one

In the config file there are values that define the working of the algorithm

In the config file you can write anything inbetween the lines of the variables, except the = sign
If the line has = in it, it is consideres a value line, and it will be attempted to be parsed
Space between the variable name, =, and the value are not allowed

Input/Output:
	It has to have 2 input files which are by default 
	courses_input=resources/radionice.txt
	participants_input=resources/sudionici.txt

	And one output file, which represents the folder in which the results will be saved
	output_file=resources/results/
	
The other variables are used for the genetic algorithm, and should be easy to understand if you look at the code
Because of the primitive parser, the same variable can be written to several times, but only the last one counts