
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Danica Cvjetkovic
		Ivna Ćavar
		Klaudija Kaurić
		Mihaela Budimir
		Mladen Petkovic
		Nika Fantulin
		Rasim Trnjanin
		Tarik Tahirovic
		Tina Matoc
		lucija hero
	Petak u 15:30 - 17:0 (limit=10):
		Antonija Jug
		Božena Sorić
		Dunja Ilić
		Ivan Frka Šeparović
		Karlo Posavec
		Karlo Počanić
		Katarina Pagadur
		Martina Besek
		Nikolina Mohorovic
		Sara Jakoliš

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Andrej Škerjanc
		Antonela Kirin
		Dora Gobin
		Fran Glanz
		Isabel Pavlović
		Ivana Pehar
		Karlo Vrbanić
		Lucia Levar
		Marta Furdi
		Marta Posavec
		Nika Horvat
		Saira Omanovic
		Tina Matoc
	Petak u 18:30 - 20:0 (limit=13):
		Alba Elezović
		Ela Tomljanović
		Erika Milovec
		Iva Barišić
		Ivan Orabović
		Karla Bikić
		Magdalena Prskalo
		Marija Mastelić
		Marta Adam
		Mia Škaljac
		Monika Vukić
		Paula Bašić
		blanka dermit

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Adna Vlahovljak
		Ana Čeko
		Dejana Komlen
		Denis Abdihodžić
		Edina Memiši
		Karla Ugrin
		Klara Gabriela Penić
		Lea Grzela
		Lucija Šarić
		Luka Paunović
		Matea Vidov
		Mihovil Škalic
		Milenko Paponja
		Moorea Kuvačić
		Nera Gržetić
		Saira Omanovic

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Ana Aračić
		Andrea Vrankić
		Dora Arhanić
		Ena Keser
		Katja Lukić
		Lea Imamović
		Lea Patekar
		Lucija Rakocija
		Matea Bubalo
		Maša Matić
		Mia Škaljac
		Mirna Radočaj
		Nika Fantulin
		Paula Petrašić
		Zinajda Šabić
	Petak u 18:0 - 19:0 (limit=15):
		Alessia Bučić
		Anamarija Vidovic
		Danica Cvjetkovic
		Dora Visković
		Dora istenić
		Ena Alagic
		Jana Joksimović
		Jelena Đapić
		Katarina Grcic
		Katarina Lazar
		LUCIJA RAKO
		Lucija Ratkovski
		Nika Supina
		Sara Jindra
		lucija hero
	Petak u 19:0 - 20:0 (limit=14):
		Ana Radan
		Anđela Batur
		Božena Sorić
		Ena Kulis
		Harun Mašić
		Inga Jukanović
		Ivona Barić
		Jurica Nekić
		Luka Begonja
		Matea Dželalija
		Matea Vučetić
		Mihaela Budimir
		Petra Mihalić
		Sara Kožul

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Ajla Bašić
		Ana Domitrović
		Anamaria Mazarekić
		Barbara Čulin
		Ida Kuštelega
		Jure trogrlic
		Karlo Posavec
		Marija Mastelić
		Marija Matijašević
		Mario Gržančić
		Maša Matić
		Nejra Kubat
		Ninib malki
		Viktorija Perčinlić
		Zrinka Jemrić
	Petak u 15:30 - 17:0 (limit=14):
		Ana Brajdić
		Anja Ljevar
		Dora Alagic
		Doris Šimac
		Ela Tomljanović
		Erika Milovec
		Frano Visković
		Iva Pavičić
		Jana Joksimović
		Karla Bikić
		Karla Jurković
		Katarina Lasan
		Mihaela Rac
		Petar Ivaniš

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Admira Selimović
		Alba Elezović
		Božana Knežević
		Ela Prajz
		Ena Keser
		Eva Rožman
		Filip Kašner
		Iva Ćorluka
		Josip Orešković
		Lucija Herceg
		Lucija Rakocija
		Marko Ivančević
		Monika Dragić
		Nikola Matijevic
		Zinajda Šabić
	Petak u 15:30 - 17:0 (limit=14):
		Ana Božinović
		Ana Maria Dekanić
		Ana Perica
		Domen Tomič
		Filip Kliček
		Karla Miškić
		Marija Dolić
		Marina Gadža
		Nedžad Osmanović
		Rebecca Beissmann
		Renata Volf
		Tadeja Blagec
		Teja Trontelj
		Zorana Mavija
	Subota u 14:0 - 15:30 (limit=14):
		Amra Mahmutović
		Danijela Sabljo
		Domagoj Ivančić
		Harun Mašić
		Karla Jurković
		Katja Kovačević
		Liza Uršič
		Luciia Ribić
		Matea Dželalija
		Nikolina Mohorovic
		Paula Bašić
		Sara Krog
		Stanija Jovanovic
		Urška Štajdohar
	Subota u 15:30 - 17:0 (limit=15):
		Ajla Mustafić
		Antonija Jokić
		Darko Đukić
		Dora Habek
		Elizabeta Zorić
		Helena Vidović
		Iva Kordić
		Karolina Boljkovac
		Katja Lukić
		Lejla Reizbegović
		Mona Maarouf
		Nina Vovk
		Petra Lukač
		Stipe Ivanda
		Valentina Marasovic

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Ivan Orabović
		Katarina Lelas
		Klara Gabriela Penić
		Lejla Reizbegović
		Luka Paunović
		Margareta Strihic
		Marta Novak
		Mateja Novački
		Monika Vukić
		Nikolina Klarić
		Stanija Jovanovic
		Stipe Ivanda
	Petak u 18:0 - 19:0 (limit=16):
		Andrija Segin
		Elizabeta Zorić
		Grgo Orlović
		Marija Dolić
		Marija Razum
		Tadeja Blagec
		Valentina Šalković
	Petak u 19:0 - 20:0 (limit=16):
		Magdalena Udovičić
		Marta Posavec

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Ana Aračić
		Bozana corluka
		Darko Đukić
		Ena Kulis
		Lucija Ivanac
		Luka Begonja
		Margareta Strihic
		Marija Buotić
		Marta Adam
		Nika Kovačević
		Sabra Čolić
		Sara Krog
	Petak u 15:30 - 17:0 (limit=14):
		Amila Balić
		Ana Istenić
		Andrija Segin
		Antonia Milas
		Debora Kanižaj
		Dorotea Bralo
		Eva Krizmanić
		Fabio Pažin
		Ida Kuštelega
		Luciia Ribić
		Matija Žanetić
		Mirna Draganja
		Mona Maarouf
		Nora Salkičević
	Petak u 17:0 - 18:30 (limit=9):
		Ana Jukić
		Edina Memiši
		Eva Rožman
		Filip Kašner
		Iva Skvorcov
		Jure trogrlic
		Klara Naka
		Magdalena Udovičić
		Moorea Kuvačić

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Branimir Ninčević
		Elis Mikac
		Kristina Filipović
		Marina Jardas
		Matea Vučetić
		Nikolina Nazor
		Tijana Botić
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Antea Cikojevic
		Antonija Jerković
		Darko Tevanovic
		Karla Roksandić
		Laura Plančak
		Mirna Radočaj
	Petak u 15:30 - 17:0 (limit=6):
		Anamarija Vidovic
		Grgo Orlović
		Katarina Grcic
		Tessa Anastasja Stankic
		Viktorija Perčinlić
		blanka dermit

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ana Čeko
		Dejan Prpos
		Denis Abdihodžić
		Elia Croner
		Fran Glanz
		Josip Orešković
		Marta Furdi
	Subota u 15:30 - 17:0 (limit=8):
		Domen Tomič
		Iva Pavičić
		Katarina Lazar
		Klaudija Kaurić
		Matea Bubalo
		Matea Vidov
		Mihaela Rac
		Tiffany Ocko

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Anđela Batur
		Dejan Prpos
		Emina Dzaferović
		Lucija Kuštra
		Marta Smok
		Mia Maretić
		Nikolina Akmadža
		Petra Lukač
		Petra Mihalić
		Stela Cerovac
	Petak u 18:0 - 19:0 (limit=10):
		Ana Perica
		Antea Cikojevic
		Anđelika Vučković
		Barbara Čulin
		Dora Alagic
		Dora Habek
		Marta Leović
		Nejra Šapčanin
		Nikolina Dernaj
		Urška Štajdohar
	Petak u 19:0 - 20:0 (limit=12):
		Ana Pavlović
		Anja Antea Sokač
		Asja Jerlagic
		Branimir Ninčević
		Daria Vuk
		Ena Popović
		Lucia Levar
		Marta Novak
		Matea Čupen
		Petar Ivaniš
		Tessa Anastasja Stankic
		Tiffany Ocko
	Subota u 17:0 - 18:0 (limit=12):
		Ante Zelenika
		Ena Roca
		Kristina Filipović
		Mirna Draganja
		Nikolina Nazor
		Paula Vršić
	Subota u 18:0 - 19:0 (limit=10):
	Subota u 19:0 - 20:0 (limit=10):
		Svetlana Galić

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Antonija Jokić
		Ivona Barić
		LUCIJA RAKO
		Lucija Kovačić
		Marija Razum
		Marijeta Dadić
		Marta Leović
		Milan Tejic
		Ninib malki
		Tina Robič
		Valentina Šalković
		Vladimir Šavija

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Brigić
		Ena Popović
		Iva Barišić
		Iva Kordić
		Ivana Čavka
		Karla Roksandić
		Karolina Boljkovac
		Laura Plančak
		Matea Čupen
		Paula Vršić
		Sara El-Sabeh
		Stela Salopek
		Valentina Marasovic

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Dora Arhanić
		Dunja Ilić
		Ela Prajz
		Jelena Đapić
		Lucija Kuštra
		Martina Besek
		Monika Dragić
		Sara El-Sabeh
		Višnja Šljivac

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Adna Vlahovljak
		Andrea Vrankić
		Daria Vuk
		Darko Tevanovic
		Jurica Nekić
		Lea Grzela
		Lea Patekar
		Lucija Šarić
		Marija Matijašević
		Nera Gržetić
		Paula Petrašić
	Subota u 17:0 - 18:30 (limit=11):
		Alessia Bučić
		Anja Antea Sokač
		Antonia Milas
		Iva Ćorluka
		Marina Gadža
		Matija Žanetić
		Mihovil Škalic
		Milan Tejic
		Nikolina Klarić
		Sara Jindra
		Tarik Tahirovic
	Subota u 18:30 - 20:0 (limit=10):
		Andreas Ivanovic
		Bozana corluka
		Dora Visković
		Doris Šimac
		Filip Kliček
		Inga Jukanović
		Katarina Lasan
		Katja Kovačević
		Sara Kožul
		Teja Trontelj

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Ana Božinović
		Andreas Ivanovic
		Antonia Vranjković
		Dora Gobin
		Ena Roca
		Frano Visković
		Helena Buljubašić
		Iva Skvorcov
		Mia Maretić
		Nika Kovačević
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Admira Selimović
		Ajla Brigić
		Anja Ljevar
		Asja Jerlagic
		Ivna Ćavar
		Katarina Lelas
		Katarina Pagadur
		Marija Buotić
		Mihaela Hercigonja
		Nika Supina
		Rasim Trnjanin
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Mustafić
		Andrej Škerjanc
		Dora istenić
		Dorotea Bralo
		Emina Dzaferović
		Eva Krizmanić
		Helena Buljubašić
		Helena Vidović
		Lucija Herceg
		Nikolina Dernaj
		Nina Vovk
		Tijana Botić
		Vladimir Šavija

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amra Mahmutović
		Antonia Vranjković
		Ines Perić
		Ivana Pehar
		Klara Naka
		Lucija Ivanac
		Lucija Kovačić
		Magdalena Prskalo
		Svetlana Galić
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Adis Trešnjo
		Ana Jukić
		Ana Maria Dekanić
		Antonela Kirin
		Fran Popić
		Ines Perić
		Ivana Čavka
		Jasmina Alendar
		Karla Sabolić
		Karlo Vrbanić
		Marija Kelić
		Nika Horvat
		Nikolina Akmadža
		Renata Volf
		Rene Križ
		Tina Robič
	Subota u 15:30 - 17:0 (limit=16):
		Anđelika Vučković
		Ena Alagic
		Fabio Pažin
		Marija Oreč
		Marijeta Dadić
		Marko Ivančević
	Subota u 17:0 - 18:30 (limit=16):
		Danijela Sabljo
		Josipa Rokov

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ante Zelenika
		Fran Popić
		Iva Plazonić
		Karla Ugrin
	Petak u 18:30 - 20:0 (limit=14):
		Milenko Paponja
		Stela Salopek

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Ajla Bašić
		Anamaria Mazarekić
		Karla Miškić
		Mihaela Hercigonja
		Sara Jakoliš
	Subota u 15:30 - 17:0 (limit=6):
		Ana Domitrović
		Božana Knežević
		Mario Gržančić
		Nedžad Osmanović
		Nejra Kubat
		Zrinka Jemrić

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Ana Barać
		Barbara Marušić
		Josipa Rokov
		Lea Imamović
		Lucija Ratkovski
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Amila Balić
		Barbara Marušić
		Barbara Ovčarić
		Debora Kanižaj
		Dejana Komlen
		Isabel Pavlović
		Iva Plazonić
		Ivan Frka Šeparović
		Karla Sabolić
		Marija Kelić
		Mladen Petkovic
		Nora Salkičević
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Brajdić
		Ana Istenić
		Domagoj Ivančić
		Elia Croner
		Elis Mikac
		Jasmina Alendar
		Magdalena Jurisic
		Marina Jardas
	Subota u 19:0 - 20:0 (limit=10):
		Ana Barać
		Antonija Jerković
		Antonija Jug
		Barbara Ovčarić
		Karlo Počanić
		Liza Uršič
		Marija Oreč
		Mateja Novački
		Rene Križ
		Stela Cerovac
