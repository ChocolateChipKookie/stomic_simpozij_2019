
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Antonija Jug
		Božena Sorić
		Dora istenić
		Dorotea Bralo
		Ena Alagic
		Fabio Pažin
		Katarina Pagadur
		Mihaela Budimir
		Rasim Trnjanin
		Urška Štajdohar
	Petak u 15:30 - 17:0 (limit=10):
		Ana Istenić
		Jurica Nekić
		Karla Ugrin
		Karlo Vrbanić
		Klaudija Kaurić
		Marija Oreč
		Mario Gržančić
		Maša Matić
		Tarik Tahirovic
		Vladimir Šavija

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Andrej Škerjanc
		Antonija Jug
		Debora Kanižaj
		Lucia Levar
		Magdalena Prskalo
		Margareta Strihic
		Marta Adam
		Marta Furdi
		Marta Posavec
		Matea Vidov
		Rene Križ
		Tina Matoc
		blanka dermit
	Petak u 18:30 - 20:0 (limit=13):
		Dora Alagic
		Filip Kašner
		Fran Glanz
		Jelena Đapić
		Karlo Vrbanić
		Klaudija Kaurić
		Marija Kelić
		Mia Škaljac
		Monika Vukić
		Ninib malki
		Paula Bašić
		Sabra Čolić
		Stanija Jovanovic

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Amila Balić
		Ana Barać
		Ana Jukić
		Ana Pavlović
		Ana Čeko
		Dejana Komlen
		Iva Pavičić
		Ivana Pehar
		Karlo Počanić
		Katarina Lelas
		Lea Grzela
		Mihovil Škalic
		Milenko Paponja
		Saira Omanovic
		Tina Matoc
		Valentina Šalković

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Alessia Bučić
		Dora Arhanić
		Iva Plazonić
		Ivan Frka Šeparović
		Jana Joksimović
		Katja Lukić
		Lea Patekar
		Lucija Kovačić
		Marta Leović
		Mia Škaljac
		Nika Supina
		Nikolina Dernaj
		Sara Jindra
		Sara Kožul
		lucija hero
	Petak u 18:0 - 19:0 (limit=15):
		Anamarija Vidovic
		Andrea Vrankić
		Anđelika Vučković
		Božena Sorić
		Dora istenić
		Dorotea Bralo
		Laura Plančak
		Lucija Kuštra
		Lucija Ratkovski
		Luka Begonja
		Matea Vučetić
		Mirna Radočaj
		Nika Fantulin
		Paula Petrašić
		Zinajda Šabić
	Petak u 19:0 - 20:0 (limit=14):
		Antea Cikojevic
		Helena Buljubašić
		Iva Barišić
		Ivana Čavka
		Ivona Barić
		Jurica Nekić
		Katarina Grcic
		Katarina Lazar
		LUCIJA RAKO
		Luka Paunović
		Matea Bubalo
		Mihaela Budimir
		Sara El-Sabeh
		Valentina Marasovic

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Ana Domitrović
		Ana Istenić
		Anja Ljevar
		Antonia Vranjković
		Ela Tomljanović
		Erika Milovec
		Filip Kliček
		Frano Visković
		Jure trogrlic
		Marija Matijašević
		Marijeta Dadić
		Ninib malki
		Petar Ivaniš
		Tarik Tahirovic
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Bašić
		Ajla Brigić
		Amra Mahmutović
		Doris Šimac
		Ida Kuštelega
		Iva Skvorcov
		Jasmina Alendar
		Karla Bikić
		Karlo Posavec
		Laura Plančak
		Lucija Ratkovski
		Martina Besek
		Nejra Kubat
		Zrinka Jemrić

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Alba Elezović
		Ana Perica
		Antonija Jokić
		Božana Knežević
		Harun Mašić
		Josip Orešković
		Karolina Boljkovac
		Katja Lukić
		Marija Dolić
		Mona Maarouf
		Monika Dragić
		Nikolina Mohorovic
		Renata Volf
		Sara Jakoliš
		Teja Trontelj
	Petak u 15:30 - 17:0 (limit=14):
		Admira Selimović
		Ana Božinović
		Ana Maria Dekanić
		Darko Đukić
		Ela Prajz
		Elizabeta Zorić
		Eva Rožman
		Filip Kašner
		Iva Ćorluka
		Jure trogrlic
		Monika Vukić
		Nedžad Osmanović
		Rebecca Beissmann
		Stanija Jovanovic
	Subota u 14:0 - 15:30 (limit=14):
		Amra Mahmutović
		Domagoj Ivančić
		Domen Tomič
		Karla Miškić
		Liza Uršič
		Luciia Ribić
		Marina Gadža
		Mladen Petkovic
		Nikolina Nazor
		Nina Vovk
		Paula Bašić
		Stipe Ivanda
		Urška Štajdohar
		Valentina Marasovic
	Subota u 15:30 - 17:0 (limit=15):
		Ajla Mustafić
		Dora Habek
		Ena Keser
		Ena Kulis
		Helena Vidović
		Iva Kordić
		Ivna Ćavar
		Jana Joksimović
		Katja Kovačević
		Lejla Reizbegović
		Lucija Rakocija
		Marko Ivančević
		Matea Dželalija
		Tadeja Blagec
		Zorana Mavija

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Andrija Segin
		Antonela Kirin
		Grgo Orlović
		Lejla Reizbegović
		Luka Paunović
		Marija Razum
		Mateja Novački
		Nera Gržetić
		Nikolina Klarić
		Stipe Ivanda
		Tadeja Blagec
		Valentina Šalković
	Petak u 18:0 - 19:0 (limit=16):
		Ivan Orabović
		Karlo Počanić
		Katarina Lelas
		Klara Gabriela Penić
		Liza Uršič
		Mihovil Škalic
		Teja Trontelj
	Petak u 19:0 - 20:0 (limit=16):
		Dora Gobin
		Karlo Posavec
		Margareta Strihic
		Marta Posavec
		Matija Žanetić

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Andrija Segin
		Bozana corluka
		Lucija Ivanac
		Lucija Šarić
		Magdalena Udovičić
		Marina Gadža
		Marta Adam
		Mihaela Rac
		Mirna Draganja
		Nera Gržetić
		Sabra Čolić
		lucija hero
	Petak u 15:30 - 17:0 (limit=14):
		Adna Vlahovljak
		Ana Aračić
		Andreas Ivanovic
		Antonia Milas
		Danica Cvjetkovic
		Dora Habek
		Ena Kulis
		Eva Krizmanić
		Klara Naka
		Lea Imamović
		Luciia Ribić
		Marija Buotić
		Moorea Kuvačić
		Viktorija Perčinlić
	Petak u 17:0 - 18:30 (limit=9):
		Ana Perica
		Antonija Jerković
		Edina Memiši
		Karla Jurković
		Marija Dolić
		Matija Žanetić
		Milenko Paponja
		Petra Lukač
		Tiffany Ocko

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Branimir Ninčević
		Elis Mikac
		Lea Patekar
		Marina Jardas
		Tijana Botić
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Alessia Bučić
		Dora Arhanić
		Nika Fantulin
		Nikolina Akmadža
		Stela Salopek
		Zinajda Šabić
	Petak u 15:30 - 17:0 (limit=6):
		Ines Perić
		Karla Roksandić
		Matea Vučetić
		Milan Tejic
		Mirna Radočaj
		Tessa Anastasja Stankic

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ana Barać
		Dejan Prpos
		Ena Popović
		Josip Orešković
		Lucija Kovačić
		Svetlana Galić
		Tiffany Ocko
	Subota u 15:30 - 17:0 (limit=8):
		Iva Skvorcov
		Lucija Kuštra
		Marta Furdi
		Marta Novak
		Matea Vidov
		Mihaela Rac
		Viktorija Perčinlić
		blanka dermit

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Ante Zelenika
		Asja Jerlagic
		Barbara Čulin
		Dora Alagic
		Emina Dzaferović
		Ena Popović
		Fran Glanz
		Inga Jukanović
		Mirna Draganja
		Nikolina Akmadža
	Petak u 18:0 - 19:0 (limit=10):
		Ajla Mustafić
		Ana Radan
		Anja Antea Sokač
		Dora Visković
		Ela Tomljanović
		Katja Kovačević
		Mia Maretić
		Moorea Kuvačić
		Paula Vršić
		Stela Cerovac
	Petak u 19:0 - 20:0 (limit=12):
		Anđela Batur
		Branimir Ninčević
		Elis Mikac
		Ena Roca
		Kristina Filipović
		Marta Novak
		Marta Smok
		Nikolina Dernaj
		Petra Lukač
		Petra Mihalić
		Sara Krog
	Subota u 17:0 - 18:0 (limit=12):
		Andrej Škerjanc
		Daria Vuk
		Dejan Prpos
		Denis Abdihodžić
		Ivan Orabović
		Lucia Levar
		Marija Razum
		Nejra Šapčanin
		Petar Ivaniš
		Svetlana Galić
		Tessa Anastasja Stankic
	Subota u 18:0 - 19:0 (limit=10):
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Anamaria Mazarekić
		Anja Antea Sokač
		Darko Tevanovic
		Dora Visković
		Dunja Ilić
		Iva Kordić
		Ivona Barić
		Katarina Grcic
		Katarina Lasan
		Klara Gabriela Penić
		Marko Ivančević
		Nika Horvat

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Antea Cikojevic
		Anđela Batur
		Iva Barišić
		Karla Miškić
		Karolina Boljkovac
		LUCIJA RAKO
		Lucija Rakocija
		Marija Mastelić
		Marijeta Dadić
		Matea Čupen
		Sara El-Sabeh
		Stela Salopek
		Tina Robič
		Višnja Šljivac

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Ana Aračić
		Barbara Ovčarić
		Dunja Ilić
		Ela Prajz
		Katarina Lasan
		Klara Naka
		Martina Besek
		Monika Dragić
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Adna Vlahovljak
		Filip Kliček
		Grgo Orlović
		Harun Mašić
		Karla Bikić
		Lea Grzela
		Marta Leović
		Matea Čupen
		Paula Vršić
		Sara Jindra
		Sara Kožul
	Subota u 17:0 - 18:30 (limit=11):
		Bozana corluka
		Darko Đukić
		Doris Šimac
		Fran Popić
		Karla Roksandić
		Marija Mastelić
		Milan Tejic
		Nedžad Osmanović
		Nikola Matijevic
		Nikolina Nazor
		Paula Petrašić
	Subota u 18:30 - 20:0 (limit=10):
		Andrea Vrankić
		Daria Vuk
		Ida Kuštelega
		Iva Ćorluka
		Ivan Frka Šeparović
		Lucija Šarić
		Luka Begonja
		Marija Matijašević
		Nika Kovačević
		Nikolina Klarić

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Ana Božinović
		Anamaria Mazarekić
		Andreas Ivanovic
		Anđelika Vučković
		Barbara Čulin
		Eva Rožman
		Katarina Lazar
		Mario Gržančić
		Mia Maretić
		Nika Kovačević
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Admira Selimović
		Ajla Brigić
		Anja Ljevar
		Asja Jerlagic
		Domen Tomič
		Emina Dzaferović
		Helena Buljubašić
		Iva Plazonić
		Katarina Pagadur
		Nina Vovk
		Vladimir Šavija
	Subota u 18:30 - 20:0 (limit=13):
		Danica Cvjetkovic
		Danijela Sabljo
		Ena Keser
		Eva Krizmanić
		Helena Vidović
		Ivna Ćavar
		Lea Imamović
		Lucija Herceg
		Marija Buotić
		Matea Dželalija
		Mihaela Hercigonja
		Petra Mihalić
		Tijana Botić

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Ana Jukić
		Ana Čeko
		Antonia Vranjković
		Dejana Komlen
		Iva Pavičić
		Ivana Pehar
		Ivana Čavka
		Magdalena Prskalo
		Matea Bubalo

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Ana Maria Dekanić
		Antonia Milas
		Denis Abdihodžić
		Elizabeta Zorić
		Fabio Pažin
		Fran Popić
		Inga Jukanović
		Karla Jurković
		Karla Sabolić
		Lucija Ivanac
		Magdalena Udovičić
		Marija Kelić
		Nika Horvat
		Renata Volf
		Rene Križ
		Sara Krog
	Subota u 15:30 - 17:0 (limit=16):
		Adis Trešnjo
		Antonela Kirin
		Danijela Sabljo
		Ena Alagic
		Ines Perić
		Marija Oreč
		Saira Omanovic
		Tina Robič
	Subota u 17:0 - 18:30 (limit=16):
		Josipa Rokov

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ana Brajdić
		Dora Gobin
		Nora Salkičević
	Petak u 18:30 - 20:0 (limit=14):
		Ajla Bašić
		Ante Zelenika
		Karla Ugrin
		Maša Matić

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Ana Domitrović
		Antonija Jokić
		Lucija Herceg
		Mihaela Hercigonja
		Nikolina Mohorovic
	Subota u 15:30 - 17:0 (limit=6):
		Alba Elezović
		Božana Knežević
		Erika Milovec
		Mona Maarouf
		Nejra Kubat
		Rasim Trnjanin

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Barbara Marušić
		Elia Croner
		Josipa Rokov
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nika Supina
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Barbara Marušić
		Debora Kanižaj
		Isabel Pavlović
		Karla Sabolić
		Mladen Petkovic
		Nora Salkičević
		Sara Jakoliš

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Barbara Ovčarić
		Domagoj Ivančić
		Elia Croner
		Ena Roca
		Jasmina Alendar
		Jelena Đapić
		Kristina Filipović
		Marina Jardas
	Subota u 19:0 - 20:0 (limit=10):
		Ana Brajdić
		Anamarija Vidovic
		Antonija Jerković
		Darko Tevanovic
		Edina Memiši
		Frano Visković
		Isabel Pavlović
		Magdalena Jurisic
		Mateja Novački
		Stela Cerovac
