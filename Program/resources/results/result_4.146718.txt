
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Božena Sorić
		Dorotea Bralo
		Erika Milovec
		Jasmina Alendar
		Katarina Pagadur
		Mateja Novački
		Mihaela Budimir
		Mladen Petkovic
		Rasim Trnjanin
		Vladimir Šavija
	Petak u 15:30 - 17:0 (limit=10):
		Ana Istenić
		Dejan Prpos
		Dora istenić
		Ivan Frka Šeparović
		Josip Orešković
		Matea Dželalija
		Tarik Tahirovic
		Tina Matoc
		Urška Štajdohar
		Viktorija Perčinlić

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Andrej Škerjanc
		Antonija Jug
		Božana Knežević
		Debora Kanižaj
		Jelena Đapić
		Karlo Vrbanić
		Lucia Levar
		Magdalena Prskalo
		Margareta Strihic
		Marta Furdi
		Monika Vukić
		Sabra Čolić
		Saira Omanovic
	Petak u 18:30 - 20:0 (limit=13):
		Antonela Kirin
		Ela Tomljanović
		Ivan Orabović
		LUCIJA RAKO
		Marija Mastelić
		Marta Novak
		Marta Posavec
		Matea Vidov
		Nika Horvat
		Ninib malki
		Rene Križ
		Stanija Jovanovic
		Valentina Šalković

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Ana Jukić
		Ana Čeko
		Andrej Škerjanc
		Antonija Jug
		Elis Mikac
		Karla Ugrin
		Karlo Vrbanić
		Klara Gabriela Penić
		Klara Naka
		Klaudija Kaurić
		Lucija Ratkovski
		Magdalena Jurisic
		Marija Razum
		Mihovil Škalic
		Moorea Kuvačić
		Saira Omanovic

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Antea Cikojevic
		Antonija Jerković
		Božena Sorić
		Dora Arhanić
		Ena Kulis
		Fabio Pažin
		Katja Lukić
		Lea Patekar
		Marija Buotić
		Matea Bubalo
		Mia Škaljac
		Mihaela Budimir
		Paula Petrašić
		Sara Jindra
		Stela Salopek
	Petak u 18:0 - 19:0 (limit=15):
		Alessia Bučić
		Ana Aračić
		Anamarija Vidovic
		Andrea Vrankić
		Jana Joksimović
		Josipa Rokov
		Jurica Nekić
		Lea Imamović
		Lucija Kuštra
		Mateja Novački
		Mia Maretić
		Nika Fantulin
		Nikolina Akmadža
		Sara Kožul
		lucija hero
	Petak u 19:0 - 20:0 (limit=14):
		Daria Vuk
		Dora Visković
		Dorotea Bralo
		Erika Milovec
		Ines Perić
		Iva Kordić
		Ivona Barić
		Karla Sabolić
		Katarina Lazar
		Luka Paunović
		Matea Vučetić
		Nora Salkičević
		Valentina Marasovic
		Zinajda Šabić

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Ana Domitrović
		Ana Istenić
		Anamaria Mazarekić
		Anja Antea Sokač
		Anja Ljevar
		Antonia Milas
		Frano Visković
		Ida Kuštelega
		Jure trogrlic
		Karla Bikić
		Karlo Posavec
		Marija Matijašević
		Nika Kovačević
		Nikolina Klarić
		Tarik Tahirovic
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Brigić
		Antonia Vranjković
		Barbara Čulin
		Dora Alagic
		Doris Šimac
		Ela Tomljanović
		Filip Kliček
		Iva Skvorcov
		Jana Joksimović
		Marija Oreč
		Mario Gržančić
		Martina Besek
		Maša Matić
		Nejra Kubat

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Božana Knežević
		Danijela Sabljo
		Harun Mašić
		Helena Vidović
		Karolina Boljkovac
		Katja Lukić
		Mona Maarouf
		Monika Vukić
		Nedžad Osmanović
		Sara Jakoliš
		Stipe Ivanda
		Tadeja Blagec
		Teja Trontelj
		Tina Robič
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Admira Selimović
		Ana Perica
		Domen Tomič
		Ela Prajz
		Iva Kordić
		Ivna Ćavar
		Jure trogrlic
		Lejla Reizbegović
		Matea Čupen
		Monika Dragić
		Nikola Matijevic
		Renata Volf
		Stanija Jovanovic
		Zinajda Šabić
	Subota u 14:0 - 15:30 (limit=14):
		Amra Mahmutović
		Dora Habek
		Elizabeta Zorić
		Ena Keser
		Filip Kašner
		Josip Orešković
		Katja Kovačević
		Lucija Rakocija
		Marija Dolić
		Marko Ivančević
		Matea Dželalija
		Nina Vovk
		Paula Bašić
		Urška Štajdohar
	Subota u 15:30 - 17:0 (limit=15):
		Alba Elezović
		Ana Božinović
		Antonija Jokić
		Darko Đukić
		Domagoj Ivančić
		Karla Miškić
		Liza Uršič
		Luciia Ribić
		Lucija Herceg
		Luka Begonja
		Matija Žanetić
		Nikolina Mohorovic
		Petra Lukač
		Rebecca Beissmann
		Sara Krog

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Ana Čeko
		Karlo Posavec
		Katarina Lelas
		Klara Gabriela Penić
		Luka Paunović
		Magdalena Udovičić
		Marija Razum
		Marta Leović
		Marta Posavec
		Stipe Ivanda
		Tijana Botić
		blanka dermit
	Petak u 18:0 - 19:0 (limit=16):
		Andrija Segin
		Marija Dolić
		Matija Žanetić
		Milan Tejic
		Nera Gržetić
		Tadeja Blagec
	Petak u 19:0 - 20:0 (limit=16):
		Bozana corluka
		Sara El-Sabeh

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Adna Vlahovljak
		Alessia Bučić
		Ana Aračić
		Dora Habek
		Elizabeta Zorić
		Ena Kulis
		Lucija Šarić
		Margareta Strihic
		Marta Adam
		Milenko Paponja
		Sabra Čolić
		Tiffany Ocko
	Petak u 15:30 - 17:0 (limit=14):
		Amila Balić
		Andrija Segin
		Antonia Milas
		Bozana corluka
		Daria Vuk
		Dora Gobin
		Eva Krizmanić
		Filip Kašner
		Ida Kuštelega
		Lucija Ivanac
		Mihaela Rac
		Nera Gržetić
		Nikolina Mohorovic
		lucija hero
	Petak u 17:0 - 18:30 (limit=9):
		Andreas Ivanovic
		Darko Tevanovic
		Darko Đukić
		Edina Memiši
		Eva Rožman
		Klara Naka
		Luciia Ribić
		Luka Begonja
		Moorea Kuvačić

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Ana Maria Dekanić
		Branimir Ninčević
		Ena Keser
		Kristina Filipović
		Marina Gadža
		Marina Jardas
		Matea Vučetić
		Tijana Botić
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Antea Cikojevic
		Antonija Jerković
		Dora Arhanić
		Ivona Barić
		Milan Tejic
		Stela Salopek
	Petak u 15:30 - 17:0 (limit=6):
		Anamarija Vidovic
		Barbara Marušić
		Ines Perić
		Iva Barišić
		Nika Fantulin
		Nikolina Akmadža

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ena Popović
		Iva Pavičić
		Ivna Ćavar
		Klaudija Kaurić
		Lucija Kuštra
		Marta Furdi
		Matea Bubalo
	Subota u 15:30 - 17:0 (limit=8):
		Anđela Batur
		Domen Tomič
		Fran Glanz
		Grgo Orlović
		Magdalena Jurisic
		Marta Novak
		Matea Vidov
		Valentina Marasovic

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Alba Elezović
		Asja Jerlagic
		Fran Glanz
		Katarina Lazar
		Kristina Filipović
		Lea Grzela
		Marta Smok
		Nikolina Dernaj
		Sara Krog
		Tiffany Ocko
	Petak u 18:0 - 19:0 (limit=10):
		Ana Radan
		Anđelika Vučković
		Barbara Čulin
		Dejan Prpos
		Dora Alagic
		Emina Dzaferović
		Ena Roca
		Lucija Kovačić
		Mihaela Hercigonja
		Petra Mihalić
	Petak u 19:0 - 20:0 (limit=12):
		Ana Pavlović
		Ante Zelenika
		Branimir Ninčević
		Denis Abdihodžić
		Inga Jukanović
		Iva Barišić
		Marta Leović
		Matea Čupen
		Paula Vršić
		Petar Ivaniš
		Petra Lukač
		Stela Cerovac
	Subota u 17:0 - 18:0 (limit=12):
		Ena Popović
		Eva Rožman
		Iva Ćorluka
		Katarina Lasan
		Lucia Levar
		Nejra Šapčanin
		Nikolina Nazor
		Svetlana Galić
		Tessa Anastasja Stankic
	Subota u 18:0 - 19:0 (limit=10):
		Anđela Batur
	Subota u 19:0 - 20:0 (limit=10):
		Mirna Draganja

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Antonija Jokić
		Dejana Komlen
		Ivan Orabović
		Katarina Grcic
		Katarina Lasan
		Lucija Kovačić
		Marijeta Dadić
		Nika Horvat
		Ninib malki
		Petra Mihalić
		Svetlana Galić
		Valentina Šalković

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Bašić
		Inga Jukanović
		Iva Pavičić
		Ivana Pehar
		Ivana Čavka
		Karla Roksandić
		Karolina Boljkovac
		LUCIJA RAKO
		Laura Plančak
		Lucija Rakocija
		Sara El-Sabeh
		Višnja Šljivac
		Zrinka Jemrić

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Dunja Ilić
		Ela Prajz
		Marija Mastelić
		Marijeta Dadić
		Marta Adam
		Martina Besek
		Mirna Radočaj
		Monika Dragić
		Viktorija Perčinlić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Anja Antea Sokač
		Lea Patekar
		Lejla Reizbegović
		Marija Matijašević
		Mihovil Škalic
		Nika Kovačević
		Nikolina Klarić
		Paula Vršić
		Sara Jindra
		Teja Trontelj
		Vladimir Šavija
	Subota u 17:0 - 18:30 (limit=11):
		Adna Vlahovljak
		Andrea Vrankić
		Doris Šimac
		Harun Mašić
		Josipa Rokov
		Jurica Nekić
		Marina Gadža
		Mario Gržančić
		Mirna Draganja
		Paula Petrašić
		Sara Kožul
	Subota u 18:30 - 20:0 (limit=10):
		Dora Visković
		Grgo Orlović
		Iva Ćorluka
		Ivan Frka Šeparović
		Karla Sabolić
		Katja Kovačević
		Lucija Šarić
		Marko Ivančević
		Nejra Kubat
		Nikolina Nazor

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Anamaria Mazarekić
		Andreas Ivanovic
		Anđelika Vučković
		Dora Gobin
		Ena Roca
		Frano Visković
		Helena Buljubašić
		Mia Maretić
		Petar Ivaniš
		Zrinka Jemrić
		blanka dermit

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Bašić
		Ajla Brigić
		Ana Perica
		Anja Ljevar
		Asja Jerlagic
		Katarina Pagadur
		Lea Imamović
		Marija Buotić
		Mirna Radočaj
		Nina Vovk
		Rasim Trnjanin
	Subota u 18:30 - 20:0 (limit=13):
		Emina Dzaferović
		Eva Krizmanić
		Helena Buljubašić
		Iva Plazonić
		Karla Roksandić
		Katarina Lelas
		Lea Grzela
		Lucija Herceg
		Mihaela Hercigonja
		Nika Supina
		Nikolina Dernaj
		Rebecca Beissmann
		Tessa Anastasja Stankic

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Amra Mahmutović
		Ana Jukić
		Antonia Vranjković
		Barbara Marušić
		Dejana Komlen
		Dora istenić
		Magdalena Prskalo
		Nora Salkičević
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Antonela Kirin
		Danijela Sabljo
		Denis Abdihodžić
		Filip Kliček
		Ivana Pehar
		Ivana Čavka
		Karlo Počanić
		Lucija Ivanac
		Magdalena Udovičić
		Marija Kelić
		Marija Oreč
		Mihaela Rac
		Renata Volf
		Rene Križ
		Tina Matoc
	Subota u 15:30 - 17:0 (limit=16):
		Adis Trešnjo
		Ana Maria Dekanić
		Danica Cvjetkovic
		Ena Alagic
		Fabio Pažin
		Fran Popić
		Mia Škaljac
	Subota u 17:0 - 18:30 (limit=16):
		Ajla Mustafić
		Karla Jurković

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ana Brajdić
		Ante Zelenika
		Danica Cvjetkovic
		Fran Popić
		Isabel Pavlović
		Karla Ugrin
		Milenko Paponja
	Petak u 18:30 - 20:0 (limit=14):
		Karla Miškić
		Maša Matić

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Admira Selimović
		Ajla Mustafić
		Karla Bikić
		Karla Jurković
		Tina Robič
	Subota u 15:30 - 17:0 (limit=6):
		Ana Domitrović
		Helena Vidović
		Iva Skvorcov
		Mona Maarouf
		Nedžad Osmanović
		Sara Jakoliš

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Ana Barać
		Elia Croner
		Lucija Ratkovski
		Marta Smok
		Nejra Šapčanin
		Nika Supina
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Barbara Ovčarić
		Debora Kanižaj
		Elia Croner
		Isabel Pavlović
		Iva Plazonić
		Laura Plančak
		Marija Kelić
		Mladen Petkovic

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Barać
		Domagoj Ivančić
		Elis Mikac
		Jasmina Alendar
		Jelena Đapić
		Katarina Grcic
		Liza Uršič
		Paula Bašić
	Subota u 19:0 - 20:0 (limit=10):
		Ana Božinović
		Ana Brajdić
		Barbara Ovčarić
		Darko Tevanovic
		Dunja Ilić
		Edina Memiši
		Ena Alagic
		Karlo Počanić
		Marina Jardas
		Stela Cerovac
