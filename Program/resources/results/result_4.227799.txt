
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Andrej Škerjanc
		Danica Cvjetkovic
		Ena Alagic
		Katarina Pagadur
		Klaudija Kaurić
		Mihaela Budimir
		Mladen Petkovic
		Monika Vukić
		Moorea Kuvačić
		Rasim Trnjanin
	Petak u 15:30 - 17:0 (limit=10):
		Dunja Ilić
		Ena Popović
		Lea Patekar
		Magdalena Udovičić
		Mateja Novački
		Monika Dragić
		Nika Fantulin
		Nikolina Mohorovic
		Sara Jindra
		Zrinka Jemrić

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Antonija Jug
		Ivan Orabović
		Karlo Počanić
		Karlo Vrbanić
		Marta Adam
		Marta Furdi
		Marta Posavec
		Matea Vidov
		Paula Bašić
		Sabra Čolić
		Saira Omanovic
		Tiffany Ocko
		blanka dermit
	Petak u 18:30 - 20:0 (limit=13):
		Andrej Škerjanc
		Antonela Kirin
		Dora Alagic
		Dora Gobin
		Ela Tomljanović
		LUCIJA RAKO
		Lucia Levar
		Magdalena Prskalo
		Magdalena Udovičić
		Mia Škaljac
		Mihaela Rac
		Valentina Marasovic
		Valentina Šalković

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Ana Barać
		Ana Čeko
		Antonija Jug
		Ivana Pehar
		Jelena Đapić
		Karlo Počanić
		Karlo Vrbanić
		Katarina Lelas
		Klara Gabriela Penić
		Klara Naka
		Marta Novak
		Matea Vidov
		Mihovil Škalic
		Nera Gržetić
		Nika Horvat
		Saira Omanovic

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Ana Aračić
		Ana Radan
		Dora Arhanić
		Dorotea Bralo
		Fabio Pažin
		Inga Jukanović
		Ivana Čavka
		Karla Sabolić
		LUCIJA RAKO
		Lucija Kuštra
		Marija Buotić
		Mia Maretić
		Mihaela Budimir
		Valentina Marasovic
		Višnja Šljivac
	Petak u 18:0 - 19:0 (limit=15):
		Anamarija Vidovic
		Ante Zelenika
		Danica Cvjetkovic
		Dora Visković
		Ena Alagic
		Helena Buljubašić
		Iva Barišić
		Ivan Frka Šeparović
		Jelena Đapić
		Jurica Nekić
		Lucija Kovačić
		Mirna Radočaj
		Nika Supina
		Nikolina Dernaj
		Petra Mihalić
	Petak u 19:0 - 20:0 (limit=14):
		Alessia Bučić
		Andrija Segin
		Antonija Jerković
		Erika Milovec
		Ines Perić
		Ivona Barić
		Laura Plančak
		Luka Paunović
		Matea Dželalija
		Nika Fantulin
		Paula Petrašić
		Paula Vršić
		Tiffany Ocko
		Zinajda Šabić

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Ana Domitrović
		Andreas Ivanovic
		Anja Ljevar
		Antonia Vranjković
		Barbara Čulin
		Filip Kliček
		Frano Visković
		Jasmina Alendar
		Karla Bikić
		Karlo Posavec
		Katarina Lasan
		Marija Matijašević
		Tessa Anastasja Stankic
		Viktorija Perčinlić
	Petak u 15:30 - 17:0 (limit=14):
		Adis Trešnjo
		Ajla Bašić
		Doris Šimac
		Erika Milovec
		Ida Kuštelega
		Iva Pavičić
		Iva Skvorcov
		Laura Plančak
		Marija Mastelić
		Marijeta Dadić
		Mihaela Rac
		Nikolina Dernaj
		Petar Ivaniš
		Svetlana Galić

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Admira Selimović
		Ana Božinović
		Ana Maria Dekanić
		Ana Perica
		Danijela Sabljo
		Darko Đukić
		Elizabeta Zorić
		Eva Rožman
		Iva Ćorluka
		Lucija Herceg
		Lucija Rakocija
		Mona Maarouf
		Nikola Matijevic
		Renata Volf
		Sara Jakoliš
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Mustafić
		Alba Elezović
		Božana Knežević
		Ela Prajz
		Harun Mašić
		Josip Orešković
		Karolina Boljkovac
		Lejla Reizbegović
		Marija Dolić
		Matea Dželalija
		Rebecca Beissmann
		Stipe Ivanda
		Tina Robič
		Urška Štajdohar
	Subota u 14:0 - 15:30 (limit=14):
		Antonija Jokić
		Ena Keser
		Filip Kašner
		Jure trogrlic
		Karla Jurković
		Karla Miškić
		Katja Kovačević
		Matea Čupen
		Monika Dragić
		Nejra Kubat
		Nikolina Mohorovic
		Paula Bašić
		Sara Krog
		Zorana Mavija
	Subota u 15:30 - 17:0 (limit=15):
		Domagoj Ivančić
		Ena Kulis
		Helena Vidović
		Ivna Ćavar
		Jana Joksimović
		Katja Lukić
		Luciia Ribić
		Marina Gadža
		Marko Ivančević
		Nedžad Osmanović
		Nina Vovk
		Petra Lukač
		Stanija Jovanovic
		Tadeja Blagec
		lucija hero

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Ana Pavlović
		Edina Memiši
		Grgo Orlović
		Karlo Posavec
		Katarina Lelas
		Klara Gabriela Penić
		Luka Paunović
		Marina Gadža
		Mario Gržančić
		Matea Vučetić
		Matija Žanetić
		Monika Vukić
		Nikolina Klarić
		Stanija Jovanovic
		Teja Trontelj
	Petak u 18:0 - 19:0 (limit=16):
		Marija Dolić
		Marija Razum
		Tadeja Blagec
		Tarik Tahirovic
	Petak u 19:0 - 20:0 (limit=16):
		Bozana corluka
		Ivan Orabović
		Marta Posavec
		Milenko Paponja

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Amila Balić
		Andrija Segin
		Eva Krizmanić
		Filip Kašner
		Karla Jurković
		Lea Imamović
		Lucija Ratkovski
		Luka Begonja
		Marta Adam
		Mirna Draganja
		Sabra Čolić
		Sara Krog
	Petak u 15:30 - 17:0 (limit=14):
		Adna Vlahovljak
		Antonia Milas
		Daria Vuk
		Debora Kanižaj
		Dora Habek
		Dora istenić
		Luciia Ribić
		Margareta Strihic
		Nika Kovačević
		Nora Salkičević
		Paula Petrašić
		Rene Križ
		Tarik Tahirovic
		lucija hero
	Petak u 17:0 - 18:30 (limit=9):
		Alessia Bučić
		Ana Perica
		Elizabeta Zorić
		Ena Kulis
		Eva Rožman
		Klara Naka
		Lucija Ivanac
		Milenko Paponja
		Moorea Kuvačić

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Ana Istenić
		Andrea Vrankić
		Branimir Ninčević
		Domen Tomič
		Elis Mikac
		Liza Uršič
		Marina Jardas
		Matea Bubalo
		Nikolina Nazor
		Tijana Botić
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Ana Radan
		Dora Arhanić
		Ivana Čavka
		Milan Tejic
		Nikolina Akmadža
		blanka dermit
	Petak u 15:30 - 17:0 (limit=6):
		Amra Mahmutović
		Ana Domitrović
		Darko Tevanovic
		Dora Visković
		Iva Barišić
		Mirna Radočaj

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Adis Trešnjo
		Ana Barać
		Anđela Batur
		Denis Abdihodžić
		Domen Tomič
		Lucija Kuštra
		Matea Bubalo
	Subota u 15:30 - 17:0 (limit=8):
		Ajla Mustafić
		Ana Čeko
		Dejan Prpos
		Iva Pavičić
		Iva Skvorcov
		Josip Orešković
		Klaudija Kaurić
		Urška Štajdohar

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Barbara Čulin
		Božena Sorić
		Ena Roca
		Fran Glanz
		Katarina Lazar
		Lucia Levar
		Mirna Draganja
		Nejra Šapčanin
		Nikolina Nazor
		Stela Cerovac
	Petak u 18:0 - 19:0 (limit=10):
		Alba Elezović
		Anja Antea Sokač
		Daria Vuk
		Ena Popović
		Marta Leović
		Marta Smok
		Nedžad Osmanović
		Svetlana Galić
		Tina Matoc
	Petak u 19:0 - 20:0 (limit=12):
		Ana Pavlović
		Antea Cikojevic
		Anđela Batur
		Branimir Ninčević
		Emina Dzaferović
		Grgo Orlović
		Kristina Filipović
		Lea Grzela
		Marta Furdi
		Marta Novak
		Petra Lukač
	Subota u 17:0 - 18:0 (limit=12):
		Ante Zelenika
		Anđelika Vučković
		Denis Abdihodžić
		Dora Alagic
		Ela Tomljanović
		Iva Ćorluka
		Mihaela Hercigonja
		Nikolina Akmadža
		Petra Mihalić
		Zinajda Šabić
	Subota u 18:0 - 19:0 (limit=10):
		Dejan Prpos
	Subota u 19:0 - 20:0 (limit=10):
		Asja Jerlagic

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Anamaria Mazarekić
		Anja Antea Sokač
		Antonija Jokić
		Barbara Marušić
		Dejana Komlen
		Jana Joksimović
		Karla Roksandić
		Katja Lukić
		Marija Razum
		Marko Ivančević
		Marta Leović
		Ninib malki
		Valentina Šalković

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Brigić
		Ana Jukić
		Iva Kordić
		Jure trogrlic
		Lucija Rakocija
		Lucija Ratkovski
		Lucija Šarić
		Martina Besek
		Nika Horvat
		Sara El-Sabeh
		Stela Salopek

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Ana Brajdić
		Antea Cikojevic
		Barbara Ovčarić
		Ela Prajz
		Karla Roksandić
		Marijeta Dadić
		Martina Besek
		Sara El-Sabeh
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Darko Tevanovic
		Darko Đukić
		Harun Mašić
		Inga Jukanović
		Ivan Frka Šeparović
		Jurica Nekić
		Karla Bikić
		Katarina Lasan
		Milan Tejic
		Nera Gržetić
		Teja Trontelj
	Subota u 17:0 - 18:30 (limit=11):
		Anamarija Vidovic
		Doris Šimac
		Ida Kuštelega
		Ivona Barić
		Karolina Boljkovac
		Katja Kovačević
		Lejla Reizbegović
		Marija Mastelić
		Nikolina Klarić
		Paula Vršić
		Višnja Šljivac
	Subota u 18:30 - 20:0 (limit=10):
		Antonija Jerković
		Bozana corluka
		Karla Miškić
		Lea Patekar
		Marija Oreč
		Matea Čupen
		Mihovil Škalic
		Sara Jindra
		Sara Kožul
		Vladimir Šavija

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Anamaria Mazarekić
		Andreas Ivanovic
		Anđelika Vučković
		Dora Gobin
		Ena Roca
		Katarina Lazar
		Mia Maretić
		Nika Kovačević
		Rebecca Beissmann
		Sara Kožul
		Stela Salopek

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Bašić
		Anja Ljevar
		Asja Jerlagic
		Božena Sorić
		Emina Dzaferović
		Eva Krizmanić
		Helena Buljubašić
		Katarina Pagadur
		Marija Buotić
		Nika Supina
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Brigić
		Dorotea Bralo
		Fran Glanz
		Helena Vidović
		Iva Plazonić
		Ivna Ćavar
		Lea Grzela
		Lea Imamović
		Matea Vučetić
		Maša Matić
		Nina Vovk
		Ninib malki
		Tessa Anastasja Stankic

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Ana Aračić
		Barbara Marušić
		Dejana Komlen
		Dora istenić
		Ines Perić
		Ivana Pehar
		Magdalena Prskalo
		Nejra Kubat
		Stipe Ivanda
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Adna Vlahovljak
		Ana Božinović
		Ana Jukić
		Ana Maria Dekanić
		Antonela Kirin
		Danijela Sabljo
		Filip Kliček
		Karla Sabolić
		Lucija Ivanac
		Marija Kelić
		Marija Matijašević
		Marija Oreč
		Renata Volf
		Tina Matoc
		Tina Robič
	Subota u 15:30 - 17:0 (limit=16):
		Antonia Milas
		Dunja Ilić
		Fabio Pažin
		Fran Popić
		Iva Kordić
		Lucija Kovačić
		Lucija Šarić
		Luka Begonja
		Matija Žanetić
		Mia Škaljac
		Rene Križ
		Viktorija Perčinlić
	Subota u 17:0 - 18:30 (limit=16):
		Ena Keser
		Josipa Rokov

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Andrea Vrankić
		Fran Popić
		Isabel Pavlović
		Karla Ugrin
		Maša Matić
	Petak u 18:30 - 20:0 (limit=14):

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Božana Knežević
		Mihaela Hercigonja
		Petar Ivaniš
		Rasim Trnjanin
		Sara Jakoliš
	Subota u 15:30 - 17:0 (limit=6):
		Antonia Vranjković
		Frano Visković
		Kristina Filipović
		Lucija Herceg
		Mona Maarouf
		Vladimir Šavija

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Elia Croner
		Josipa Rokov
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Amila Balić
		Debora Kanižaj
		Edina Memiši
		Isabel Pavlović
		Iva Plazonić
		Karla Ugrin
		Katarina Grcic
		Marija Kelić
		Nora Salkičević

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Brajdić
		Barbara Ovčarić
		Domagoj Ivančić
		Magdalena Jurisic
		Margareta Strihic
		Marina Jardas
		Mateja Novački
		Stela Cerovac
	Subota u 19:0 - 20:0 (limit=10):
		Admira Selimović
		Ana Istenić
		Dora Habek
		Elia Croner
		Elis Mikac
		Jasmina Alendar
		Katarina Grcic
		Liza Uršič
		Mario Gržančić
		Mladen Petkovic
