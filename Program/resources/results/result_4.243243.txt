
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Dejan Prpos
		Fabio Pažin
		Katarina Pagadur
		Marina Jardas
		Mateja Novački
		Mihaela Budimir
		Mihovil Škalic
		Moorea Kuvačić
		Rasim Trnjanin
		Rebecca Beissmann
	Petak u 15:30 - 17:0 (limit=10):
		Ana Istenić
		Antonija Jug
		Danica Cvjetkovic
		Dora istenić
		Jurica Nekić
		Lea Grzela
		Lejla Reizbegović
		Magdalena Udovičić
		Saira Omanovic
		Vladimir Šavija

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Ana Jukić
		Andrej Škerjanc
		Debora Kanižaj
		Frano Visković
		Isabel Pavlović
		Ivan Orabović
		Lucia Levar
		Luka Begonja
		Marta Adam
		Matea Vidov
		Maša Matić
		Rene Križ
		blanka dermit
	Petak u 18:30 - 20:0 (limit=13):
		Fran Glanz
		Grgo Orlović
		Jurica Nekić
		Karlo Počanić
		Liza Uršič
		Luka Paunović
		Magdalena Udovičić
		Mia Škaljac
		Milenko Paponja
		Nikolina Akmadža
		Ninib malki
		Saira Omanovic
		Stanija Jovanovic

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Adna Vlahovljak
		Ana Barać
		Ana Brajdić
		Ana Pavlović
		Andrej Škerjanc
		Barbara Marušić
		Dejana Komlen
		Edina Memiši
		Elis Mikac
		Iva Pavičić
		Ivan Orabović
		Jelena Đapić
		Lucia Levar
		Magdalena Prskalo
		Marta Smok
		Nera Gržetić

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Andrija Segin
		Antonija Jerković
		Dora Arhanić
		Ena Alagic
		Helena Buljubašić
		Ivona Barić
		Katarina Lazar
		Laura Plančak
		Lucija Kuštra
		Marija Buotić
		Matija Žanetić
		Mia Maretić
		Mia Škaljac
		Petra Mihalić
		Sara Jindra
	Petak u 18:0 - 19:0 (limit=15):
		Ana Aračić
		Anamarija Vidovic
		Daria Vuk
		Ena Kulis
		Inga Jukanović
		Ivan Frka Šeparović
		Jana Joksimović
		Jelena Đapić
		LUCIJA RAKO
		Lucija Kovačić
		Marta Leović
		Mihaela Budimir
		Paula Petrašić
		Sara Kožul
		Tiffany Ocko
	Petak u 19:0 - 20:0 (limit=14):
		Alessia Bučić
		Danica Cvjetkovic
		Dora Habek
		Dora istenić
		Erika Milovec
		Josipa Rokov
		Katarina Grcic
		Lucija Rakocija
		Matea Bubalo
		Matea Vučetić
		Mateja Novački
		Maša Matić
		Nika Fantulin
		Paula Vršić

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Ana Domitrović
		Andreas Ivanovic
		Doris Šimac
		Dunja Ilić
		Ida Kuštelega
		Karlo Posavec
		Lucija Ratkovski
		Marija Matijašević
		Martina Besek
		Matea Čupen
		Mirna Draganja
		Nejra Kubat
		Svetlana Galić
		Viktorija Perčinlić
	Petak u 15:30 - 17:0 (limit=14):
		Anamaria Mazarekić
		Anja Ljevar
		Ena Popović
		Erika Milovec
		Eva Krizmanić
		Jana Joksimović
		Jure trogrlic
		Karla Jurković
		Marija Mastelić
		Marija Oreč
		Mario Gržančić
		Nikolina Klarić
		Petar Ivaniš
		Tessa Anastasja Stankic

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Ajla Mustafić
		Ana Perica
		Antonija Jokić
		Božana Knežević
		Harun Mašić
		Iva Ćorluka
		Liza Uršič
		Lucija Herceg
		Lucija Rakocija
		Marko Ivančević
		Nedžad Osmanović
		Paula Bašić
		Sara Jakoliš
		Tadeja Blagec
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Branimir Ninčević
		Danijela Sabljo
		Domen Tomič
		Dora Habek
		Elizabeta Zorić
		Ena Keser
		Katja Kovačević
		Mirna Radočaj
		Mona Maarouf
		Nina Vovk
		Renata Volf
		Stanija Jovanovic
		Stipe Ivanda
		Zinajda Šabić
	Subota u 14:0 - 15:30 (limit=14):
		Admira Selimović
		Darko Đukić
		Domagoj Ivančić
		Jure trogrlic
		Karla Jurković
		Lejla Reizbegović
		Luciia Ribić
		Marija Dolić
		Monika Dragić
		Monika Vukić
		Sara Krog
		Tina Robič
		Valentina Marasovic
		lucija hero
	Subota u 15:30 - 17:0 (limit=15):
		Alba Elezović
		Ela Prajz
		Filip Kašner
		Helena Vidović
		Iva Kordić
		Ivna Ćavar
		Josip Orešković
		Karla Miškić
		Katja Lukić
		Marina Gadža
		Matea Dželalija
		Mladen Petkovic
		Nikolina Mohorovic
		Rebecca Beissmann
		Teja Trontelj

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Antonela Kirin
		Grgo Orlović
		Ivana Čavka
		Luka Paunović
		Marija Razum
		Marta Novak
		Marta Posavec
		Milan Tejic
		Milenko Paponja
		Monika Vukić
		Tadeja Blagec
		Teja Trontelj
	Petak u 18:0 - 19:0 (limit=16):
		Ana Čeko
		Elizabeta Zorić
		Katarina Lelas
		Klara Gabriela Penić
		Margareta Strihic
		Valentina Šalković
	Petak u 19:0 - 20:0 (limit=16):
		Dora Gobin
		Tarik Tahirovic

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Amila Balić
		Ana Aračić
		Andrija Segin
		Bozana corluka
		Darko Đukić
		Ena Kulis
		Filip Kliček
		Lucija Šarić
		Margareta Strihic
		Marija Dolić
		Marta Adam
		Sara Krog
	Petak u 15:30 - 17:0 (limit=14):
		Anja Antea Sokač
		Antonia Milas
		Barbara Ovčarić
		Filip Kašner
		Fran Popić
		Ida Kuštelega
		Iva Skvorcov
		Klara Naka
		Mirna Draganja
		Nika Kovačević
		Nika Supina
		Nikolina Mohorovic
		Nora Salkičević
		Paula Petrašić
	Petak u 17:0 - 18:30 (limit=9):
		Ana Perica
		Edina Memiši
		Eva Rožman
		Iva Barišić
		Luciia Ribić
		Lucija Ivanac
		Mihaela Rac
		Moorea Kuvačić
		Tarik Tahirovic

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Ana Maria Dekanić
		Božena Sorić
		Ela Prajz
		Petra Lukač
		Tijana Botić
		lucija hero
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Anamarija Vidovic
		Antea Cikojevic
		Dora Arhanić
		Katarina Lazar
		Milan Tejic
		blanka dermit
	Petak u 15:30 - 17:0 (limit=6):
		Alessia Bučić
		Amra Mahmutović
		Darko Tevanovic
		Dora Alagic
		Katarina Grcic
		Nika Fantulin

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ajla Mustafić
		Ana Barać
		Anđela Batur
		Marta Furdi
		Mihaela Rac
		Svetlana Galić
		Tiffany Ocko
	Subota u 15:30 - 17:0 (limit=8):
		Dejan Prpos
		Domen Tomič
		Ena Popović
		Lucija Kovačić
		Lucija Kuštra
		Matea Vidov
		Matea Vučetić
		Urška Štajdohar

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Alba Elezović
		Anđela Batur
		Barbara Čulin
		Božena Sorić
		Denis Abdihodžić
		Ela Tomljanović
		Emina Dzaferović
		Ena Roca
		Nejra Šapčanin
		Nikolina Dernaj
	Petak u 18:0 - 19:0 (limit=10):
		Ana Radan
		Antea Cikojevic
		Dora Visković
		Karlo Vrbanić
		Katja Kovačević
		Marta Furdi
		Nikolina Nazor
		Stela Cerovac
		Tessa Anastasja Stankic
		Tina Matoc
	Petak u 19:0 - 20:0 (limit=12):
		Anđelika Vučković
		Asja Jerlagic
		Dora Alagic
		Iva Barišić
		Kristina Filipović
		Marija Razum
		Marta Novak
		Marta Posavec
		Mona Maarouf
		Petra Mihalić
		Urška Štajdohar
	Subota u 17:0 - 18:0 (limit=12):
		Ante Zelenika
		Nikolina Akmadža
		Nina Vovk
		Petra Lukač
		Zinajda Šabić
	Subota u 18:0 - 19:0 (limit=10):
		Lea Patekar
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Dora Visković
		Dorotea Bralo
		Iva Kordić
		Karla Bikić
		Karla Roksandić
		Katarina Lasan
		Katja Lukić
		Klara Gabriela Penić
		LUCIJA RAKO
		Ninib malki
		Tina Robič
		Valentina Šalković

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Bašić
		Ajla Brigić
		Antonia Vranjković
		Ivana Pehar
		Karla Miškić
		Karolina Boljkovac
		Lucija Ratkovski
		Matea Dželalija
		Nika Horvat
		Sara El-Sabeh
		Stela Salopek
		Valentina Marasovic
		Višnja Šljivac
		Zrinka Jemrić

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Antonija Jokić
		Barbara Ovčarić
		Karla Ugrin
		Katarina Lasan
		Martina Besek
		Mirna Radočaj
		Sara El-Sabeh
		Višnja Šljivac
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Andrea Vrankić
		Bozana corluka
		Harun Mašić
		Iva Ćorluka
		Karla Roksandić
		Lea Patekar
		Marija Matijašević
		Marko Ivančević
		Matija Žanetić
		Nikolina Nazor
		Sara Jindra
	Subota u 17:0 - 18:30 (limit=11):
		Adna Vlahovljak
		Anja Antea Sokač
		Branimir Ninčević
		Daria Vuk
		Ivan Frka Šeparović
		Luka Begonja
		Marta Leović
		Matea Čupen
		Nera Gržetić
		Nikola Matijevic
		Sara Kožul
	Subota u 18:30 - 20:0 (limit=10):
		Darko Tevanovic
		Inga Jukanović
		Josipa Rokov
		Lea Grzela
		Marija Mastelić
		Marina Gadža
		Mihovil Škalic
		Nikolina Klarić
		Paula Vršić
		Vladimir Šavija

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Anamaria Mazarekić
		Andreas Ivanovic
		Antonia Vranjković
		Barbara Čulin
		Dora Gobin
		Ena Roca
		Frano Visković
		Iva Skvorcov
		Mia Maretić
		Nika Kovačević
		Petar Ivaniš

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Admira Selimović
		Ajla Brigić
		Anja Ljevar
		Eva Krizmanić
		Helena Buljubašić
		Katarina Lelas
		Katarina Pagadur
		Marija Buotić
		Mihaela Hercigonja
		Rasim Trnjanin
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Bašić
		Anđelika Vučković
		Dorotea Bralo
		Emina Dzaferović
		Ena Keser
		Fran Glanz
		Helena Vidović
		Ivna Ćavar
		Josip Orešković
		Lea Imamović
		Monika Dragić
		Nika Supina
		Nikolina Dernaj

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Ana Jukić
		Barbara Marušić
		Dejana Komlen
		Iva Pavičić
		Klara Naka
		Lucija Šarić
		Magdalena Prskalo
		Matea Bubalo
		Stipe Ivanda

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Ana Domitrović
		Antonela Kirin
		Antonia Milas
		Antonija Jug
		Denis Abdihodžić
		Fabio Pažin
		Fran Popić
		Jasmina Alendar
		Karla Sabolić
		Marija Kelić
		Marija Oreč
		Marina Jardas
		Nika Horvat
		Renata Volf
		Rene Križ
		Tina Matoc
	Subota u 15:30 - 17:0 (limit=16):
		Adis Trešnjo
		Ana Božinović
		Ana Maria Dekanić
		Ena Alagic
		Filip Kliček
		Ines Perić
		Ivana Pehar
		Ivana Čavka
		Karla Bikić
		Karlo Vrbanić
		Karolina Boljkovac
		Lucija Ivanac
		Marijeta Dadić
	Subota u 17:0 - 18:30 (limit=16):
		Danijela Sabljo
		Viktorija Perčinlić

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Andrea Vrankić
		Ante Zelenika
		Asja Jerlagic
		Ines Perić
		Iva Plazonić
		Karla Ugrin
	Petak u 18:30 - 20:0 (limit=14):
		Laura Plančak
		Stela Salopek

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Doris Šimac
		Karlo Posavec
		Mihaela Hercigonja
		Nedžad Osmanović
		Paula Bašić
	Subota u 15:30 - 17:0 (limit=6):
		Ana Brajdić
		Božana Knežević
		Ela Tomljanović
		Lucija Herceg
		Nejra Kubat
		Sara Jakoliš

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Elia Croner
		Lea Imamović
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
		Sabra Čolić
		Zorana Mavija
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Debora Kanižaj
		Isabel Pavlović
		Iva Plazonić
		Ivona Barić
		Karla Sabolić
		Klaudija Kaurić
		Marija Kelić
		Nora Salkičević
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Istenić
		Antonija Jerković
		Domagoj Ivančić
		Elis Mikac
		Jasmina Alendar
		Karlo Počanić
		Magdalena Jurisic
		Mario Gržančić
	Subota u 19:0 - 20:0 (limit=10):
		Ana Božinović
		Ana Čeko
		Dunja Ilić
		Elia Croner
		Eva Rožman
		Klaudija Kaurić
		Kristina Filipović
		Marijeta Dadić
		Mladen Petkovic
		Stela Cerovac
