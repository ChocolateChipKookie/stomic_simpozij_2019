
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Božena Sorić
		Danica Cvjetkovic
		Dora istenić
		Fabio Pažin
		Katarina Pagadur
		Klaudija Kaurić
		Mladen Petkovic
		Rasim Trnjanin
		Saira Omanovic
		Zrinka Jemrić
	Petak u 15:30 - 17:0 (limit=10):
		Ana Istenić
		Ena Alagic
		Erika Milovec
		Helena Buljubašić
		Marija Oreč
		Nika Fantulin
		Rebecca Beissmann
		Tarik Tahirovic
		Viktorija Perčinlić
		lucija hero

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Andrej Škerjanc
		Antonija Jug
		Debora Kanižaj
		Fran Glanz
		Ivana Pehar
		Lucia Levar
		Margareta Strihic
		Marta Adam
		Marta Furdi
		Matea Vidov
		Nika Horvat
		Saira Omanovic
		Valentina Marasovic
	Petak u 18:30 - 20:0 (limit=13):
		Alba Elezović
		Ena Alagic
		Erika Milovec
		Ivan Orabović
		Jelena Đapić
		LUCIJA RAKO
		Magdalena Prskalo
		Mia Škaljac
		Monika Vukić
		Sabra Čolić
		Stanija Jovanovic
		Valentina Šalković
		blanka dermit

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Adna Vlahovljak
		Amila Balić
		Ana Jukić
		Ana Čeko
		Andrej Škerjanc
		Antonija Jug
		Debora Kanižaj
		Dejana Komlen
		Karlo Počanić
		Klara Gabriela Penić
		Klara Naka
		Lea Grzela
		Magdalena Jurisic
		Mihovil Škalic
		Moorea Kuvačić
		Nera Gržetić

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Ante Zelenika
		Antea Cikojevic
		Anđelika Vučković
		Božena Sorić
		Danica Cvjetkovic
		Daria Vuk
		Fabio Pažin
		Katarina Grcic
		Katarina Lazar
		Katja Lukić
		LUCIJA RAKO
		Lea Imamović
		Matea Vučetić
		Mia Škaljac
		Sara Jindra
	Petak u 18:0 - 19:0 (limit=15):
		Alessia Bučić
		Barbara Ovčarić
		Dora Arhanić
		Dora Visković
		Ena Kulis
		Ines Perić
		Karla Sabolić
		Lea Patekar
		Marta Leović
		Maša Matić
		Mia Maretić
		Mirna Radočaj
		Nikolina Akmadža
		Paula Petrašić
		Petra Mihalić
	Petak u 19:0 - 20:0 (limit=14):
		Ana Aračić
		Anamarija Vidovic
		Ivan Frka Šeparović
		Ivona Barić
		Josipa Rokov
		Jurica Nekić
		Lucija Ivanac
		Lucija Kuštra
		Matea Bubalo
		Matija Žanetić
		Mladen Petkovic
		Nikolina Dernaj
		Valentina Marasovic
		lucija hero

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Ana Domitrović
		Antonia Milas
		Antonia Vranjković
		Filip Kliček
		Iva Skvorcov
		Mario Gržančić
		Martina Besek
		Mihaela Rac
		Mirna Draganja
		Nejra Kubat
		Nika Kovačević
		Nika Supina
		Petar Ivaniš
		Tarik Tahirovic
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Bašić
		Ana Brajdić
		Anja Ljevar
		Dora Alagic
		Eva Krizmanić
		Frano Visković
		Ida Kuštelega
		Iva Pavičić
		Jasmina Alendar
		Karla Jurković
		Karlo Posavec
		Marija Matijašević
		Matea Čupen
		Maša Matić

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Domagoj Ivančić
		Filip Kašner
		Harun Mašić
		Lejla Reizbegović
		Luciia Ribić
		Matea Dželalija
		Mona Maarouf
		Monika Dragić
		Nedžad Osmanović
		Nikola Matijevic
		Nikolina Mohorovic
		Nikolina Nazor
		Renata Volf
		Tadeja Blagec
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Mustafić
		Alba Elezović
		Ana Perica
		Antonija Jokić
		Danijela Sabljo
		Domen Tomič
		Eva Rožman
		Iva Kordić
		Marija Dolić
		Marko Ivančević
		Monika Vukić
		Sara Jakoliš
		Stanija Jovanovic
		Urška Štajdohar
	Subota u 14:0 - 15:30 (limit=14):
		Ana Maria Dekanić
		Božana Knežević
		Darko Đukić
		Ela Prajz
		Elizabeta Zorić
		Jana Joksimović
		Karla Jurković
		Karolina Boljkovac
		Lucija Herceg
		Marina Gadža
		Petra Lukač
		Rebecca Beissmann
		Sara Krog
		Stipe Ivanda
	Subota u 15:30 - 17:0 (limit=15):
		Admira Selimović
		Ana Božinović
		Ena Keser
		Helena Vidović
		Iva Ćorluka
		Josip Orešković
		Jure trogrlic
		Karla Miškić
		Katja Lukić
		Liza Uršič
		Lucija Rakocija
		Nejra Kubat
		Paula Bašić
		Tina Robič
		Zinajda Šabić

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Antonela Kirin
		Grgo Orlović
		Katarina Lelas
		Klara Gabriela Penić
		Luka Paunović
		Marta Novak
		Marta Posavec
		Mateja Novački
		Nera Gržetić
		Nikolina Klarić
		Stipe Ivanda
		Tadeja Blagec
		Teja Trontelj
	Petak u 18:0 - 19:0 (limit=16):
		Andrija Segin
		Magdalena Udovičić
		Marija Dolić
		Marija Razum
	Petak u 19:0 - 20:0 (limit=16):
		Milenko Paponja

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Ana Aračić
		Andreas Ivanovic
		Bozana corluka
		Elizabeta Zorić
		Ena Kulis
		Lucija Ivanac
		Margareta Strihic
		Marija Buotić
		Marta Adam
		Mihaela Budimir
		Sabra Čolić
		Sara Krog
	Petak u 15:30 - 17:0 (limit=14):
		Andrija Segin
		Antonia Milas
		Antonija Jerković
		Božana Knežević
		Dorotea Bralo
		Edina Memiši
		Fran Popić
		Lucija Šarić
		Magdalena Udovičić
		Marina Gadža
		Mihaela Rac
		Mirna Draganja
		Nika Kovačević
		Rene Križ
	Petak u 17:0 - 18:30 (limit=9):
		Anamaria Mazarekić
		Dora Gobin
		Filip Kašner
		Jure trogrlic
		Klara Naka
		Milenko Paponja
		Nora Salkičević
		Sara Kožul

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Andrea Vrankić
		Branimir Ninčević
		Doris Šimac
		Elis Mikac
		Ivna Ćavar
		Katja Kovačević
		Kristina Filipović
		Luka Begonja
		Marina Jardas
		Nina Vovk
		Stela Cerovac
		Tijana Botić
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Antea Cikojevic
		Darko Tevanovic
		Karla Sabolić
		Matea Vučetić
		Mirna Radočaj
		Nikolina Akmadža
	Petak u 15:30 - 17:0 (limit=6):
		Alessia Bučić
		Amra Mahmutović
		Dora Arhanić
		Ines Perić
		Karla Roksandić
		Tessa Anastasja Stankic

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ana Čeko
		Anđela Batur
		Fran Glanz
		Lucija Kovačić
		Magdalena Jurisic
		Marta Furdi
		Marta Novak
	Subota u 15:30 - 17:0 (limit=8):
		Ajla Mustafić
		Domen Tomič
		Iva Pavičić
		Klaudija Kaurić
		Matea Vidov
		Nikolina Dernaj
		Urška Štajdohar
		Viktorija Perčinlić

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Barbara Čulin
		Dejan Prpos
		Emina Dzaferović
		Inga Jukanović
		Iva Ćorluka
		Josip Orešković
		Katarina Lasan
		Lea Grzela
		Petra Lukač
		Tina Matoc
	Petak u 18:0 - 19:0 (limit=10):
		Ana Radan
		Asja Jerlagic
		Dora Alagic
		Ivna Ćavar
		Moorea Kuvačić
		Nina Vovk
		Paula Vršić
		Stela Cerovac
		Svetlana Galić
		Tiffany Ocko
	Petak u 19:0 - 20:0 (limit=12):
		Ante Zelenika
		Anđela Batur
		Anđelika Vučković
		Dora Habek
		Ena Popović
		Ena Roca
		Grgo Orlović
		Karlo Vrbanić
		Katja Kovačević
		Marta Posavec
		Nejra Šapčanin
	Subota u 17:0 - 18:0 (limit=12):
		Ana Pavlović
		Daria Vuk
		Denis Abdihodžić
		Dora Visković
		Ela Tomljanović
		Katarina Lazar
		Kristina Filipović
		Lucija Kovačić
		Marta Smok
		Petra Mihalić
		Tessa Anastasja Stankic
	Subota u 18:0 - 19:0 (limit=10):
		Anja Antea Sokač
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Dunja Ilić
		Iva Barišić
		Ivan Orabović
		Jana Joksimović
		Laura Plančak
		Marija Razum
		Marijeta Dadić
		Milan Tejic
		Ninib malki
		Tina Robič
		Valentina Šalković
		Vladimir Šavija

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Brigić
		Antonia Vranjković
		Denis Abdihodžić
		Ena Popović
		Ivana Čavka
		Karla Miškić
		Karolina Boljkovac
		Lucija Rakocija
		Lucija Ratkovski
		Sara El-Sabeh
		Stela Salopek
		Višnja Šljivac
		Zinajda Šabić

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Antonija Jokić
		Dunja Ilić
		Edina Memiši
		Iva Barišić
		Marija Mastelić
		Monika Dragić
		Sara El-Sabeh
		Sara Jakoliš
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Adna Vlahovljak
		Anja Antea Sokač
		Darko Tevanovic
		Ena Roca
		Harun Mašić
		Karlo Posavec
		Lejla Reizbegović
		Marta Leović
		Nikolina Nazor
		Paula Petrašić
		Sara Jindra
	Subota u 17:0 - 18:30 (limit=11):
		Anamarija Vidovic
		Bozana corluka
		Branimir Ninčević
		Ida Kuštelega
		Jurica Nekić
		Karla Roksandić
		Lea Patekar
		Marija Matijašević
		Marko Ivančević
		Matija Žanetić
		Mihovil Škalic
	Subota u 18:30 - 20:0 (limit=10):
		Darko Đukić
		Ela Prajz
		Ivona Barić
		Jelena Đapić
		Josipa Rokov
		Marija Mastelić
		Matea Čupen
		Milan Tejic
		Nika Fantulin
		blanka dermit

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Anamaria Mazarekić
		Andreas Ivanovic
		Barbara Čulin
		Dora Gobin
		Eva Rožman
		Frano Visković
		Laura Plančak
		Mia Maretić
		Petar Ivaniš
		Sara Kožul
		Stela Salopek

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Bašić
		Ana Perica
		Anja Ljevar
		Eva Krizmanić
		Helena Buljubašić
		Katarina Lelas
		Katarina Pagadur
		Lea Imamović
		Lucija Herceg
		Marija Buotić
		Ninib malki
	Subota u 18:30 - 20:0 (limit=13):
		Admira Selimović
		Dora istenić
		Doris Šimac
		Dorotea Bralo
		Emina Dzaferović
		Ena Keser
		Helena Vidović
		Inga Jukanović
		Matea Dželalija
		Mihaela Hercigonja
		Nikolina Klarić
		Tijana Botić
		Vladimir Šavija

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Barbara Marušić
		Dejana Komlen
		Ivana Pehar
		Lucija Kuštra
		Luka Paunović
		Magdalena Prskalo
		Matea Bubalo
		Mihaela Budimir
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Adis Trešnjo
		Ana Jukić
		Antonela Kirin
		Danijela Sabljo
		Fran Popić
		Karla Bikić
		Karlo Počanić
		Karlo Vrbanić
		Lucija Šarić
		Luka Begonja
		Marija Kelić
		Nika Horvat
		Renata Volf
		Rene Križ
		Svetlana Galić
		Tina Matoc
	Subota u 15:30 - 17:0 (limit=16):
		Iva Kordić
		Ivana Čavka
		Katarina Lasan
		Marija Oreč
		Marijeta Dadić
		Teja Trontelj
	Subota u 17:0 - 18:30 (limit=16):
		Ana Maria Dekanić

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Andrea Vrankić
		Domagoj Ivančić
		Dora Habek
		Filip Kliček
		Iva Plazonić
		Karla Ugrin
	Petak u 18:30 - 20:0 (limit=14):
		Ajla Brigić
		Dejan Prpos

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Ela Tomljanović
		Iva Skvorcov
		Luciia Ribić
		Mona Maarouf
		Rasim Trnjanin
	Subota u 15:30 - 17:0 (limit=6):
		Ana Domitrović
		Mario Gržančić
		Martina Besek
		Mihaela Hercigonja
		Nedžad Osmanović
		Nikolina Mohorovic

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Ana Barać
		Barbara Marušić
		Elia Croner
		Lucija Ratkovski
		Marta Smok
		Nejra Šapčanin
		Nika Supina
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Ana Brajdić
		Elia Croner
		Isabel Pavlović
		Iva Plazonić
		Ivan Frka Šeparović
		Lucia Levar
		Marija Kelić
		Nora Salkičević

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Istenić
		Antonija Jerković
		Barbara Ovčarić
		Jasmina Alendar
		Marina Jardas
		Mateja Novački
		Paula Bašić
		Tiffany Ocko
	Subota u 19:0 - 20:0 (limit=10):
		Ana Barać
		Ana Božinović
		Asja Jerlagic
		Elis Mikac
		Isabel Pavlović
		Karla Bikić
		Karla Ugrin
		Katarina Grcic
		Liza Uršič
		Paula Vršić
