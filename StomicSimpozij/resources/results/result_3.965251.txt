
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Dora istenić
		Dunja Ilić
		Ena Alagic
		Fabio Pažin
		Karla Ugrin
		Mihaela Budimir
		Mihaela Hercigonja
		Milenko Paponja
		Rasim Trnjanin
		lucija hero
	Petak u 15:30 - 17:0 (limit=10):
		Admira Selimović
		Ana Istenić
		Erika Milovec
		Helena Buljubašić
		Ivna Ćavar
		Katarina Pagadur
		Mladen Petkovic
		Monika Vukić
		Saira Omanovic
		Tina Matoc

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Ana Jukić
		Antonela Kirin
		Antonija Jug
		Ela Tomljanović
		Fran Glanz
		Karlo Vrbanić
		Lucia Levar
		Margareta Strihic
		Marta Adam
		Marta Furdi
		Matea Vidov
		Nika Horvat
		Valentina Šalković
	Petak u 18:30 - 20:0 (limit=13):
		Alba Elezović
		Andrej Škerjanc
		Elis Mikac
		Iva Barišić
		Karlo Počanić
		Katarina Grcic
		Liza Uršič
		Luka Begonja
		Marta Posavec
		Mia Škaljac
		Ninib malki
		Saira Omanovic
		blanka dermit

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Amila Balić
		Ana Pavlović
		Ana Čeko
		Andrej Škerjanc
		Dejana Komlen
		Edina Memiši
		Grgo Orlović
		Ivana Pehar
		Klaudija Kaurić
		Lea Grzela
		Lucia Levar
		Luka Paunović
		Magdalena Prskalo
		Mihovil Škalic
		Moorea Kuvačić
		Nera Gržetić

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Anamarija Vidovic
		Barbara Ovčarić
		Danica Cvjetkovic
		Daria Vuk
		Dora Arhanić
		Inga Jukanović
		Katja Lukić
		Lucija Ivanac
		Lucija Kovačić
		Matea Vučetić
		Mirna Radočaj
		Nora Salkičević
		Stela Salopek
		Zinajda Šabić
		lucija hero
	Petak u 18:0 - 19:0 (limit=15):
		Antea Cikojevic
		Anđelika Vučković
		Ivona Barić
		Jelena Đapić
		Katarina Lazar
		LUCIJA RAKO
		Lea Imamović
		Lucija Rakocija
		Marija Buotić
		Marta Leović
		Matija Žanetić
		Maša Matić
		Mihaela Budimir
		Nika Fantulin
		Sara Jindra
	Petak u 19:0 - 20:0 (limit=14):
		Alessia Bučić
		Ana Aračić
		Andrea Vrankić
		Dora istenić
		Iva Plazonić
		Ivan Frka Šeparović
		Jana Joksimović
		Jurica Nekić
		Laura Plančak
		Lucija Kuštra
		Lucija Ratkovski
		Nikolina Dernaj
		Petra Mihalić
		Valentina Marasovic

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Ajla Bašić
		Amra Mahmutović
		Ana Istenić
		Anja Ljevar
		Antonia Vranjković
		Ela Tomljanović
		Erika Milovec
		Ida Kuštelega
		Iva Skvorcov
		Karla Bikić
		Karlo Posavec
		Marijeta Dadić
		Mihaela Rac
		Nejra Kubat
		Zrinka Jemrić
	Petak u 15:30 - 17:0 (limit=14):
		Ana Domitrović
		Dora Alagic
		Ena Popović
		Filip Kliček
		Frano Visković
		Iva Pavičić
		Jana Joksimović
		Laura Plančak
		Marija Mastelić
		Mario Gržančić
		Martina Besek
		Petar Ivaniš
		Tarik Tahirovic
		Tessa Anastasja Stankic

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Alba Elezović
		Antonija Jokić
		Danijela Sabljo
		Domagoj Ivančić
		Domen Tomič
		Helena Vidović
		Iva Kordić
		Karolina Boljkovac
		Katja Lukić
		Lucija Herceg
		Marina Gadža
		Marko Ivančević
		Mona Maarouf
		Nikolina Mohorovic
		Sara Jakoliš
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Mustafić
		Ana Božinović
		Ana Perica
		Branimir Ninčević
		Harun Mašić
		Josip Orešković
		Lejla Reizbegović
		Lucija Rakocija
		Marija Dolić
		Monika Dragić
		Nedžad Osmanović
		Rebecca Beissmann
		Tina Robič
		Urška Štajdohar
	Subota u 14:0 - 15:30 (limit=14):
		Admira Selimović
		Božana Knežević
		Dora Habek
		Elizabeta Zorić
		Iva Ćorluka
		Karla Jurković
		Luciia Ribić
		Matea Dželalija
		Matea Čupen
		Mirna Radočaj
		Monika Vukić
		Petra Lukač
		Renata Volf
		Zorana Mavija
	Subota u 15:30 - 17:0 (limit=15):
		Darko Đukić
		Ela Prajz
		Ena Keser
		Eva Rožman
		Filip Kašner
		Ivna Ćavar
		Jure trogrlic
		Karla Miškić
		Katja Kovačević
		Nina Vovk
		Paula Bašić
		Stanija Jovanovic
		Stipe Ivanda
		Tadeja Blagec
		Teja Trontelj

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Ivan Orabović
		Katarina Lelas
		Luka Paunović
		Marta Novak
		Marta Posavec
		Mihovil Škalic
		Nikolina Klarić
		Stanija Jovanovic
		Stipe Ivanda
		Tadeja Blagec
		Teja Trontelj
	Petak u 18:0 - 19:0 (limit=16):
		Andrija Segin
		Klara Gabriela Penić
		Magdalena Udovičić
		Marija Dolić
		Marija Razum
	Petak u 19:0 - 20:0 (limit=16):
		Milenko Paponja
		Valentina Šalković

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Ana Aračić
		Antonia Milas
		Bozana corluka
		Ena Kulis
		Jure trogrlic
		Klara Naka
		Margareta Strihic
		Marta Adam
		Matea Bubalo
		Matea Vidov
		Mia Škaljac
		Sabra Čolić
	Petak u 15:30 - 17:0 (limit=14):
		Adna Vlahovljak
		Andreas Ivanovic
		Andrija Segin
		Darko Đukić
		Dora Habek
		Eva Rožman
		Ida Kuštelega
		Iva Skvorcov
		Luka Begonja
		Magdalena Udovičić
		Mirna Draganja
		Nika Kovačević
		Nika Supina
		Sara Jindra
	Petak u 17:0 - 18:30 (limit=9):
		Debora Kanižaj
		Edina Memiši
		Elizabeta Zorić
		Eva Krizmanić
		Filip Kašner
		Lea Grzela
		Lucija Šarić
		Moorea Kuvačić
		Rene Križ

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Ana Maria Dekanić
		Andrea Vrankić
		Ela Prajz
		Marina Jardas
		Nikolina Nazor
		Tijana Botić
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Ana Radan
		Antonija Jerković
		Darko Tevanovic
		Dora Arhanić
		Karla Roksandić
		Stela Salopek
	Petak u 15:30 - 17:0 (limit=6):
		Amra Mahmutović
		Antea Cikojevic
		Ines Perić
		Karla Sabolić
		Milan Tejic
		Nika Fantulin

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Anđela Batur
		Domen Tomič
		Lucija Kovačić
		Lucija Kuštra
		Marta Furdi
		Svetlana Galić
		Viktorija Perčinlić
	Subota u 15:30 - 17:0 (limit=8):
		Adis Trešnjo
		Ajla Mustafić
		Ana Čeko
		Dejan Prpos
		Iva Pavičić
		Matea Vučetić
		Tiffany Ocko
		Urška Štajdohar

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Asja Jerlagic
		Denis Abdihodžić
		Emina Dzaferović
		Marta Smok
		Nikolina Akmadža
		Nikolina Dernaj
		Paula Vršić
		Petra Lukač
		Petra Mihalić
		Stela Cerovac
	Petak u 18:0 - 19:0 (limit=10):
		Dejan Prpos
		Dora Alagic
		Ena Popović
		Katja Kovačević
		Lea Patekar
		Mirna Draganja
		Sara Krog
		Tessa Anastasja Stankic
		Tina Matoc
	Petak u 19:0 - 20:0 (limit=12):
		Ana Radan
		Anja Antea Sokač
		Anđela Batur
		Branimir Ninčević
		Daria Vuk
		Dora Visković
		Ena Roca
		Ivan Orabović
		Lucija Ivanac
		Marta Novak
		Mia Maretić
		Nikolina Nazor
	Subota u 17:0 - 18:0 (limit=12):
		Ante Zelenika
		Barbara Čulin
		Božena Sorić
		Katarina Lazar
		Nejra Šapčanin
		Svetlana Galić
		Zinajda Šabić
	Subota u 18:0 - 19:0 (limit=10):
		Tiffany Ocko
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Anamaria Mazarekić
		Anja Antea Sokač
		Dorotea Bralo
		Iva Barišić
		Katarina Lasan
		Klara Gabriela Penić
		LUCIJA RAKO
		Marija Razum
		Marijeta Dadić
		Matea Čupen
		Ninib malki
		Sara Kožul
		Vladimir Šavija

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Brigić
		Antonia Milas
		Antonia Vranjković
		Barbara Čulin
		Dora Visković
		Iva Kordić
		Karla Miškić
		Karla Roksandić
		Karolina Boljkovac
		Lucija Ratkovski
		Paula Petrašić
		Sara El-Sabeh
		Valentina Marasovic
		Višnja Šljivac

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Ana Brajdić
		Katarina Lasan
		Marija Matijašević
		Martina Besek
		Monika Dragić
		Sara El-Sabeh
		Sara Jakoliš
		Višnja Šljivac
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Ana Domitrović
		Bozana corluka
		Grgo Orlović
		Marina Gadža
		Marko Ivančević
		Marta Leović
		Matija Žanetić
		Milan Tejic
		Mona Maarouf
		Nera Gržetić
		Tarik Tahirovic
	Subota u 17:0 - 18:30 (limit=11):
		Alessia Bučić
		Darko Tevanovic
		Filip Kliček
		Harun Mašić
		Ivan Frka Šeparović
		Josipa Rokov
		Karla Bikić
		Mario Gržančić
		Nedžad Osmanović
		Nikolina Klarić
		Vladimir Šavija
	Subota u 18:30 - 20:0 (limit=10):
		Iva Ćorluka
		Jurica Nekić
		Karlo Počanić
		Katarina Grcic
		Luciia Ribić
		Marija Matijašević
		Matea Dželalija
		Mia Maretić
		Nikola Matijevic
		Paula Petrašić

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Anamaria Mazarekić
		Andreas Ivanovic
		Anđelika Vučković
		Dora Gobin
		Doris Šimac
		Frano Visković
		Josip Orešković
		Nika Kovačević
		Rebecca Beissmann
		Sara Kožul
		blanka dermit

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ana Perica
		Anja Ljevar
		Asja Jerlagic
		Emina Dzaferović
		Eva Krizmanić
		Iva Plazonić
		Katarina Lelas
		Lea Imamović
		Marija Buotić
		Mihaela Hercigonja
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Brigić
		Božena Sorić
		Dorotea Bralo
		Ena Keser
		Fran Glanz
		Helena Buljubašić
		Katarina Pagadur
		Lea Patekar
		Maša Matić
		Nika Supina
		Nina Vovk
		Paula Vršić
		Rasim Trnjanin

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Ana Jukić
		Ena Kulis
		Ivana Pehar
		Ivana Čavka
		Klara Naka
		Magdalena Prskalo
		Matea Bubalo
		Nora Salkičević
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Adna Vlahovljak
		Antonela Kirin
		Antonija Jug
		Danica Cvjetkovic
		Danijela Sabljo
		Denis Abdihodžić
		Fran Popić
		Inga Jukanović
		Ivana Čavka
		Jasmina Alendar
		Karlo Vrbanić
		Lucija Šarić
		Marija Kelić
		Marija Mastelić
		Nikolina Akmadža
		Tina Robič
	Subota u 15:30 - 17:0 (limit=16):
		Ana Božinović
		Ana Maria Dekanić
		Dunja Ilić
		Ena Alagic
		Fabio Pažin
		Ines Perić
		Karla Sabolić
		Lejla Reizbegović
		Marija Oreč
		Mateja Novački
		Nika Horvat
		Rene Križ
		Sara Krog
	Subota u 17:0 - 18:30 (limit=16):
		Karla Jurković
		Renata Volf
		Viktorija Perčinlić

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Doris Šimac
		Fran Popić
		Isabel Pavlović
		Karla Ugrin
	Petak u 18:30 - 20:0 (limit=14):
		Ante Zelenika
		Dora Gobin

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Karlo Posavec
		Lucija Herceg
		Nejra Kubat
		Nikolina Mohorovic
		Petar Ivaniš
	Subota u 15:30 - 17:0 (limit=6):
		Ajla Bašić
		Antonija Jokić
		Domagoj Ivančić
		Helena Vidović
		Kristina Filipović
		Mihaela Rac

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Ana Barać
		Barbara Marušić
		Elia Croner
		Josipa Rokov
		Klaudija Kaurić
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Anamarija Vidovic
		Barbara Marušić
		Debora Kanižaj
		Dejana Komlen
		Isabel Pavlović
		Ivona Barić
		Jelena Đapić
		Marija Kelić
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Brajdić
		Barbara Ovčarić
		Kristina Filipović
		Marina Jardas
		Mateja Novački
		Mladen Petkovic
		Paula Bašić
		Stela Cerovac
	Subota u 19:0 - 20:0 (limit=10):
		Ana Barać
		Antonija Jerković
		Božana Knežević
		Elia Croner
		Elis Mikac
		Ena Roca
		Jasmina Alendar
		Liza Uršič
		Magdalena Jurisic
		Marija Oreč
