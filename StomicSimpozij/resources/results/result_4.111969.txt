
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Andrej Škerjanc
		Antonija Jug
		Božena Sorić
		Dejan Prpos
		Dora istenić
		Dorotea Bralo
		Karlo Počanić
		Mihaela Budimir
		Mihovil Škalic
		Rebecca Beissmann
	Petak u 15:30 - 17:0 (limit=10):
		Danica Cvjetkovic
		Erika Milovec
		Helena Vidović
		Jasmina Alendar
		Katarina Pagadur
		Moorea Kuvačić
		Saira Omanovic
		Tarik Tahirovic
		Tina Matoc
		Višnja Šljivac

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Andrej Škerjanc
		Antonela Kirin
		Antonija Jug
		Debora Kanižaj
		Dora Gobin
		Ela Tomljanović
		Fran Glanz
		Ivan Orabović
		Margareta Strihic
		Marija Kelić
		Marta Furdi
		Valentina Šalković
		blanka dermit
	Petak u 18:30 - 20:0 (limit=13):
		Daria Vuk
		Doris Šimac
		Erika Milovec
		Karla Bikić
		Karlo Počanić
		Marija Matijašević
		Marta Adam
		Marta Novak
		Matea Vidov
		Mihaela Rac
		Monika Vukić
		Moorea Kuvačić
		Saira Omanovic

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Ana Jukić
		Ana Čeko
		Denis Abdihodžić
		Elis Mikac
		Grgo Orlović
		Iva Pavičić
		Ivana Pehar
		Jelena Đapić
		Karla Ugrin
		Klara Naka
		Klaudija Kaurić
		Lucia Levar
		Luka Paunović
		Magdalena Prskalo
		Milenko Paponja
		Nera Gržetić

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Ana Aračić
		Antea Cikojevic
		Dorotea Bralo
		Helena Buljubašić
		Ivona Barić
		Jana Joksimović
		Josipa Rokov
		Katja Lukić
		Matea Vučetić
		Mladen Petkovic
		Nika Fantulin
		Nika Supina
		Sara Jindra
		Stela Salopek
		Zinajda Šabić
	Petak u 18:0 - 19:0 (limit=15):
		Andrija Segin
		Božena Sorić
		Dora Visković
		Ena Kulis
		Iva Barišić
		Ivan Frka Šeparović
		Jelena Đapić
		Jurica Nekić
		Katarina Grcic
		Lea Imamović
		Lucija Kuštra
		Lucija Rakocija
		Lucija Ratkovski
		Matija Žanetić
		Nikolina Dernaj
	Petak u 19:0 - 20:0 (limit=14):
		Alessia Bučić
		Andrea Vrankić
		Dora Arhanić
		Dora istenić
		Fabio Pažin
		Liza Uršič
		Lucija Ivanac
		Lucija Kovačić
		Matea Bubalo
		Matea Dželalija
		Maša Matić
		Mihaela Budimir
		Paula Petrašić
		lucija hero

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Anja Ljevar
		Antonia Vranjković
		Dunja Ilić
		Ela Tomljanović
		Ida Kuštelega
		Jana Joksimović
		Jure trogrlic
		Karla Bikić
		Mirna Draganja
		Ninib malki
		Petar Ivaniš
		Tarik Tahirovic
		Viktorija Perčinlić
		Zrinka Jemrić
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Brigić
		Ana Brajdić
		Ana Domitrović
		Barbara Čulin
		Eva Krizmanić
		Karla Jurković
		Karlo Posavec
		Katarina Lasan
		Marija Mastelić
		Marija Oreč
		Mario Gržančić
		Martina Besek
		Nejra Kubat
		Nikolina Dernaj

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Admira Selimović
		Ana Božinović
		Ana Maria Dekanić
		Darko Đukić
		Filip Kašner
		Josip Orešković
		Katja Lukić
		Lejla Reizbegović
		Luciia Ribić
		Marina Gadža
		Monika Dragić
		Stanija Jovanovic
		Stipe Ivanda
		Teja Trontelj
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Domagoj Ivančić
		Domen Tomič
		Ela Prajz
		Eva Rožman
		Iva Kordić
		Liza Uršič
		Lucija Rakocija
		Marija Dolić
		Marko Ivančević
		Mona Maarouf
		Nedžad Osmanović
		Nikola Matijevic
		Renata Volf
		Tina Robič
	Subota u 14:0 - 15:30 (limit=14):
		Ana Perica
		Božana Knežević
		Danijela Sabljo
		Filip Kliček
		Helena Vidović
		Iva Ćorluka
		Karla Jurković
		Karla Miškić
		Matea Dželalija
		Monika Vukić
		Paula Bašić
		Petra Lukač
		Rebecca Beissmann
		Sara Jakoliš
	Subota u 15:30 - 17:0 (limit=15):
		Ajla Mustafić
		Alba Elezović
		Antonija Jokić
		Anđela Batur
		Dora Habek
		Elizabeta Zorić
		Harun Mašić
		Ivna Ćavar
		Karolina Boljkovac
		Lucija Herceg
		Nikolina Mohorovic
		Nina Vovk
		Sara Krog
		Tadeja Blagec
		Valentina Marasovic

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Elizabeta Zorić
		Katarina Lelas
		Luka Paunović
		Marija Razum
		Marina Gadža
		Marta Leović
		Marta Posavec
		Mateja Novački
		Nikolina Klarić
		Tadeja Blagec
		Vladimir Šavija
	Petak u 18:0 - 19:0 (limit=16):
		Klara Gabriela Penić
		Marija Dolić
	Petak u 19:0 - 20:0 (limit=16):
		Ivan Orabović
		Valentina Šalković

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Adna Vlahovljak
		Ana Aračić
		Andrija Segin
		Bozana corluka
		Ena Kulis
		Filip Kliček
		Lucija Ivanac
		Lucija Šarić
		Margareta Strihic
		Marta Adam
		Matea Vidov
		Sabra Čolić
	Petak u 15:30 - 17:0 (limit=14):
		Ana Perica
		Andreas Ivanovic
		Antonia Milas
		Antonija Jerković
		Božana Knežević
		Dora Habek
		Frano Visković
		Iva Skvorcov
		Marija Buotić
		Mia Škaljac
		Mihaela Rac
		Mirna Draganja
		Nika Kovačević
		Nora Salkičević
	Petak u 17:0 - 18:30 (limit=9):
		Amila Balić
		Ana Istenić
		Edina Memiši
		Filip Kašner
		Fran Popić
		Ida Kuštelega
		Luka Begonja
		Magdalena Udovičić
		Milenko Paponja

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Branimir Ninčević
		Doris Šimac
		Luka Begonja
		Marina Jardas
		Tessa Anastasja Stankic
		Tijana Botić
		lucija hero
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Darko Tevanovic
		Katarina Lazar
		Matea Vučetić
		Stela Salopek
		Zinajda Šabić
		blanka dermit
	Petak u 15:30 - 17:0 (limit=6):
		Amra Mahmutović
		Dora Alagic
		Dora Visković
		Iva Barišić
		Ivana Čavka
		Milan Tejic

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Adis Trešnjo
		Domen Tomič
		Iva Pavičić
		Josip Orešković
		Klaudija Kaurić
		Lucija Kovačić
		Urška Štajdohar
	Subota u 15:30 - 17:0 (limit=8):
		Ana Čeko
		Denis Abdihodžić
		Fran Glanz
		Katarina Lazar
		Lucia Levar
		Lucija Kuštra
		Marta Novak
		Matea Bubalo

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Alba Elezović
		Asja Jerlagic
		Daria Vuk
		Dejan Prpos
		Inga Jukanović
		Karlo Vrbanić
		Lea Patekar
		Mihaela Hercigonja
		Petra Lukač
		Sara Krog
	Petak u 18:0 - 19:0 (limit=10):
		Ana Radan
		Anja Antea Sokač
		Barbara Čulin
		Iva Ćorluka
		Ivna Ćavar
		Lea Grzela
		Mia Maretić
		Paula Vršić
	Petak u 19:0 - 20:0 (limit=12):
		Ana Pavlović
		Ante Zelenika
		Dora Alagic
		Emina Dzaferović
		Marta Leović
		Marta Posavec
		Marta Smok
		Nejra Šapčanin
		Nikolina Nazor
		Petar Ivaniš
		Stela Cerovac
	Subota u 17:0 - 18:0 (limit=12):
		Anđelika Vučković
		Ena Popović
		Kristina Filipović
		Marija Razum
		Marta Furdi
		Nikolina Klarić
		Petra Mihalić
		Tessa Anastasja Stankic
		Tiffany Ocko
		Urška Štajdohar
	Subota u 18:0 - 19:0 (limit=10):
		Anđela Batur
		Ena Roca
		Nikolina Akmadža
		Svetlana Galić
	Subota u 19:0 - 20:0 (limit=10):
		Katja Kovačević

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Anamaria Mazarekić
		Anamarija Vidovic
		Anja Antea Sokač
		Dejana Komlen
		Karla Roksandić
		Klara Gabriela Penić
		LUCIJA RAKO
		Nika Horvat
		Ninib malki
		Petra Mihalić
		Rasim Trnjanin
		Sara Kožul

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ena Popović
		Jure trogrlic
		Karolina Boljkovac
		Laura Plančak
		Marijeta Dadić
		Matea Čupen
		Rene Križ
		Sara El-Sabeh
		Valentina Marasovic

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Anamarija Vidovic
		Edina Memiši
		Ela Prajz
		Marija Matijašević
		Marijeta Dadić
		Mirna Radočaj
		Monika Dragić
		Sara El-Sabeh
		Višnja Šljivac

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Bozana corluka
		Darko Tevanovic
		Darko Đukić
		Ena Roca
		Grgo Orlović
		Jurica Nekić
		Matea Čupen
		Matija Žanetić
		Nera Gržetić
		Nikolina Nazor
		Sara Jindra
	Subota u 17:0 - 18:30 (limit=11):
		Adna Vlahovljak
		Alessia Bučić
		Branimir Ninčević
		Inga Jukanović
		Josipa Rokov
		Karla Roksandić
		Katarina Lasan
		Katja Kovačević
		Marija Mastelić
		Mihovil Škalic
		Milan Tejic
	Subota u 18:30 - 20:0 (limit=10):
		Dora Arhanić
		Fran Popić
		Karlo Posavec
		Marko Ivančević
		Mona Maarouf
		Nika Fantulin
		Paula Petrašić
		Paula Vršić
		Sara Kožul
		Teja Trontelj

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Andreas Ivanovic
		Antea Cikojevic
		Anđelika Vučković
		Dora Gobin
		Frano Visković
		Iva Skvorcov
		Klara Naka
		LUCIJA RAKO
		Laura Plančak
		Mia Maretić
		Nika Kovačević

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Brigić
		Anja Ljevar
		Asja Jerlagic
		Eva Krizmanić
		Helena Buljubašić
		Katarina Lelas
		Katarina Pagadur
		Lea Imamović
		Mihaela Hercigonja
		Nika Supina
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Bašić
		Ajla Mustafić
		Ena Keser
		Harun Mašić
		Lea Grzela
		Lea Patekar
		Lucija Herceg
		Marija Buotić
		Mirna Radočaj
		Nikolina Mohorovic
		Nina Vovk
		Rasim Trnjanin
		Vladimir Šavija

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Antonia Vranjković
		Dejana Komlen
		Ena Alagic
		Ines Perić
		Ivana Pehar
		Mia Škaljac
		Nejra Kubat
		Nora Salkičević
		Stipe Ivanda
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Ana Domitrović
		Ana Jukić
		Antonela Kirin
		Antonia Milas
		Danica Cvjetkovic
		Ena Alagic
		Ines Perić
		Ivana Čavka
		Karla Sabolić
		Magdalena Prskalo
		Marija Kelić
		Marija Oreč
		Nika Horvat
		Renata Volf
		Tina Matoc
		Tina Robič
	Subota u 15:30 - 17:0 (limit=16):
		Dunja Ilić
		Ena Keser
		Iva Kordić
		Karlo Vrbanić
		Lejla Reizbegović
		Lucija Šarić
		Magdalena Udovičić
		Mateja Novački
		Nikolina Akmadža
		Rene Križ
		Svetlana Galić
		Viktorija Perčinlić
	Subota u 17:0 - 18:30 (limit=16):
		Ana Maria Dekanić
		Danijela Sabljo

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Andrea Vrankić
		Ante Zelenika
		Emina Dzaferović
		Iva Plazonić
		Karla Ugrin
		Maša Matić
	Petak u 18:30 - 20:0 (limit=14):
		Ajla Bašić

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Admira Selimović
		Ana Božinović
		Kristina Filipović
		Luciia Ribić
		Mario Gržančić
	Subota u 15:30 - 17:0 (limit=6):
		Anamaria Mazarekić
		Domagoj Ivančić
		Eva Rožman
		Nedžad Osmanović
		Stanija Jovanovic
		Zrinka Jemrić

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Ana Barać
		Barbara Marušić
		Elia Croner
		Lucija Ratkovski
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Amila Balić
		Barbara Marušić
		Barbara Ovčarić
		Debora Kanižaj
		Fabio Pažin
		Isabel Pavlović
		Iva Plazonić
		Ivan Frka Šeparović
		Karla Sabolić
		Katarina Grcic
		Martina Besek
		Mladen Petkovic
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Barać
		Ana Brajdić
		Ana Istenić
		Elia Croner
		Elis Mikac
		Jasmina Alendar
		Magdalena Jurisic
		Marina Jardas
	Subota u 19:0 - 20:0 (limit=10):
		Antonija Jerković
		Antonija Jokić
		Barbara Ovčarić
		Isabel Pavlović
		Ivona Barić
		Karla Miškić
		Paula Bašić
		Sara Jakoliš
		Stela Cerovac
		Tiffany Ocko
