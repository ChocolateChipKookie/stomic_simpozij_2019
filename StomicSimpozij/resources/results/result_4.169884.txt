
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Danica Cvjetkovic
		Dora istenić
		Dorotea Bralo
		Edina Memiši
		Fabio Pažin
		Jasmina Alendar
		Karlo Vrbanić
		Marina Jardas
		Mladen Petkovic
		lucija hero
	Petak u 15:30 - 17:0 (limit=10):
		Admira Selimović
		Ajla Mustafić
		Ana Maria Dekanić
		Iva Skvorcov
		Ivna Ćavar
		Lejla Reizbegović
		Matea Dželalija
		Rasim Trnjanin
		Tarik Tahirovic
		Tijana Botić

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Ana Jukić
		Andrej Škerjanc
		Antonela Kirin
		Dora Gobin
		Ela Tomljanović
		Frano Visković
		Isabel Pavlović
		Karlo Počanić
		Karlo Vrbanić
		Monika Vukić
		Sabra Čolić
		Saira Omanovic
		Tina Matoc
	Petak u 18:30 - 20:0 (limit=13):
		Alba Elezović
		Ana Pavlović
		Doris Šimac
		LUCIJA RAKO
		Liza Uršič
		Lucia Levar
		Marta Posavec
		Mihaela Rac
		Ninib malki
		Paula Bašić
		Rene Križ
		Stanija Jovanovic
		Valentina Šalković

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Amila Balić
		Andrej Škerjanc
		Dejana Komlen
		Grgo Orlović
		Ivana Pehar
		Jelena Đapić
		Karla Ugrin
		Klara Gabriela Penić
		Klaudija Kaurić
		Lea Grzela
		Lucija Šarić
		Magdalena Jurisic
		Magdalena Prskalo
		Mihovil Škalic
		Moorea Kuvačić
		Saira Omanovic

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Antonija Jerković
		Daria Vuk
		Jelena Đapić
		Jurica Nekić
		Karla Sabolić
		Katarina Grcic
		Katja Lukić
		Marija Buotić
		Marta Leović
		Matea Bubalo
		Mirna Radočaj
		Nika Fantulin
		Sara Jindra
		Stela Salopek
		Valentina Marasovic
	Petak u 18:0 - 19:0 (limit=15):
		Andrea Vrankić
		Anđelika Vučković
		Dora Arhanić
		Dora Visković
		Iva Barišić
		Ivan Frka Šeparović
		Jana Joksimović
		Katarina Lazar
		Laura Plančak
		Lucija Kovačić
		Lucija Kuštra
		Lucija Ratkovski
		Maša Matić
		Zinajda Šabić
		lucija hero
	Petak u 19:0 - 20:0 (limit=14):
		Alessia Bučić
		Andrija Segin
		Antea Cikojevic
		Danica Cvjetkovic
		Dora istenić
		Dorotea Bralo
		Ena Kulis
		Ivona Barić
		Luka Begonja
		Matija Žanetić
		Nikolina Dernaj
		Nora Salkičević
		Paula Petrašić
		Sara Kožul

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Anja Ljevar
		Antonia Vranjković
		Barbara Čulin
		Ela Tomljanović
		Erika Milovec
		Eva Krizmanić
		Ida Kuštelega
		Jure trogrlic
		Karla Bikić
		Martina Besek
		Maša Matić
		Ninib malki
		Paula Petrašić
		Petar Ivaniš
		Tarik Tahirovic
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Brigić
		Amra Mahmutović
		Andreas Ivanovic
		Antea Cikojevic
		Dunja Ilić
		Fran Popić
		Iva Pavičić
		Jana Joksimović
		Karlo Posavec
		Marija Mastelić
		Mario Gržančić
		Nejra Kubat
		Nikolina Dernaj
		Zrinka Jemrić

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Ana Perica
		Božana Knežević
		Danijela Sabljo
		Darko Đukić
		Eva Rožman
		Filip Kašner
		Helena Vidović
		Iva Ćorluka
		Katja Lukić
		Lucija Herceg
		Marko Ivančević
		Monika Dragić
		Nedžad Osmanović
		Rebecca Beissmann
		Sara Jakoliš
	Petak u 15:30 - 17:0 (limit=14):
		Alba Elezović
		Ana Božinović
		Domen Tomič
		Ela Prajz
		Jure trogrlic
		Marija Dolić
		Marina Gadža
		Nikola Matijevic
		Paula Bašić
		Stanija Jovanovic
		Stipe Ivanda
		Tina Robič
		Zinajda Šabić
		Zorana Mavija
	Subota u 14:0 - 15:30 (limit=14):
		Admira Selimović
		Ajla Mustafić
		Elizabeta Zorić
		Harun Mašić
		Iva Kordić
		Ivna Ćavar
		Karla Miškić
		Luciia Ribić
		Matea Dželalija
		Matea Čupen
		Monika Vukić
		Nina Vovk
		Sara Krog
		Teja Trontelj
	Subota u 15:30 - 17:0 (limit=15):
		Ana Aračić
		Antonija Jokić
		Daria Vuk
		Domagoj Ivančić
		Dora Habek
		Filip Kliček
		Karla Jurković
		Karolina Boljkovac
		Lejla Reizbegović
		Liza Uršič
		Lucija Rakocija
		Mona Maarouf
		Nikolina Mohorovic
		Petra Lukač
		Tadeja Blagec

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Ana Pavlović
		Ana Čeko
		Katarina Lelas
		Luka Paunović
		Magdalena Udovičić
		Marija Razum
		Marta Novak
		Marta Posavec
		Matea Vidov
		Mateja Novački
		Mihovil Škalic
		Nikolina Klarić
		Tadeja Blagec
		Teja Trontelj
	Petak u 18:0 - 19:0 (limit=16):
		Adis Trešnjo
		Ivan Orabović
		Klara Gabriela Penić
		Marija Dolić
		Nera Gržetić
		blanka dermit
	Petak u 19:0 - 20:0 (limit=16):
		Sara El-Sabeh

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Adna Vlahovljak
		Andrija Segin
		Bozana corluka
		Dora Alagic
		Elizabeta Zorić
		Ena Kulis
		Filip Kliček
		Matija Žanetić
		Mihaela Budimir
		Mihaela Rac
		Nikolina Mohorovic
		Sara Krog
	Petak u 15:30 - 17:0 (limit=14):
		Ana Aračić
		Barbara Ovčarić
		Debora Kanižaj
		Ida Kuštelega
		Katarina Pagadur
		Klara Naka
		Lucija Ivanac
		Margareta Strihic
		Marija Oreč
		Marta Adam
		Milenko Paponja
		Nera Gržetić
		Nika Kovačević
		Tiffany Ocko
	Petak u 17:0 - 18:30 (limit=9):
		Ana Istenić
		Antonia Milas
		Eva Rožman
		Filip Kašner
		Karla Jurković
		Mia Škaljac
		Mirna Draganja
		Mona Maarouf
		Moorea Kuvačić

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Branimir Ninčević
		Katja Kovačević
		Nikolina Nazor
		Renata Volf
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Antonija Jerković
		Iva Barišić
		Katarina Grcic
		Milan Tejic
		Mirna Radočaj
		Nejra Šapčanin
	Petak u 15:30 - 17:0 (limit=6):
		Darko Tevanovic
		Dora Arhanić
		Dora Visković
		Ines Perić
		Katarina Lazar
		Laura Plančak

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ana Barać
		Fran Glanz
		Josip Orešković
		Klaudija Kaurić
		Lucija Kovačić
		Lucija Kuštra
		Matea Bubalo
	Subota u 15:30 - 17:0 (limit=8):
		Adis Trešnjo
		Anđela Batur
		Božena Sorić
		Dejan Prpos
		Domen Tomič
		Iva Pavičić
		Magdalena Jurisic
		Svetlana Galić

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Ante Zelenika
		Barbara Čulin
		Božena Sorić
		Denis Abdihodžić
		Lucia Levar
		Marta Smok
		Nedžad Osmanović
		Nikolina Akmadža
		Stela Cerovac
		Svetlana Galić
	Petak u 18:0 - 19:0 (limit=10):
		Ana Radan
		Asja Jerlagic
		Branimir Ninčević
		Dora Habek
		Inga Jukanović
		Kristina Filipović
		Lea Patekar
		Nika Kovačević
		Petra Lukač
		Tiffany Ocko
	Petak u 19:0 - 20:0 (limit=12):
		Dejan Prpos
		Dora Alagic
		Elis Mikac
		Ena Roca
		Katja Kovačević
		Marija Razum
		Marta Furdi
		Mirna Draganja
		Nikolina Nazor
		Petra Mihalić
		Urška Štajdohar
	Subota u 17:0 - 18:0 (limit=12):
		Ana Perica
		Emina Dzaferović
		Ena Popović
		Fran Glanz
		Marta Novak
		Mia Maretić
		Nejra Šapčanin
		Paula Vršić
		Tessa Anastasja Stankic
	Subota u 18:0 - 19:0 (limit=10):
		Anja Antea Sokač
		Anđela Batur
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Domitrović
		Ana Radan
		Anamaria Mazarekić
		Anamarija Vidovic
		Helena Buljubašić
		Iva Kordić
		Ivan Orabović
		Karla Roksandić
		Katarina Lasan
		Marijeta Dadić
		Matea Čupen
		Nika Horvat
		Valentina Šalković

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Bašić
		Antonia Vranjković
		Ivana Čavka
		Karolina Boljkovac
		LUCIJA RAKO
		Lucija Rakocija
		Paula Vršić
		Sara El-Sabeh
		Višnja Šljivac

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Dunja Ilić
		Ela Prajz
		Karla Roksandić
		Katarina Lasan
		Marija Matijašević
		Marta Adam
		Matea Vučetić
		Monika Dragić
		Viktorija Perčinlić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Anja Antea Sokač
		Darko Đukić
		Ena Roca
		Grgo Orlović
		Iva Ćorluka
		Jurica Nekić
		Lucija Šarić
		Marija Mastelić
		Marko Ivančević
		Marta Leović
		Sara Jindra
	Subota u 17:0 - 18:30 (limit=11):
		Alessia Bučić
		Amra Mahmutović
		Andrea Vrankić
		Darko Tevanovic
		Doris Šimac
		Fran Popić
		Josipa Rokov
		Karlo Počanić
		Lea Grzela
		Luka Begonja
		Mario Gržančić
	Subota u 18:30 - 20:0 (limit=10):
		Adna Vlahovljak
		Ana Domitrović
		Bozana corluka
		Harun Mašić
		Ivona Barić
		Karla Miškić
		Luciia Ribić
		Marina Gadža
		Nejra Kubat
		Vladimir Šavija

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Ana Božinović
		Anamaria Mazarekić
		Andreas Ivanovic
		Anđelika Vučković
		Dora Gobin
		Frano Visković
		Iva Skvorcov
		Klara Naka
		Mia Maretić
		Petar Ivaniš
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Anja Ljevar
		Asja Jerlagic
		Katarina Lelas
		Lucija Herceg
		Marija Buotić
		Marta Furdi
		Nikolina Klarić
		Nina Vovk
		Petra Mihalić
		Rasim Trnjanin
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Ana Čeko
		Ena Popović
		Helena Buljubašić
		Inga Jukanović
		Iva Plazonić
		Katarina Pagadur
		Lea Imamović
		Lea Patekar
		Matea Vučetić
		Mihaela Hercigonja
		Nika Supina
		Tessa Anastasja Stankic
		blanka dermit

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Ana Jukić
		Dejana Komlen
		Ivana Pehar
		Luka Paunović
		Magdalena Prskalo
		Mihaela Budimir
		Nora Salkičević
		Stipe Ivanda
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Ana Maria Dekanić
		Antonela Kirin
		Antonia Milas
		Danijela Sabljo
		Denis Abdihodžić
		Ines Perić
		Ivana Čavka
		Karla Bikić
		Lucija Ivanac
		Marija Kelić
		Marijeta Dadić
		Mia Škaljac
		Nika Horvat
		Nikolina Akmadža
		Rene Križ
		Tina Robič
	Subota u 15:30 - 17:0 (limit=16):
		Antonija Jug
		Božana Knežević
		Ena Alagic
		Ena Keser
		Fabio Pažin
		Karla Sabolić
		Magdalena Udovičić
		Marija Oreč
		Nika Fantulin
		Renata Volf
		Sara Kožul
		Tina Matoc
		Valentina Marasovic
	Subota u 17:0 - 18:30 (limit=16):
		Josip Orešković
		Viktorija Perčinlić

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ana Brajdić
		Emina Dzaferović
		Karla Ugrin
	Petak u 18:30 - 20:0 (limit=14):
		Ajla Brigić
		Ante Zelenika
		Matea Vidov
		Milenko Paponja
		Stela Salopek

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Ajla Bašić
		Karlo Posavec
		Kristina Filipović
		Milan Tejic
		Zrinka Jemrić
	Subota u 15:30 - 17:0 (limit=6):
		Erika Milovec
		Helena Vidović
		Martina Besek
		Mihaela Hercigonja
		Sara Jakoliš
		Vladimir Šavija

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Barbara Marušić
		Elia Croner
		Eva Krizmanić
		Josipa Rokov
		Lea Imamović
		Lucija Ratkovski
		Marta Smok
		Nika Supina
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Ana Barać
		Barbara Marušić
		Debora Kanižaj
		Edina Memiši
		Isabel Pavlović
		Iva Plazonić
		Ivan Frka Šeparović
		Marija Kelić
		Mladen Petkovic
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Brajdić
		Anamarija Vidovic
		Domagoj Ivančić
		Jasmina Alendar
		Marija Matijašević
		Marina Jardas
		Mateja Novački
		Stela Cerovac
	Subota u 19:0 - 20:0 (limit=10):
		Ana Istenić
		Antonija Jokić
		Antonija Jug
		Barbara Ovčarić
		Elia Croner
		Elis Mikac
		Ena Alagic
		Ena Keser
		Margareta Strihic
		Urška Štajdohar
