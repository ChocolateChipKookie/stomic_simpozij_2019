
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Antonija Jug
		Danica Cvjetkovic
		Dora istenić
		Dorotea Bralo
		Magdalena Udovičić
		Mateja Novački
		Mihaela Budimir
		Rasim Trnjanin
		Vladimir Šavija
		Zrinka Jemrić
	Petak u 15:30 - 17:0 (limit=10):
		Ajla Mustafić
		Anđelika Vučković
		Ena Alagic
		Fabio Pažin
		Ivna Ćavar
		Karlo Posavec
		Lea Patekar
		Lejla Reizbegović
		Paula Petrašić
		Petar Ivaniš

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Ana Jukić
		Andrej Škerjanc
		Anja Ljevar
		Antonela Kirin
		Božana Knežević
		Isabel Pavlović
		Ivan Frka Šeparović
		Ivan Orabović
		Magdalena Prskalo
		Marta Novak
		Matea Vidov
		Rene Križ
		Saira Omanovic
	Petak u 18:30 - 20:0 (limit=13):
		Alba Elezović
		Ana Pavlović
		Antonija Jug
		Debora Kanižaj
		Frano Visković
		Iva Barišić
		Katarina Grcic
		Marta Posavec
		Mia Škaljac
		Monika Vukić
		Paula Bašić
		Stanija Jovanovic
		blanka dermit

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Andrej Škerjanc
		Barbara Marušić
		Denis Abdihodžić
		Edina Memiši
		Isabel Pavlović
		Iva Pavičić
		Karlo Vrbanić
		Klara Gabriela Penić
		Marija Razum
		Matea Vidov
		Mihovil Škalic
		Milenko Paponja
		Moorea Kuvačić
		Nera Gržetić
		Saira Omanovic
		Tina Matoc

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Anamarija Vidovic
		Barbara Ovčarić
		Daria Vuk
		Dora Visković
		Dora istenić
		Katja Lukić
		Lea Imamović
		Lucija Ivanac
		Lucija Kovačić
		Lucija Kuštra
		Marija Buotić
		Matea Vučetić
		Mia Škaljac
		Mihaela Budimir
		Sara Jindra
	Petak u 18:0 - 19:0 (limit=15):
		Alessia Bučić
		Antea Cikojevic
		Božena Sorić
		Danica Cvjetkovic
		Helena Buljubašić
		Ivana Čavka
		Ivona Barić
		Jana Joksimović
		Katarina Lazar
		LUCIJA RAKO
		Luka Begonja
		Matea Bubalo
		Matija Žanetić
		Nora Salkičević
		Zinajda Šabić
	Petak u 19:0 - 20:0 (limit=14):
		Ana Radan
		Dora Arhanić
		Dorotea Bralo
		Erika Milovec
		Jurica Nekić
		Lucija Rakocija
		Lucija Ratkovski
		Maša Matić
		Nika Fantulin
		Nika Supina
		Petra Mihalić
		Stela Salopek
		Tijana Botić
		lucija hero

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Andreas Ivanovic
		Asja Jerlagic
		Barbara Čulin
		Ela Tomljanović
		Ida Kuštelega
		Jana Joksimović
		Karlo Posavec
		Marija Matijašević
		Marta Adam
		Martina Besek
		Mirna Draganja
		Nikolina Klarić
		Ninib malki
		Viktorija Perčinlić
	Petak u 15:30 - 17:0 (limit=14):
		Ana Domitrović
		Anja Antea Sokač
		Antonia Vranjković
		Doris Šimac
		Dunja Ilić
		Erika Milovec
		Jasmina Alendar
		Jure trogrlic
		Marija Oreč
		Matea Čupen
		Mihaela Rac
		Nejra Kubat
		Stela Salopek
		Tarik Tahirovic

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Admira Selimović
		Ana Maria Dekanić
		Antonija Jokić
		Božana Knežević
		Danijela Sabljo
		Harun Mašić
		Helena Vidović
		Katja Lukić
		Lucija Herceg
		Marina Gadža
		Mona Maarouf
		Monika Dragić
		Rebecca Beissmann
		Tadeja Blagec
		Urška Štajdohar
	Petak u 15:30 - 17:0 (limit=14):
		Alba Elezović
		Ana Božinović
		Ena Keser
		Eva Rožman
		Karla Miškić
		Marija Dolić
		Marko Ivančević
		Monika Vukić
		Nina Vovk
		Paula Bašić
		Renata Volf
		Stanija Jovanovic
		Teja Trontelj
		Zorana Mavija
	Subota u 14:0 - 15:30 (limit=14):
		Ajla Mustafić
		Ana Perica
		Domagoj Ivančić
		Ela Prajz
		Elizabeta Zorić
		Iva Ćorluka
		Ivna Ćavar
		Jure trogrlic
		Karla Jurković
		Karolina Boljkovac
		Katja Kovačević
		Lejla Reizbegović
		Matea Dželalija
		Stipe Ivanda
	Subota u 15:30 - 17:0 (limit=15):
		Amra Mahmutović
		Anđela Batur
		Darko Đukić
		Domen Tomič
		Dora Habek
		Filip Kašner
		Filip Kliček
		Liza Uršič
		Luciia Ribić
		Mladen Petkovic
		Nedžad Osmanović
		Nikola Matijevic
		Nikolina Mohorovic
		Sara Jakoliš
		Valentina Marasovic

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Ana Čeko
		Andrija Segin
		Grgo Orlović
		Karlo Počanić
		Katarina Lelas
		Luka Paunović
		Marija Razum
		Mario Gržančić
		Marta Leović
		Marta Posavec
		Mihovil Škalic
		Nikolina Dernaj
		Tadeja Blagec
	Petak u 18:0 - 19:0 (limit=16):
		Eva Rožman
		Klara Gabriela Penić
		Marija Dolić
		Valentina Šalković
	Petak u 19:0 - 20:0 (limit=16):
		Dora Gobin
		Ivan Orabović

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Ana Perica
		Andrija Segin
		Antonija Jerković
		Dora Alagic
		Elizabeta Zorić
		Ena Kulis
		Karla Jurković
		Lucija Ratkovski
		Margareta Strihic
		Nika Kovačević
		Sabra Čolić
		Tiffany Ocko
	Petak u 15:30 - 17:0 (limit=14):
		Ana Aračić
		Antonia Milas
		Bozana corluka
		Darko Đukić
		Dora Habek
		Filip Kliček
		Ida Kuštelega
		Iva Skvorcov
		Ivana Pehar
		Katarina Pagadur
		Klara Naka
		Lucija Šarić
		Mirna Draganja
		Nikolina Mohorovic
	Petak u 17:0 - 18:30 (limit=9):
		Adna Vlahovljak
		Ana Istenić
		Edina Memiši
		Luciia Ribić
		Magdalena Udovičić
		Marta Adam
		Milenko Paponja
		Moorea Kuvačić
		lucija hero

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Andrea Vrankić
		Božena Sorić
		Branimir Ninčević
		Dejan Prpos
		Elis Mikac
		Kristina Filipović
		Marina Jardas
		Petra Lukač
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Anamarija Vidovic
		Dora Visković
		Ivana Čavka
		Karla Roksandić
		Matea Vučetić
		Nejra Šapčanin
	Petak u 15:30 - 17:0 (limit=6):
		Alessia Bučić
		Antea Cikojevic
		Katarina Lazar
		Milan Tejic
		Zinajda Šabić
		blanka dermit

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Denis Abdihodžić
		Ena Popović
		Fran Glanz
		Iva Pavičić
		Lucija Kovačić
		Lucija Kuštra
		Matea Bubalo
	Subota u 15:30 - 17:0 (limit=8):
		Fabio Pažin
		Karla Bikić
		Luka Paunović
		Magdalena Jurisic
		Marta Novak
		Mihaela Rac
		Nikolina Akmadža
		Urška Štajdohar

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Ante Zelenika
		Barbara Čulin
		Emina Dzaferović
		Iva Ćorluka
		Nedžad Osmanović
		Nikolina Akmadža
		Paula Vršić
		Petra Mihalić
		Stela Cerovac
		Tiffany Ocko
	Petak u 18:0 - 19:0 (limit=10):
		Anđela Batur
		Dejan Prpos
		Inga Jukanović
		Karlo Vrbanić
		Katja Kovačević
		Lea Patekar
		Marta Furdi
		Petra Lukač
		Tina Matoc
	Petak u 19:0 - 20:0 (limit=12):
		Dora Alagic
		Elis Mikac
		Ena Roca
		Filip Kašner
		Fran Glanz
		Josip Orešković
		Kristina Filipović
		Lucia Levar
		Marta Leović
		Mia Maretić
		Mihaela Hercigonja
		Nikolina Dernaj
	Subota u 17:0 - 18:0 (limit=12):
		Anja Antea Sokač
		Branimir Ninčević
		Ena Popović
		Lea Grzela
		Marta Smok
		Svetlana Galić
		Tessa Anastasja Stankic
	Subota u 18:0 - 19:0 (limit=10):
		Katarina Lasan
		Nikolina Nazor
		Sara Krog
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Radan
		Anamaria Mazarekić
		Darko Tevanovic
		Dejana Komlen
		Iva Barišić
		Katarina Grcic
		LUCIJA RAKO
		Marija Mastelić
		Marijeta Dadić
		Nika Horvat
		Ninib malki
		Sara Kožul
		Valentina Šalković

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Bašić
		Ajla Brigić
		Iva Kordić
		Karolina Boljkovac
		Laura Plančak
		Lucija Rakocija
		Marija Kelić
		Sara El-Sabeh
		Tina Robič
		Valentina Marasovic
		Višnja Šljivac

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Ana Aračić
		Antonija Jokić
		Dunja Ilić
		Jelena Đapić
		Karla Miškić
		Marija Mastelić
		Mirna Radočaj
		Monika Dragić
		Sara El-Sabeh

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Daria Vuk
		Harun Mašić
		Karla Roksandić
		Katarina Lasan
		Luka Begonja
		Marija Matijašević
		Matija Žanetić
		Milan Tejic
		Nikolina Nazor
		Sara Jindra
		Sara Kožul
	Subota u 17:0 - 18:30 (limit=11):
		Ana Domitrović
		Doris Šimac
		Ena Roca
		Inga Jukanović
		Josipa Rokov
		Matea Čupen
		Mona Maarouf
		Nera Gržetić
		Nika Fantulin
		Nikolina Klarić
		Tarik Tahirovic
	Subota u 18:30 - 20:0 (limit=10):
		Dora Arhanić
		Jurica Nekić
		Karla Bikić
		Karla Sabolić
		Lea Grzela
		Marko Ivančević
		Nejra Kubat
		Nikola Matijevic
		Teja Trontelj
		Vladimir Šavija

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Ana Božinović
		Anamaria Mazarekić
		Anđelika Vučković
		Frano Visković
		Helena Buljubašić
		Iva Skvorcov
		Klara Naka
		Mia Maretić
		Nika Kovačević
		Petar Ivaniš
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Bašić
		Andreas Ivanovic
		Anja Ljevar
		Asja Jerlagic
		Emina Dzaferović
		Ena Keser
		Katarina Lelas
		Lea Imamović
		Marija Buotić
		Nina Vovk
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Bozana corluka
		Domen Tomič
		Ela Prajz
		Eva Krizmanić
		Grgo Orlović
		Ivan Frka Šeparović
		Katarina Pagadur
		Klaudija Kaurić
		Marta Furdi
		Matea Dželalija
		Mirna Radočaj
		Paula Vršić
		Tessa Anastasja Stankic

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Ana Jukić
		Antonia Vranjković
		Barbara Marušić
		Darko Tevanovic
		Ena Kulis
		Magdalena Prskalo
		Nora Salkičević
		Paula Petrašić
		Stipe Ivanda
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Ana Maria Dekanić
		Antonela Kirin
		Antonia Milas
		Danijela Sabljo
		Ena Alagic
		Josip Orešković
		Karla Sabolić
		Lucija Ivanac
		Lucija Šarić
		Marija Oreč
		Nika Horvat
		Renata Volf
		Rene Križ
		Svetlana Galić
		Viktorija Perčinlić
	Subota u 15:30 - 17:0 (limit=16):
		Adis Trešnjo
		Adna Vlahovljak
		Ines Perić
		Iva Kordić
		Ivana Pehar
		Karlo Počanić
		Marijeta Dadić
		Sara Krog
		Tina Robič
	Subota u 17:0 - 18:30 (limit=16):
		Fran Popić

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ana Brajdić
		Andrea Vrankić
		Dora Gobin
		Fran Popić
		Ines Perić
		Iva Plazonić
		Karla Ugrin
		Maša Matić
	Petak u 18:30 - 20:0 (limit=14):
		Ante Zelenika
		Laura Plančak

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Ajla Brigić
		Lucija Herceg
		Mario Gržančić
		Mihaela Hercigonja
		Zrinka Jemrić
	Subota u 15:30 - 17:0 (limit=6):
		Admira Selimović
		Ela Tomljanović
		Helena Vidović
		Marina Gadža
		Martina Besek
		Rasim Trnjanin

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Amila Balić
		Ana Barać
		Dejana Komlen
		Elia Croner
		Eva Krizmanić
		Josipa Rokov
		Marta Smok
		Nejra Šapčanin
		Nika Supina
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Amila Balić
		Ana Barać
		Debora Kanižaj
		Elia Croner
		Iva Plazonić
		Klaudija Kaurić
		Lucia Levar
		Marija Kelić
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Brajdić
		Barbara Ovčarić
		Domagoj Ivančić
		Jasmina Alendar
		Magdalena Jurisic
		Margareta Strihic
		Mateja Novački
		Stela Cerovac
	Subota u 19:0 - 20:0 (limit=10):
		Ana Istenić
		Ana Čeko
		Antonija Jerković
		Ivona Barić
		Jelena Đapić
		Karla Ugrin
		Liza Uršič
		Marina Jardas
		Mladen Petkovic
		Sara Jakoliš
