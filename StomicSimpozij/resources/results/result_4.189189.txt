
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Antonija Jug
		Božena Sorić
		Dora istenić
		Dorotea Bralo
		Ena Alagic
		Fabio Pažin
		Marina Jardas
		Mladen Petkovic
		Rasim Trnjanin
		lucija hero
	Petak u 15:30 - 17:0 (limit=10):
		Edina Memiši
		Jasmina Alendar
		Karlo Posavec
		Karlo Vrbanić
		Marija Oreč
		Mihaela Budimir
		Mihaela Hercigonja
		Nika Fantulin
		Sara Jindra
		Tarik Tahirovic

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Antonija Jug
		Ela Tomljanović
		Iva Barišić
		Ivan Orabović
		Jelena Đapić
		Karlo Počanić
		Marta Furdi
		Marta Posavec
		Matea Vidov
		Nika Horvat
		Nikolina Mohorovic
		Saira Omanovic
		Tina Matoc
	Petak u 18:30 - 20:0 (limit=13):
		Ana Pavlović
		Božana Knežević
		Dora Gobin
		Isabel Pavlović
		Ivona Barić
		Karlo Vrbanić
		Lucia Levar
		Magdalena Udovičić
		Margareta Strihic
		Marija Mastelić
		Ninib malki
		Nora Salkičević
		Valentina Šalković

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Ana Domitrović
		Ana Čeko
		Barbara Marušić
		Grgo Orlović
		Iva Pavičić
		Ivan Orabović
		Ivana Pehar
		Karlo Počanić
		Klara Gabriela Penić
		Klaudija Kaurić
		Lucija Šarić
		Milenko Paponja
		Moorea Kuvačić
		Nika Horvat
		Saira Omanovic
		Tina Matoc

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Andrea Vrankić
		Antonija Jerković
		Danica Cvjetkovic
		Dora istenić
		Jana Joksimović
		Jurica Nekić
		Katarina Grcic
		Katja Lukić
		Lea Patekar
		Matija Žanetić
		Nika Supina
		Stela Salopek
		Tiffany Ocko
		Zinajda Šabić
		lucija hero
	Petak u 18:0 - 19:0 (limit=15):
		Ana Aračić
		Božena Sorić
		Dora Habek
		Dorotea Bralo
		Ena Alagic
		Ena Kulis
		Liza Uršič
		Lucija Ratkovski
		Luka Begonja
		Marija Buotić
		Marta Leović
		Mia Škaljac
		Nejra Šapčanin
		Paula Petrašić
		Tijana Botić
	Petak u 19:0 - 20:0 (limit=14):
		Anamarija Vidovic
		Andrija Segin
		Anđelika Vučković
		Barbara Ovčarić
		Dora Arhanić
		Helena Buljubašić
		Iva Barišić
		Jelena Đapić
		LUCIJA RAKO
		Laura Plančak
		Lucija Rakocija
		Maša Matić
		Mihaela Budimir
		Sara El-Sabeh

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Anja Ljevar
		Antonia Milas
		Ela Tomljanović
		Ena Popović
		Ida Kuštelega
		Iva Skvorcov
		Jasmina Alendar
		Karla Bikić
		Karla Jurković
		Marijeta Dadić
		Marta Adam
		Mihaela Rac
		Nejra Kubat
		Tarik Tahirovic
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Bašić
		Andreas Ivanovic
		Antonia Vranjković
		Asja Jerlagic
		Doris Šimac
		Dunja Ilić
		Fran Popić
		Ivan Frka Šeparović
		Jure trogrlic
		Laura Plančak
		Marija Matijašević
		Nikolina Dernaj
		Ninib malki
		Zrinka Jemrić

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Antonija Jokić
		Dora Habek
		Ela Prajz
		Elizabeta Zorić
		Harun Mašić
		Iva Kordić
		Iva Ćorluka
		Ivna Ćavar
		Katja Lukić
		Liza Uršič
		Lucija Herceg
		Marko Ivančević
		Nikolina Mohorovic
		Rebecca Beissmann
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Mustafić
		Ana Božinović
		Božana Knežević
		Danijela Sabljo
		Josip Orešković
		Karla Miškić
		Lejla Reizbegović
		Lucija Rakocija
		Monika Dragić
		Stipe Ivanda
		Teja Trontelj
		Tina Robič
		Urška Štajdohar
		Valentina Marasovic
	Subota u 14:0 - 15:30 (limit=14):
		Alba Elezović
		Domagoj Ivančić
		Helena Vidović
		Jure trogrlic
		Karolina Boljkovac
		Luciia Ribić
		Marina Gadža
		Mirna Radočaj
		Mona Maarouf
		Monika Vukić
		Paula Bašić
		Renata Volf
		Sara Krog
		Tadeja Blagec
	Subota u 15:30 - 17:0 (limit=15):
		Admira Selimović
		Ana Maria Dekanić
		Ana Perica
		Darko Đukić
		Domen Tomič
		Ena Keser
		Eva Rožman
		Filip Kašner
		Filip Kliček
		Marija Dolić
		Matea Dželalija
		Matea Čupen
		Nedžad Osmanović
		Petra Lukač
		Sara Jakoliš

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Antonela Kirin
		Katarina Lelas
		Klara Gabriela Penić
		Magdalena Udovičić
		Mario Gržančić
		Marta Novak
		Mateja Novački
		Mihovil Škalic
		Milan Tejic
		Monika Vukić
		Nera Gržetić
		Nikolina Klarić
	Petak u 18:0 - 19:0 (limit=16):
		Luka Paunović
		Marija Razum
		Tadeja Blagec
		blanka dermit
	Petak u 19:0 - 20:0 (limit=16):
		Lea Imamović
		Marta Posavec
		Matea Vidov
		Matija Žanetić
		Sara Kožul

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Adna Vlahovljak
		Alessia Bučić
		Ana Aračić
		Bozana corluka
		Ena Kulis
		Frano Visković
		Katarina Pagadur
		Lucija Ivanac
		Magdalena Prskalo
		Margareta Strihic
		Nera Gržetić
		Nika Kovačević
	Petak u 15:30 - 17:0 (limit=14):
		Amila Balić
		Ana Istenić
		Andrija Segin
		Antonia Milas
		Debora Kanižaj
		Filip Kašner
		Ida Kuštelega
		Klara Naka
		Marina Gadža
		Marta Adam
		Matea Bubalo
		Mirna Draganja
		Sara Krog
		Viktorija Perčinlić
	Petak u 17:0 - 18:30 (limit=9):
		Ana Domitrović
		Darko Đukić
		Elizabeta Zorić
		Eva Rožman
		Iva Skvorcov
		Marija Dolić
		Milenko Paponja
		Moorea Kuvačić
		Sara Kožul

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Ana Maria Dekanić
		Branimir Ninčević
		Domen Tomič
		Elis Mikac
		Katja Kovačević
		Lea Patekar
		Tessa Anastasja Stankic
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Antea Cikojevic
		Antonija Jerković
		Lucija Ratkovski
		Milan Tejic
		Nikolina Akmadža
		Zinajda Šabić
	Petak u 15:30 - 17:0 (limit=6):
		Amra Mahmutović
		Dora Alagic
		Karla Roksandić
		Karla Sabolić
		Luka Paunović
		Mia Škaljac

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ana Barać
		Ana Čeko
		Ena Popović
		Iva Pavičić
		Ivna Ćavar
		Klaudija Kaurić
		Marta Furdi
	Subota u 15:30 - 17:0 (limit=8):
		Andrej Škerjanc
		Anđela Batur
		Josip Orešković
		Lucia Levar
		Lucija Kuštra
		Matea Vučetić
		Urška Štajdohar
		Valentina Marasovic

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Alba Elezović
		Andrej Škerjanc
		Anđela Batur
		Dejan Prpos
		Fran Glanz
		Lea Grzela
		Marta Smok
		Paula Vršić
		Stela Cerovac
		Tessa Anastasja Stankic
	Petak u 18:0 - 19:0 (limit=10):
		Denis Abdihodžić
		Dora Visković
		Emina Dzaferović
		Klara Naka
		Kristina Filipović
		Lucija Kuštra
		Matea Dželalija
		Mirna Draganja
		Nedžad Osmanović
		Petra Lukač
	Petak u 19:0 - 20:0 (limit=12):
		Ajla Mustafić
		Ana Perica
		Ante Zelenika
		Barbara Čulin
		Branimir Ninčević
		Dora Alagic
		Katarina Lazar
		Nina Vovk
		Petar Ivaniš
		Petra Mihalić
		Svetlana Galić
		Tiffany Ocko
	Subota u 17:0 - 18:0 (limit=12):
		Ana Radan
		Antea Cikojevic
		Inga Jukanović
		Marta Leović
		Nejra Šapčanin
		Nikolina Akmadža
	Subota u 18:0 - 19:0 (limit=10):
		Daria Vuk
		Katja Kovačević
		Nikolina Nazor
	Subota u 19:0 - 20:0 (limit=10):

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Anamaria Mazarekić
		Anja Antea Sokač
		Darko Tevanovic
		Dejana Komlen
		Dora Visković
		Erika Milovec
		Katarina Lasan
		Lucija Kovačić
		Marija Razum
		Marijeta Dadić
		Mona Maarouf
		Petra Mihalić
		Valentina Šalković

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Admira Selimović
		Ajla Brigić
		Ana Jukić
		Barbara Čulin
		Eva Krizmanić
		Inga Jukanović
		Ivana Čavka
		Karolina Boljkovac
		LUCIJA RAKO
		Martina Besek
		Matea Čupen
		Petar Ivaniš
		Sara El-Sabeh

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Ana Brajdić
		Dunja Ilić
		Edina Memiši
		Ela Prajz
		Karla Miškić
		Martina Besek
		Monika Dragić
		Višnja Šljivac
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Andrea Vrankić
		Anja Antea Sokač
		Bozana corluka
		Daria Vuk
		Harun Mašić
		Jurica Nekić
		Karla Roksandić
		Luka Begonja
		Marko Ivančević
		Nikolina Nazor
		Teja Trontelj
	Subota u 17:0 - 18:30 (limit=11):
		Anamarija Vidovic
		Dora Arhanić
		Doris Šimac
		Ena Roca
		Grgo Orlović
		Iva Ćorluka
		Katarina Lasan
		Lejla Reizbegović
		Luciia Ribić
		Nikola Matijevic
		Paula Petrašić
	Subota u 18:30 - 20:0 (limit=10):
		Adna Vlahovljak
		Darko Tevanovic
		Fran Popić
		Ivona Barić
		Mia Maretić
		Nika Fantulin
		Nikolina Klarić
		Sara Jindra
		Višnja Šljivac
		Vladimir Šavija

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Ana Božinović
		Anamaria Mazarekić
		Andreas Ivanovic
		Anđelika Vučković
		Dora Gobin
		Ena Roca
		Frano Visković
		Matea Bubalo
		Mia Maretić
		Nika Kovačević
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Bašić
		Anja Ljevar
		Dejan Prpos
		Eva Krizmanić
		Helena Buljubašić
		Ivan Frka Šeparović
		Katarina Lelas
		Marija Buotić
		Mihaela Hercigonja
		Nikolina Dernaj
		Tijana Botić
	Subota u 18:30 - 20:0 (limit=13):
		Asja Jerlagic
		Emina Dzaferović
		Erika Milovec
		Fran Glanz
		Helena Vidović
		Katarina Pagadur
		Lea Grzela
		Lea Imamović
		Matea Vučetić
		Mihovil Škalic
		Mirna Radočaj
		Nina Vovk
		Paula Vršić

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Alessia Bučić
		Antonia Vranjković
		Barbara Marušić
		Ines Perić
		Ivana Pehar
		Lucija Kovačić
		Magdalena Prskalo
		Marta Novak
		Nora Salkičević
		Stipe Ivanda

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Antonela Kirin
		Danica Cvjetkovic
		Danijela Sabljo
		Denis Abdihodžić
		Ines Perić
		Ivana Čavka
		Lucija Ivanac
		Lucija Šarić
		Marija Kelić
		Marija Mastelić
		Marija Matijašević
		Rene Križ
		Svetlana Galić
		Tina Robič
		Viktorija Perčinlić
		blanka dermit
	Subota u 15:30 - 17:0 (limit=16):
		Adis Trešnjo
		Ana Jukić
		Fabio Pažin
		Iva Kordić
		Karla Sabolić
		Marija Oreč
	Subota u 17:0 - 18:30 (limit=16):
		Josipa Rokov
		Renata Volf
		Stanija Jovanovic

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ante Zelenika
		Filip Kliček
		Iva Plazonić
		Karla Ugrin
		Katarina Lazar
		Maša Matić
	Petak u 18:30 - 20:0 (limit=14):
		Stela Salopek

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Antonija Jokić
		Karlo Posavec
		Nejra Kubat
		Rasim Trnjanin
		Stanija Jovanovic
	Subota u 15:30 - 17:0 (limit=6):
		Ajla Brigić
		Karla Bikić
		Karla Jurković
		Lucija Herceg
		Mihaela Rac
		Vladimir Šavija

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Dejana Komlen
		Elia Croner
		Jana Joksimović
		Josipa Rokov
		Magdalena Jurisic
		Marta Smok
		Nika Supina
		Nikola Matijevic
		Sabra Čolić
		Zorana Mavija
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Amila Balić
		Ana Barać
		Debora Kanižaj
		Isabel Pavlović
		Iva Plazonić
		Karla Ugrin
		Marija Kelić
		Mladen Petkovic
		Sabra Čolić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Brajdić
		Barbara Ovčarić
		Domagoj Ivančić
		Katarina Grcic
		Magdalena Jurisic
		Mateja Novački
		Paula Bašić
		Stela Cerovac
	Subota u 19:0 - 20:0 (limit=10):
		Ana Istenić
		Ana Radan
		Elia Croner
		Elis Mikac
		Ena Keser
		Kristina Filipović
		Marina Jardas
		Mario Gržančić
		Rene Križ
		Sara Jakoliš
