﻿#include "algorithm.h"
#include <util_string.h>
#include <boost/algorithm/string.hpp>


namespace stomic {
	void algorithm::load(const std::string& courses, const std::string& participants)
	{
		//Reads file to wstring because of utf8 chars
		std::wstring file = kki::string::read_utf8_file(courses.c_str());

		//Splits the courses file to strings where each represents one course
		boost::trim_right(file);
		std::vector<std::wstring> course_strings;
		boost::split(course_strings, file, boost::is_any_of("\n"));

		//Counter for the lecture id-s
		size_t lecture_id = 0;
		size_t max_course_id = 0;

		for (auto& course_string : course_strings)
		{
			//Splitting course string to substrings for additional info
			std::vector<std::wstring> course_;
			boost::split(course_, course_string, boost::is_any_of(" "));

			//Course name and id
			stomic::course c;
			c.id = std::stoi(course_[0]);
			c.name = course_[1];
			max_course_id = c.id > max_course_id ? c.id : max_course_id;

			//Input file has _ for spaces so, here they are replaced
			std::replace(c.name.begin(), c.name.end(), '_', ' ');

			//Parsing lectures
			//i = 2 because first and substr are course definition
			for (size_t i = 2; i < course_.size(); ++i)
			{
				//For easier use, referencing
				std::wstring& lec = course_[i];
				//Finding day, because variable len of first paramater 'limit'
				size_t pos = lec.find_first_of(L"PS");
				//Parsing day
				lecture::day d{ lec[pos] == L'P' ? lecture::day::friday : lecture::day::saturday };
				//Parsing limit
				size_t limit = std::stoi(lec.substr(0, pos));

				//Parsing time
				int sh = std::stoi(lec.substr(pos + 1, pos + 3));
				int sm = std::stoi(lec.substr(pos + 4, pos + 6));
				int eh = std::stoi(lec.substr(pos + 7, pos + 9));
				int em = std::stoi(lec.substr(pos + 10, pos + 12));
				lecture::time t{ sh, sm, eh, em };

				c.lectures.emplace_back(lecture_id);
				this->lectures.emplace_back(lecture_id++, c.id, d, t, limit);
			}

			//Adding course to vec
			this->courses.push_back(c);
		}

		file = kki::string::read_utf8_file(participants.c_str());
		boost::trim_right(file);
		std::vector<std::wstring> student_strings;
		boost::split(student_strings, file, boost::is_any_of("\n"));

		size_t student_id{ 0 };

		for (auto& student_string : student_strings)
		{
			//Parsing name
			size_t num_pos = student_string.find_first_of(L"0123456789");
			std::wstring name = student_string.substr(0, num_pos - 1);

			//Parsing priorities
			std::wstringstream iss(student_string.substr(num_pos));
			std::vector<size_t> numbers;
			numbers.reserve(16);
			int number;
			while (iss >> number)
			{
				if(std::find(numbers.begin(), numbers.end(), number) == numbers.end())
					numbers.push_back(number);
			}

			//Creating student
			student s;
			s.name = name;
			s.id = student_id++;
			s.priorities = numbers;
			//1000000 predstavlja beskonacnu gresku, tj, ti coursevi su jako nepozeljni
			s.priorities_error = std::vector<size_t>(max_course_id + 1, 0);
			for (size_t i = 0; i < s.priorities.size(); ++i)
			{
				s.priorities_error[s.priorities[i]] = i;
			}

			students.push_back(s);
		}
	}

	std::vector<layout_solution*> algorithm::generate_starting_population(size_t population_size)
	{
		//Result vector
		std::vector<layout_solution*> res;
		res.reserve(population_size);

		//Indexes of the students, we randomise them per solution so there is no bias for the first students
		std::vector<size_t> indexes;
		indexes.reserve(students.size());
		for (auto& s : students)
			indexes.push_back(s.id);

		size_t i = 0;
		while (i < population_size)
		{
			//Randomization of the indexes

			std::shuffle(indexes.begin(), indexes.end(), kki::rng::global.get_engine());
			//Creation of the solution

			//Initializing limits counters
			std::vector<size_t> limits(lectures.size());
			for (auto& lec : lectures)
				limits[lec.id] = lec.limit;

			//Vectors of people with no first or second courses
			std::vector<size_t> no_first;
			std::vector<size_t> no_second;

			//Needed for reasigning
			std::vector<std::vector<size_t>> participants_in_lectures(lectures.size());

			//Temporary solution, because of the second step of assigning
			std::vector<std::pair<size_t, size_t>> tmp_solution(students.size(), {-1, -1});


			//For every student we try to find a solution where he has only courses that he/she chose, 
			//by finding first courses that are not fully occupied
			//As this step is prone to not finding a good solution we need a afterstep which reassignes courses of other students
			for (auto index : indexes)
			{
				student& s = students[index];

				unsigned first{};
				unsigned first_id{};

				unsigned second{};
				//Looking for the first lecture
				for (auto priority : s.priorities)
				{
					course& c = *std::find_if(courses.begin(), courses.end(),
						[priority](const course& c) {return c.id == priority; });

					for (auto lecture : c.lectures)
					{
						if (limits[lecture] > 0)
						{
							--limits[lecture];
							first = lecture;
							first_id = c.id;

							participants_in_lectures[lecture].push_back(s.id);


							goto exit_first_loop;
						}
					}
				}

				no_first.emplace_back(s.id);

			exit_first_loop:;

				//Looking for the second lecture
				for (auto priority : s.priorities) {
					course& c = *std::find_if(courses.begin(), courses.end(),
						[priority](const course& c) {return c.id == priority; });
					if (c.id == first_id)
						continue;

					for (auto lecture : c.lectures)
					{

						if (limits[lecture] > 0 && lectures[lecture].no_collision(lectures[first]))
						{
							--limits[lecture];
							second = lecture;
							participants_in_lectures[lecture].push_back(s.id);

							goto exit_second_loop;
						}
					}
				}
				no_second.emplace_back(s.id);

			exit_second_loop:;

				//Adding to the solution
				tmp_solution[index] = { first, second };
			}

			//Reasigning step
			//Works like this, it first tries to assign courses to people that don't have a first course
			//The same procedure is repeated for the students that don't have a second course
			//Procedure:
			//	Go through the students courses one by one
			//		Go through the lectures of the courses
			//			Find if there are students that maybe want courses that are not full, with no collision
			//				If such a student exists, reassign the student to the new course, and give the course the new student
			//				
			//	Same procedure for the people with second courses missed, but there is an additional collision check

			bool found_solution = true;

			//For every unassigned student without the first course
			for(size_t unassigned_student_i : no_first)
			{
				size_t other_student{0};
				size_t chosen_lecture{0};
				size_t chosen_other_lecture{0};

				student& unassigned_student = students[unassigned_student_i];
				//For every wanted lecture of the unassigned student
				for(size_t course_ : unassigned_student.priorities)
				{
					course& c = *std::find_if(courses.begin(), courses.end(),
						[course_](const course& c) {return c.id == course_; });

					//For all the lectures of the given courses
					for(size_t lecture : c.lectures)
					{
						std::vector<size_t>& students_in_lecture = participants_in_lectures[lecture];

						//For all the students in the lecture
						for(size_t student_ : students_in_lecture)
						{
							//For all the courses they might want
							student& stud = students[student_];
							bool is_first_lecture = tmp_solution[student_].first == lecture;

							for(size_t course__ : stud.priorities)
							{
								course& c_ = *std::find_if(courses.begin(), courses.end(),
									[course__](const course& c) {return c.id == course__; });

								//For all the lectures of the course
								for(size_t secondary_lecture : c_.lectures)
								{
									//First check if this lecture is for the same course as the other one
									//	If it is not, it has to have a empty spot
									//		If it has an empty spot, check if there is a collision
									//			If there is no collision and there is a empty slot
									//				Remember:
									//					Goal course
									//					New course for the second student
									//					Second student
									//				Break the shit
									bool is_other_lecture = (is_first_lecture ? tmp_solution[student_].second : tmp_solution[student_].first) == secondary_lecture;

									if(!is_other_lecture){
										if(limits[secondary_lecture] > 0)
										{
											if(	lectures[secondary_lecture].course_id != 
												lectures[is_first_lecture ? tmp_solution[student_].second : tmp_solution[student_].first].course_id)
												{
												if (lectures[secondary_lecture].no_collision(lectures[is_first_lecture ? tmp_solution[student_].second : tmp_solution[student_].first]))
												{
													other_student = student_;
													chosen_other_lecture = secondary_lecture;
													chosen_lecture = lecture;

													goto out_of_hell;
												}
											}
										}
									}

								}
							}

						}


					}
				}

			out_of_hell:
				if (chosen_other_lecture == chosen_lecture)
				{
					found_solution = false;
					break;
				}
				else
				{
					//Replace in tmp solution for both students
					tmp_solution[unassigned_student_i].first = chosen_lecture;
					if(chosen_lecture == tmp_solution[other_student].first)
					{
						tmp_solution[other_student].first = chosen_other_lecture;
					}
					else
					{
						tmp_solution[other_student].second = chosen_other_lecture;
					}
					
					//Replace in participants in lecture
					//Remove the first student from the participants of that lecture
					participants_in_lectures[chosen_lecture]
						.erase(std::find(
							participants_in_lectures[chosen_lecture].begin(), 
							participants_in_lectures[chosen_lecture].end(), 
							other_student));
					//Add unassigned student to the lecture
					participants_in_lectures[chosen_lecture].push_back(unassigned_student_i);
					//Add other student
					participants_in_lectures[chosen_other_lecture].push_back(other_student);
					//Subtract from the new lecture because it has a new student
					limits[chosen_other_lecture]--;
				}
			}

			if (!found_solution)
			{
				continue;
			}

			//For every unassigned student without the second course
			for (size_t unassigned_student_i : no_second)
			{
				size_t other_student{ 0 };
				size_t chosen_lecture{ 0 };
				size_t chosen_other_lecture{ 0 };

				student& unassigned_student = students[unassigned_student_i];
				//For every wanted lecture of the unassigned student
				for (size_t course_ : unassigned_student.priorities)
				{
					course& c = *std::find_if(courses.begin(), courses.end(),
						[course_](const course& c) {return c.id == course_; });

					//For all the lectures of the given courses
					for (size_t lecture : c.lectures)
					{
						std::vector<size_t>& students_in_lecture = participants_in_lectures[lecture];

						//For all the students in the lecture
						for (size_t student_ : students_in_lecture)
						{
							//For all the courses they might want
							student& stud = students[student_];
							bool is_first_lecture = tmp_solution[student_].first == lecture;

							for (size_t course__ : stud.priorities)
							{
								course& c_ = *std::find_if(courses.begin(), courses.end(),
									[course__](const course& c) {return c.id == course__; });

								//For all the lectures of the course
								for (size_t secondary_lecture : c_.lectures)
								{
									//First check if this lecture is for the same course as the other one
									//	If it is not, it has to have a empty spot
									//		If it has an empty spot, check if there is a collision
									//			If there is no collision and there is a empty slot
									//				Remember:
									//					Goal course
									//					New course for the second student
									//					Second student
									//				Break the shit
									bool is_other_lecture = (is_first_lecture ? tmp_solution[student_].second : tmp_solution[student_].first) == secondary_lecture;

									if (!is_other_lecture) {
										if (limits[secondary_lecture] > 0)
										{
											if (lectures[secondary_lecture].course_id !=
												lectures[is_first_lecture ? tmp_solution[student_].second : tmp_solution[student_].first].course_id)
											{
												if (lectures[tmp_solution[unassigned_student_i].first].course_id !=
													lectures[lecture].course_id)
												{
													if (lectures[secondary_lecture].no_collision(lectures[is_first_lecture ? tmp_solution[student_].second : tmp_solution[student_].first]))
													{
														if (lectures[tmp_solution[unassigned_student_i].first].no_collision(lectures[lecture]))
														{
															other_student = student_;
															chosen_other_lecture = secondary_lecture;
															chosen_lecture = lecture;

															goto out_of_hell_v2;
														}
													}
												}
											}
										}
									}

								}
							}

						}


					}
				}

			out_of_hell_v2:
				if (chosen_other_lecture == chosen_lecture)
				{
					found_solution = false;
					break;
				}
				else
				{
					//Replace in tmp solution for both students
					tmp_solution[unassigned_student_i].second = chosen_lecture;
					if (chosen_lecture == tmp_solution[other_student].first)
					{
						tmp_solution[other_student].first = chosen_other_lecture;
					}
					else
					{
						tmp_solution[other_student].second = chosen_other_lecture;
					}

					//Replace in participants in lecture
					//Remove the first student from the participants of that lecture
					participants_in_lectures[chosen_lecture]
						.erase(std::find(
							participants_in_lectures[chosen_lecture].begin(),
							participants_in_lectures[chosen_lecture].end(),
							other_student));
					//Add unassigned student to the lecture
					participants_in_lectures[chosen_lecture].push_back(unassigned_student_i);
					//Add other student
					participants_in_lectures[chosen_other_lecture].push_back(other_student);
					//Subtract from the new lecture because it has a new student
					limits[chosen_other_lecture]--;
				}
			}

			if(!found_solution)
			{
				continue;
			}

			layout_solution* sol = new layout_solution(students.size());

			for(size_t index = 0; index < students.size(); ++index)
			{
				sol->priorities[index] = { index, {tmp_solution[index].first, tmp_solution[index].second }};
			}

			sol->attendence = limits;

			res.push_back(sol);
			i++;
		}

		for(auto& s : res)
		{
			s->assess_attendence(lectures);
		}

		return res;
	}

	bool algorithm::check_validity(layout_solution* solution)
	{
		std::vector<size_t> limits;
		limits.reserve(lectures.size());

		for(auto& lecture : lectures)
		{
			limits.push_back(lecture.limit);
		}

		int false_hits = 0;

		for(auto& sol_ : solution->priorities)
		{
			//Check if the student wants the lectures
			student& stud = students[sol_.id];

			if(lectures[sol_.lectures[0]].course_id == lectures[sol_.lectures[1]].course_id)
			{
				false_hits++;
			}

			for(auto& lec : sol_.lectures)
			{
				size_t course_id = lectures[lec].course_id;
				bool is_element = std::find_if(
					stud.priorities.begin(), 
					stud.priorities.end(), 
					[course_id](const size_t c) {return course_id == c; }) != stud.priorities.end();
				if(!is_element)
				{
					return false;
				}
			}
			//Check if the lectures overlap
			if(!lectures[sol_.lectures[0]].no_collision(lectures[sol_.lectures[1]]))
			{
				return false;
			}
			
			//Subtract the number of available places in the limits
			//Check the limits
			for (auto i : sol_.lectures)
			{
				if (limits[i] == 0)
				{
					return false;
				}
				limits[i]--;
			}
		}

		if(false_hits > 0)
		{
			return false;
		}

		return true;
	}

	algorithm::~algorithm()
	{
		for(auto& solution : population_)
		{
			delete solution;
		}
	}

	void stomic::algorithm::init(std::vector<layout_solution*> starting_population, unsigned population_size, evaluator<layout_solution>& eval,
		mutation<layout_solution>& mut, crossover<layout_solution>& cross, selection<layout_solution>& select, unsigned bests)
	{
		population_ = std::move(starting_population);
		population_size_ = population_size;
		eval_ = &eval;
		mut_ = &mut;
		cross_ = &cross;
		sel_ = &select;
		initialized_ = true;
		bests_ = bests;
		eval_->evaluate_generation(starting_population);
		best_solution_ = *std::max_element(population_.begin(), population_.end(), [](solution* s1, solution* s2) {return s1->fitness < s2->fitness; });
	}

	layout_solution* algorithm::get_solution()
	{
		return best_solution_;
	}

	void algorithm::run_iteration()
	{
		assert(initialized_);
		std::vector<layout_solution*> new_generation(population_size_);
		std::vector<layout_solution*> sorted_generation = population_;

		std::sort(sorted_generation.begin(), sorted_generation.end(), [](layout_solution* s1, layout_solution* s2) { return s1->fitness > s2->fitness; });

		for(size_t i = 0; i < bests_; ++i)
		{
			new_generation[i] = sorted_generation[i];
		}

		for (unsigned i = bests_; i < population_size_; ++i)
		{
			new_generation[i] = cross_->cross(sel_->select(population_), sel_->select(population_));
			mut_->mutate(new_generation[i]);
		}

		for (unsigned i = bests_; i < population_size_; ++i)
		{
			delete sorted_generation[i];
		}

		population_ = new_generation;
		eval_->evaluate_generation(population_);

		best_solution_ = *std::max_element(population_.begin(), population_.end(), [](solution* s1, solution* s2) {return s1->fitness < s2->fitness; });
		new_generation.clear();
		++(this->iteration_);
	}

	void algorithm::to_file(const char* filename)
	{
		layout_solution& solution = *best_solution_;

		std::vector<std::vector<size_t>> participants_in_lectures(lectures.size());

		//Constructing list
		for(auto& sol : solution.priorities)
		{
			for(auto& single_sol : sol.lectures)
			{
				participants_in_lectures[single_sol].emplace_back(sol.id);
			}
		}

		//String for output
		std::wstring output_string;
		output_string.reserve(100000);

		for(auto& course : courses)
		{
			output_string += L"\r\n";
			output_string += course.name + L":\r\n";

			for(auto& lecture_id : course.lectures)
			{
				auto& lecture = lectures[lecture_id];

				const unsigned start_hour = lecture.lecture_time.start / 60;
				const unsigned start_minute = lecture.lecture_time.start % 60;
				const unsigned finish_hour = lecture.lecture_time.end / 60;
				const unsigned finish_minute = lecture.lecture_time.end % 60;

				output_string += L"\t";
				output_string += (lecture.lecture_day == lecture::day::friday ? L"Petak u " : L"Subota u ");
				output_string += std::to_wstring(start_hour) + L":" + std::to_wstring(start_minute) + L" - " + std::to_wstring(finish_hour) + L":" + std::to_wstring(finish_minute);
				output_string += L" (limit=";
				output_string += std::to_wstring(lecture.limit) + L"):\r\n";
				
				std::vector<std::wstring> names;
				for(auto& student_id : participants_in_lectures[lecture_id])
				{
					names.push_back(students[student_id].name);
				}

				std::sort(names.begin(), names.end());

				for(auto& name : names)
				{
					output_string += L"\t\t";
					output_string += name + L"\r\n";
				}
			}
		}

		std::ofstream file(filename, std::ios::binary);
		const std::string output_utf8_string = kki::string::wstring_to_utf8(output_string);
		file.write(output_utf8_string.c_str(), output_utf8_string.length());
	}


}


