﻿#pragma once
#include <string>
#include <vector>
#include <array>
#include <map>
#include <set>
#include <genetic_algorithm/general_ga.h>
#include <iostream>

namespace stomic
{


	///Presents a student
	///Each student has an ID, NAME and PRIORITIES (that are described inside)
	///Dealing with students is done using ID-s because faster processing
	struct student
	{
		student(size_t id, std::wstring name, std::vector<size_t> priorities, std::vector<size_t> priorities_error) 
			: id(id), name(name), priorities(priorities), priorities_error(priorities_error){}
		student() = default;

		size_t id;
		std::wstring name;
		//Because of the ease of use, and constant speed, the element on the n-th place indicates the priority of the n-th element
		//For example:
		//	Priorities
		//	Course 1, Course 4, Course 2, Course 3 -> 0, 3, 1, 2 -> 0 2 3 1
		std::vector<size_t> priorities_error;
		std::vector<size_t> priorities;
	};

	///Course name and id
	struct course
	{
		size_t id;
		std::vector<size_t> lectures;
		std::wstring name;
	};

	///Presents lecture given by time, student limit, and id
	struct lecture
	{
		enum class day : wchar_t
		{
			friday = L'P', saturday = L'S'
		};

		struct time
		{
			time(int start_h, int start_m, int end_h, int end_m) : start(start_h * 60 + start_m), end(end_h * 60 + end_m) {}

			int start;
			int end;

			bool no_collision(const time& other)
			{
				return this->end < other.start || this->start > other.end;
			}
		};

		bool no_collision(const lecture& other)
		{
			if (this->lecture_day == other.lecture_day)
				return lecture_time.no_collision(other.lecture_time);
			return true;
		}

		lecture(size_t id, size_t course_id, day lecture_day, time lecture_time, size_t limit)
			: id(id), course_id(course_id), lecture_day(lecture_day), lecture_time(lecture_time), limit(limit) {}

		size_t id;
		size_t course_id;
		day lecture_day;
		time lecture_time;
		size_t limit;
	};


	///The solution to the problem
	///Consists of a solution per student
	///Each student has 2 lectures it can attend
	struct layout_solution : solution
	{
		struct single_solution
		{
			size_t id{};
			std::array<size_t, 2> lectures{};
		};
		std::vector<single_solution> priorities{};
		std::vector<size_t> attendence;

		layout_solution(size_t size) : priorities(size) {}
		layout_solution(std::vector<single_solution>& priorities, std::vector<size_t>& attendence) : priorities(priorities), attendence(attendence){}

		void assess_attendence(std::vector<lecture>& lectures)
		{
			attendence.resize(lectures.size());
			for(size_t i = 0; i < lectures.size(); ++i)
			{
				attendence[i] = lectures[i].limit;
			}

			for(single_solution& solution : priorities)
			{
				for (auto& lec : solution.lectures)
					attendence[lec]--;
			}

		}

		virtual layout_solution* clone()
		{
			auto res = new layout_solution(priorities, attendence);
			res->fitness = res->value = 0;
			return res;
		}

	};

	//Genetic algorithm used for solving the problem
	class algorithm : public genetic_algorithm<stomic::layout_solution>
	{
	private:
		std::vector<stomic::layout_solution*> population_;
		unsigned population_size_;
		evaluator<stomic::layout_solution>* eval_;
		mutation<stomic::layout_solution>* mut_;
		crossover<stomic::layout_solution>* cross_;
		selection<stomic::layout_solution>* sel_;
		unsigned int bests_;

		stomic::layout_solution* best_solution_{ nullptr };

		bool initialized_ = false;

	public:

		algorithm(std::vector<stomic::layout_solution*> starting_population, unsigned population_size, evaluator<stomic::layout_solution>& eval,
			mutation<stomic::layout_solution>& mut, crossover<stomic::layout_solution>& cross, selection<stomic::layout_solution>& select, unsigned bests):
			population_(std::move(starting_population)),
			population_size_(population_size),
			eval_(&eval),
			mut_(&mut),
			cross_(&cross),
			sel_(&select),
			initialized_(true),
			bests_(bests)
		{
			eval_->evaluate_generation(starting_population);
			best_solution_ = *std::max_element(population_.begin(), population_.end(), [](solution* s1, solution* s2) {return s1->fitness < s2->fitness; });
		}

		algorithm() = default;

		~algorithm();


		void init(std::vector<stomic::layout_solution*> starting_population, unsigned population_size, evaluator<stomic::layout_solution> & eval,
			mutation<stomic::layout_solution> & mut, crossover<stomic::layout_solution> & cross, selection<stomic::layout_solution> & select, unsigned bests);

		stomic::layout_solution* get_solution() override;
		void run_iteration() override;

		///Loads the radionica.txt and sudionici.txt file to the algorithm
		void load(const std::string& courses, const std::string& participants);

		std::vector<layout_solution*> generate_starting_population(size_t population_size);

		//Checks validity of the solution
		bool check_validity(layout_solution* solution);

		void to_file(const char* filename);

		//List of students, every student entry has a id, name and list of priorities
		std::vector<student> students;
		//List of courses, every course contains name and Id which students reference in their priorities
		std::vector<course> courses;

		//Every course can have several lectures, every lecture has an Id, time-ID, participant limit and course which it explains
		std::vector<lecture> lectures;
	};


}