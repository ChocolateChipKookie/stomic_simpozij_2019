#pragma once
#include <genetic_algorithm/evaluator.h>
#include "../algorithm.h"

namespace stomic {

	class layout_evaluator : public evaluator<layout_solution>
	{
		algorithm& alg_;
	public:
		layout_evaluator(algorithm& alg) : alg_(alg) {}

		//Evaluates the given layout solution
		void evaluate(layout_solution* sol) override
		{
			//Error
			long long int total = 0;
			for (auto& solution_ : sol->priorities)
			{
				//Dereferencing for better readability
				student& student_ = alg_.students[solution_.id];
				//student_.priorities_error[alg_.lectures[solution_.lectures[0]].course_id]		- error
				//alg_.lectures[solution_.lectures[0]].course_id								- id of the course
				//solution_.lectures[0]															- id of the lecture given by the solution

				total += student_.priorities_error[alg_.lectures[solution_.lectures[0]].course_id];
				total += student_.priorities_error[alg_.lectures[solution_.lectures[1]].course_id];
			}
			sol->value = static_cast<double>(total) / static_cast<double>(sol->priorities.size());
			sol->fitness = -sol->value;
		}
	};

}