#pragma once
#include <genetic_algorithm/mutation.h>
#include "../algorithm.h"


namespace stomic
{
	class simple_mutation : public mutation<layout_solution>
	{
		algorithm& alg_;
	public:
		simple_mutation(algorithm& alg) : alg_(alg){}

		bool mutate(layout_solution* sol) override
		{
			//Choose random student
			student& s = kki::rng::global.random_element(alg_.students);

			//Choose any from priorities, which is not currently attended to
			auto& priorities = s.priorities;
			std::random_shuffle(priorities.begin(), priorities.end());

			//Lecture to change
			layout_solution::single_solution& single_sol = sol->priorities[s.id];
			bool first_lecture = kki::rng::global.random_bool();

			int new_lecture;

			//For all the possible courses
			for(auto& priority : priorities)
			{
				course & c = *std::find_if(
								alg_.courses.begin(), 
								alg_.courses.end(),
								[priority](const course& c) {return c.id == priority; });

				//Check all lectures
				for(auto& lecture : c.lectures)
				{
					//If there is place in that lecture
					if(sol->attendence[lecture] > 0)
					{
						//If the course is different from the other one
						if(alg_.lectures[single_sol.lectures[first_lecture ? 1 : 0]].course_id != c.id)
						{
							//If the new course will end up being a collision
							if (alg_.lectures[single_sol.lectures[first_lecture ? 1 : 0]].no_collision(alg_.lectures[lecture]))
							{
								new_lecture = lecture;
								goto loop_out;
							}
						}
					}
				}

			}
			//No free lectures found
			return false;
		loop_out:

			//Add one place to the old lecture
			++sol->attendence[single_sol.lectures[first_lecture ? 0 : 1]];
			//Remove one place from the new lecture
			--sol->attendence[new_lecture];

			single_sol.lectures[first_lecture ? 0 : 1] = new_lecture;

			return true;
		}
	};
	
	class best_mutation : public mutation<layout_solution>
	{
		algorithm& alg_;
	public:
		best_mutation(algorithm& alg) : alg_(alg){}

		//All same as the standard mutation, just doesn't access the elements randomly
		bool mutate(layout_solution* sol) override
		{
			//Choose random student
			student& s = kki::rng::global.random_element(alg_.students);

			//Choose any from priorities, which is not currently attended to
			auto& priorities = s.priorities;

			//Lecture to change
			layout_solution::single_solution& single_sol = sol->priorities[s.id];
			bool first_lecture = kki::rng::global.random_bool();

			int new_lecture;

			//For all the possible courses
			for (auto& priority : priorities)
			{
				course& c = *std::find_if(
					alg_.courses.begin(),
					alg_.courses.end(),
					[priority](const course& c) {return c.id == priority; });

				//Check all lectures
				for (auto& lecture : c.lectures)
				{
					//If there is place in that lecture
					if (sol->attendence[lecture] > 0)
					{
						//If the course is different from the other one
						if (alg_.lectures[single_sol.lectures[first_lecture ? 1 : 0]].course_id != c.id)
						{
							//If the new course will end up being a collision
							if (alg_.lectures[single_sol.lectures[first_lecture ? 1 : 0]].no_collision(alg_.lectures[lecture]))
							{
								new_lecture = lecture;
								goto loop_out;
							}
						}
					}
				}

			}
			//No free lectures found
			return false;
		loop_out:

			//Add one place to the old lecture
			++sol->attendence[single_sol.lectures[first_lecture ? 0 : 1]];
			//Remove one place from the new lecture
			--sol->attendence[new_lecture];

			single_sol.lectures[first_lecture ? 0 : 1] = new_lecture;

			return true;
		}
	};


	class multiple_mutation : public mutation<layout_solution>
	{
		unsigned mutations_min;
		unsigned mutations_max;
		simple_mutation simple_mut_;
		best_mutation best_mut_;

		const unsigned ratio = 100;
		unsigned best_mutation_ratio_;

	public:
		multiple_mutation(unsigned mutations_min, unsigned mutations_max, algorithm& alg, double best_mutation_ratio)
			: 
			mutations_min(mutations_min), 
			mutations_max(mutations_max), 
			simple_mut_(alg), 
			best_mut_(alg), 
			best_mutation_ratio_(static_cast<unsigned>(best_mutation_ratio * ratio)){}

		bool mutate(layout_solution* sol) override
		{
			unsigned i = 0;
			unsigned mutations = kki::rng::global.random_int(mutations_min, mutations_max);

			if(kki::rng::global.random_int(10) == 0)
			{
				mutations = kki::rng::global.random_int(mutations_min, mutations_max * 10);
			}

			while(i < mutations)
			{
				if(kki::rng::global.random_index(ratio) < best_mutation_ratio_)
				{
					if (best_mut_.mutate(sol))
						++i;
				}
				else
				{
					if (simple_mut_.mutate(sol))
						++i;
				}
			}
			return true;
		}
	};


}

