#include <genetic_algorithm/general_ga.h>
#include <genetic_algorithm/k_tournament_selection.h>
#include "algorithm.h"
#include "genetic_operators/layout_evaluator.h"
#include "genetic_operators/mutations.h"
#include <fstream>
#include <sstream>

unsigned repeats = 100;
unsigned min_iterations = 10000;
unsigned max_iterations = 100000;
unsigned max_stagnation = 5000;
unsigned min_mutations = 2;
unsigned max_mutations = 7;
double best_mutation_ratio = 0.3;
unsigned best_solutions_preserved = 15;

unsigned solutions_per_generation = 100;
unsigned selection_number = 3;

std::string courses_input{ "resources/radionice.txt" };
std::string participants_input{ "resources/sudionici.txt" };
std::string output_file{ "resources/results/" };


void load_arguments()
{
	std::ifstream file("config.txt");
	std::string  str;

	while(std::getline(file, str))
	{
		size_t pos = str.find('=');
		if(pos == std::string::npos)
			continue;

		std::string option = str.substr(0, pos);

		if (option == "repeats")
			repeats = std::stoi(str.substr(pos + 1));
		else if (option == "min_iterations")
			min_iterations = std::stoi(str.substr(pos + 1));
		else if (option == "max_iterations")
			max_iterations = std::stoi(str.substr(pos + 1));
		else if (option == "max_stagnation")
			max_stagnation = std::stoi(str.substr(pos + 1));
		else if (option == "min_mutations")
			min_mutations = std::stoi(str.substr(pos + 1));
		else if (option == "max_mutations")
			max_mutations = std::stoi(str.substr(pos + 1));
		else if (option == "best_mutation_ratio")
			best_mutation_ratio = std::stod(str.substr(pos + 1));
		else if (option == "best_solutions_preserved")
			best_solutions_preserved = std::stoi(str.substr(pos + 1));
		else if (option == "solutions_per_generation")
			solutions_per_generation = std::stoi(str.substr(pos + 1));
		else if (option == "selection_number")
			selection_number = std::stoi(str.substr(pos + 1));
		else if (option == "courses_input")
			courses_input = str.substr(pos + 1);
		else if (option == "participants_input")
			participants_input = str.substr(pos + 1);
		else if (option == "output_file")
			output_file = str.substr(pos + 1);
	}
}


int main(int argc, char* argv[])
{
	load_arguments();


	for(unsigned i = 0; i < repeats; ++i)
	{
		//Create genetic algorithm class
		//There sure is a way to recycle, but, as this happens only a small number of times, it is redundant
		stomic::algorithm ga;
		//Load the files
		ga.load(courses_input, participants_input);
		//Create starting population
		std::vector<stomic::layout_solution*> starting_generation = ga.generate_starting_population(solutions_per_generation);

		//Init evaluator, selection, crossover and mutation
		//Cant be moved to outside, as it needs the genetic algorithm class
		stomic::layout_evaluator eval(ga);
		k_tournament_selection<stomic::layout_solution> selection(selection_number);
		random_solution_crossover<stomic::layout_solution> crossover;
		eval.evaluate_generation(starting_generation);
		stomic::multiple_mutation mm(min_mutations, max_mutations, ga, best_mutation_ratio);

		//Initialize genetic algorithm
		ga.init(
			starting_generation,
			starting_generation.size(),
			eval,
			mm,
			crossover,
			selection,
			best_solutions_preserved);


		unsigned stagnation = 0;
		double prev_value = 0;
		unsigned iteration = 1;
		for (; iteration <= max_iterations; ++iteration)
		{
			ga.run_iteration();
			//char 8 == backspace
			std::cout << std::string(30, 8) << iteration << ". gen sol:" << ga.get_solution()->fitness;

			//Iterate at least min_iterations number of iterations
			//After that, if it is stagnating for more than max_stagnation number of iterations break
			if(ga.get_solution()->value == prev_value)
			{
				stagnation++;
				if(stagnation >= max_stagnation && min_iterations < iteration)
				{
					break;
				}
			}
			else
			{
				stagnation = 0;
				prev_value = ga.get_solution()->value;
			}

		}

		std::cout << std::string(30, 8) << i + 1 << " try: " << ga.get_solution()->fitness << " in " << iteration << " iterations" << std::endl;

		std::string filename{ output_file + "result_" };
		filename += std::to_string(ga.get_solution()->value) + ".txt";

		ga.to_file(filename.c_str());
	}
	return 0;
}
