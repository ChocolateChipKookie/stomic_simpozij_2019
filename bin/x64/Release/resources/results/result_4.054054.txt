
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Danica Cvjetkovic
		Ena Alagic
		Katarina Pagadur
		Lea Grzela
		Magdalena Udovičić
		Mateja Novački
		Mihaela Budimir
		Mladen Petkovic
		Rasim Trnjanin
		lucija hero
	Petak u 15:30 - 17:0 (limit=10):
		Admira Selimović
		Andrej Škerjanc
		Anđelika Vučković
		Edina Memiši
		Ivna Ćavar
		Jasmina Alendar
		Jurica Nekić
		Karla Ugrin
		Mario Gržančić
		Zrinka Jemrić

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Antonija Jug
		Fran Glanz
		Lucia Levar
		Margareta Strihic
		Marta Adam
		Marta Furdi
		Marta Posavec
		Matea Vidov
		Mihaela Rac
		Nika Horvat
		Rene Križ
		Saira Omanovic
		Valentina Šalković
	Petak u 18:30 - 20:0 (limit=13):
		Debora Kanižaj
		Ivan Orabović
		Katja Lukić
		Liza Uršič
		Marija Mastelić
		Mia Škaljac
		Monika Vukić
		Moorea Kuvačić
		Nikolina Mohorovic
		Paula Bašić
		Tina Matoc
		Valentina Marasovic
		blanka dermit

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Adna Vlahovljak
		Amila Balić
		Ana Jukić
		Ana Čeko
		Antonija Jug
		Iva Pavičić
		Ivana Pehar
		Jelena Đapić
		Karlo Počanić
		Klara Gabriela Penić
		Klara Naka
		Klaudija Kaurić
		Lucia Levar
		Magdalena Prskalo
		Mihovil Škalic
		Milenko Paponja

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Alessia Bučić
		Andrea Vrankić
		Barbara Ovčarić
		Danica Cvjetkovic
		Dora Arhanić
		Ivan Frka Šeparović
		Jana Joksimović
		Jelena Đapić
		Katarina Lazar
		Maša Matić
		Mia Škaljac
		Paula Vršić
		Tiffany Ocko
		Valentina Marasovic
		Zinajda Šabić
	Petak u 18:0 - 19:0 (limit=15):
		Antea Cikojevic
		Iva Barišić
		Iva Plazonić
		LUCIJA RAKO
		Lea Imamović
		Lucija Ivanac
		Lucija Kuštra
		Marija Buotić
		Matea Bubalo
		Matea Dželalija
		Mihaela Budimir
		Nika Fantulin
		Petra Mihalić
		Sara Jindra
		Sara Kožul
	Petak u 19:0 - 20:0 (limit=14):
		Ana Aračić
		Andrija Segin
		Dora Visković
		Helena Buljubašić
		Ivona Barić
		Katarina Grcic
		Lea Patekar
		Lucija Ratkovski
		Luka Paunović
		Marta Leović
		Mirna Radočaj
		Nika Supina
		Stela Salopek
		lucija hero

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Ana Domitrović
		Anamaria Mazarekić
		Anja Ljevar
		Antonia Vranjković
		Barbara Čulin
		Erika Milovec
		Karla Bikić
		Karla Jurković
		Karlo Posavec
		Katarina Lasan
		Marija Matijašević
		Mihaela Rac
		Nejra Kubat
		Ninib malki
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Bašić
		Anja Antea Sokač
		Ela Tomljanović
		Ena Popović
		Frano Visković
		Marijeta Dadić
		Martina Besek
		Matea Čupen
		Nikolina Dernaj
		Nikolina Klarić
		Paula Petrašić
		Tarik Tahirovic
		Tessa Anastasja Stankic
		Viktorija Perčinlić

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Ana Perica
		Božana Knežević
		Danijela Sabljo
		Domen Tomič
		Ena Keser
		Filip Kašner
		Marina Gadža
		Matea Dželalija
		Mona Maarouf
		Monika Dragić
		Nikola Matijevic
		Nina Vovk
		Rebecca Beissmann
		Stanija Jovanovic
		Zorana Mavija
	Petak u 15:30 - 17:0 (limit=14):
		Ana Božinović
		Darko Đukić
		Ela Prajz
		Filip Kliček
		Josip Orešković
		Katja Lukić
		Liza Uršič
		Luciia Ribić
		Nedžad Osmanović
		Nikolina Mohorovic
		Paula Bašić
		Renata Volf
		Sara Jakoliš
		Stipe Ivanda
	Subota u 14:0 - 15:30 (limit=14):
		Admira Selimović
		Eva Rožman
		Harun Mašić
		Helena Vidović
		Iva Kordić
		Iva Ćorluka
		Ivna Ćavar
		Jure trogrlic
		Marko Ivančević
		Matija Žanetić
		Monika Vukić
		Petra Lukač
		Teja Trontelj
		Urška Štajdohar
	Subota u 15:30 - 17:0 (limit=15):
		Ajla Mustafić
		Alba Elezović
		Antonija Jokić
		Domagoj Ivančić
		Dora Habek
		Elizabeta Zorić
		Karla Miškić
		Katja Kovačević
		Lejla Reizbegović
		Lucija Herceg
		Lucija Rakocija
		Luka Begonja
		Sara Krog
		Tadeja Blagec
		Tina Robič

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Adna Vlahovljak
		Ana Pavlović
		Antonela Kirin
		Ivan Orabović
		Katarina Lelas
		Lejla Reizbegović
		Luka Paunović
		Marija Razum
		Marta Novak
		Matea Vučetić
		Mihovil Škalic
		Tadeja Blagec
		Teja Trontelj
	Petak u 18:0 - 19:0 (limit=16):
		Grgo Orlović
		Klara Gabriela Penić
		Marija Dolić
		Mario Gržančić
		Mateja Novački
		Stanija Jovanovic
	Petak u 19:0 - 20:0 (limit=16):
		Marta Posavec
		Sara El-Sabeh
		Valentina Šalković

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Ana Aračić
		Andrija Segin
		Antonia Milas
		Bozana corluka
		Ena Kulis
		Eva Krizmanić
		Eva Rožman
		Margareta Strihic
		Marta Adam
		Moorea Kuvačić
		Nora Salkičević
		Sabra Čolić
	Petak u 15:30 - 17:0 (limit=14):
		Ana Istenić
		Daria Vuk
		Dora Alagic
		Dora istenić
		Fabio Pažin
		Iva Skvorcov
		Jure trogrlic
		Lucija Šarić
		Luka Begonja
		Marija Oreč
		Marta Leović
		Matija Žanetić
		Mirna Draganja
		Nera Gržetić
	Petak u 17:0 - 18:30 (limit=9):
		Ana Perica
		Elizabeta Zorić
		Filip Kašner
		Ida Kuštelega
		Klara Naka
		Magdalena Udovičić
		Milenko Paponja
		Nika Kovačević
		Sara Krog

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Ana Maria Dekanić
		Andrea Vrankić
		Branimir Ninčević
		Elis Mikac
		Marina Jardas
		Tijana Botić
		Višnja Šljivac
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Alessia Bučić
		Antea Cikojevic
		Karla Sabolić
		Katarina Lazar
		Laura Plančak
		Nikolina Akmadža
	Petak u 15:30 - 17:0 (limit=6):
		Amra Mahmutović
		Darko Tevanovic
		Dora Visković
		Iva Barišić
		Karla Roksandić
		Stela Salopek

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Domen Tomič
		Elia Croner
		Iva Pavičić
		Klaudija Kaurić
		Lucija Ivanac
		Marta Furdi
		Svetlana Galić
	Subota u 15:30 - 17:0 (limit=8):
		Ana Radan
		Anđela Batur
		Dejan Prpos
		Ena Popović
		Lucija Kuštra
		Matea Bubalo
		Matea Vidov
		blanka dermit

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Anđela Batur
		Barbara Čulin
		Ena Roca
		Iva Ćorluka
		Katja Kovačević
		Lea Grzela
		Marta Smok
		Nejra Šapčanin
		Petra Lukač
		Urška Štajdohar
	Petak u 18:0 - 19:0 (limit=10):
		Asja Jerlagic
		Božena Sorić
		Dejan Prpos
		Kristina Filipović
		Mihaela Hercigonja
		Mirna Draganja
		Nedžad Osmanović
		Nikolina Dernaj
		Tessa Anastasja Stankic
	Petak u 19:0 - 20:0 (limit=12):
		Ajla Mustafić
		Alba Elezović
		Ana Pavlović
		Ante Zelenika
		Daria Vuk
		Ena Kulis
		Karlo Vrbanić
		Lucija Herceg
		Marija Razum
		Marta Novak
		Stela Cerovac
		Tiffany Ocko
	Subota u 17:0 - 18:0 (limit=12):
		Denis Abdihodžić
		Dora Alagic
		Fran Glanz
		Nikolina Akmadža
		Petra Mihalić
		Svetlana Galić
		Zinajda Šabić
	Subota u 18:0 - 19:0 (limit=10):
	Subota u 19:0 - 20:0 (limit=10):
		Emina Dzaferović
		Inga Jukanović
		Nikolina Nazor

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Domitrović
		Ana Radan
		Anamarija Vidovic
		Dejana Komlen
		Dorotea Bralo
		Dunja Ilić
		Erika Milovec
		Iva Kordić
		Karla Bikić
		LUCIJA RAKO
		Lucija Kovačić
		Milan Tejic
		Ninib malki

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Brigić
		Antonia Milas
		Ivana Čavka
		Karla Miškić
		Karolina Boljkovac
		Laura Plančak
		Lucija Rakocija
		Sara El-Sabeh
		Tina Robič
		Višnja Šljivac

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Dunja Ilić
		Edina Memiši
		Ela Prajz
		Marija Mastelić
		Marijeta Dadić
		Martina Besek
		Mirna Radočaj
		Monika Dragić
		Sara Jakoliš

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Branimir Ninčević
		Darko Tevanovic
		Grgo Orlović
		Ivan Frka Šeparović
		Karla Roksandić
		Katarina Lasan
		Nera Gržetić
		Nikolina Klarić
		Paula Vršić
		Sara Jindra
		Sara Kožul
	Subota u 17:0 - 18:30 (limit=11):
		Anja Antea Sokač
		Antonija Jerković
		Inga Jukanović
		Ivona Barić
		Lea Patekar
		Luciia Ribić
		Matea Čupen
		Nikolina Nazor
		Paula Petrašić
		Tarik Tahirovic
		Vladimir Šavija
	Subota u 18:30 - 20:0 (limit=10):
		Anamarija Vidovic
		Bozana corluka
		Dora Arhanić
		Doris Šimac
		Filip Kliček
		Josipa Rokov
		Jurica Nekić
		Marko Ivančević
		Mia Maretić
		Petar Ivaniš

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Ana Božinović
		Andreas Ivanovic
		Antonia Vranjković
		Anđelika Vučković
		Dora Gobin
		Doris Šimac
		Josipa Rokov
		Mia Maretić
		Nika Kovačević
		Petar Ivaniš
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Ajla Bašić
		Asja Jerlagic
		Emina Dzaferović
		Helena Buljubašić
		Iva Plazonić
		Katarina Lelas
		Katarina Pagadur
		Lea Imamović
		Marija Buotić
		Nika Supina
		Nina Vovk
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Brigić
		Ana Čeko
		Andrej Škerjanc
		Anja Ljevar
		Božena Sorić
		Dora istenić
		Dorotea Bralo
		Eva Krizmanić
		Harun Mašić
		Helena Vidović
		Matea Vučetić
		Mihaela Hercigonja
		Tijana Botić

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Ida Kuštelega
		Ivana Pehar
		Karlo Posavec
		Lucija Kovačić
		Milan Tejic
		Nora Salkičević
		Stipe Ivanda
		Tina Matoc
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Adis Trešnjo
		Antonela Kirin
		Danijela Sabljo
		Denis Abdihodžić
		Ena Keser
		Fabio Pažin
		Ines Perić
		Karla Sabolić
		Karlo Počanić
		Lucija Šarić
		Magdalena Prskalo
		Marija Kelić
		Marija Matijašević
		Marija Oreč
		Renata Volf
		Rene Križ
	Subota u 15:30 - 17:0 (limit=16):
		Ana Jukić
		Ana Maria Dekanić
		Darko Đukić
		Ena Alagic
		Fran Popić
		Ivana Čavka
		Karlo Vrbanić
		Karolina Boljkovac
		Marija Dolić
		Nika Horvat
		Saira Omanovic
	Subota u 17:0 - 18:30 (limit=16):
		Josip Orešković
		Nika Fantulin

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Ana Brajdić
		Ante Zelenika
		Dora Habek
		Fran Popić
		Ines Perić
	Petak u 18:30 - 20:0 (limit=14):
		Dora Gobin
		Karla Ugrin
		Maša Matić

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Anamaria Mazarekić
		Frano Visković
		Iva Skvorcov
		Marina Gadža
		Vladimir Šavija
	Subota u 15:30 - 17:0 (limit=6):
		Božana Knežević
		Ela Tomljanović
		Karla Jurković
		Mona Maarouf
		Nejra Kubat
		Rasim Trnjanin

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Ana Barać
		Barbara Marušić
		Jana Joksimović
		Lucija Ratkovski
		Magdalena Jurisic
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Andreas Ivanovic
		Barbara Marušić
		Debora Kanižaj
		Dejana Komlen
		Isabel Pavlović
		Marija Kelić
		Mladen Petkovic
		Sabra Čolić
		Zrinka Jemrić

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Ana Istenić
		Barbara Ovčarić
		Ena Roca
		Jasmina Alendar
		Katarina Grcic
		Kristina Filipović
		Magdalena Jurisic
		Marina Jardas
	Subota u 19:0 - 20:0 (limit=10):
		Ana Barać
		Ana Brajdić
		Antonija Jerković
		Antonija Jokić
		Domagoj Ivančić
		Elia Croner
		Elis Mikac
		Isabel Pavlović
		Stela Cerovac
		Viktorija Perčinlić
