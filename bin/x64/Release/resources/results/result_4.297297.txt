
Dentalni implantati:
	Petak u 14:0 - 15:30 (limit=10):
		Andrej Škerjanc
		Danica Cvjetkovic
		Fabio Pažin
		Jasmina Alendar
		Katarina Pagadur
		Marina Jardas
		Mateja Novački
		Mihaela Budimir
		Mladen Petkovic
		Rasim Trnjanin
	Petak u 15:30 - 17:0 (limit=10):
		Ana Istenić
		Dunja Ilić
		Ena Alagic
		Jurica Nekić
		Magdalena Udovičić
		Moorea Kuvačić
		Nika Fantulin
		Saira Omanovic
		Vladimir Šavija
		Zrinka Jemrić

Izbjeljivanje zubi:
	Petak u 17:0 - 18:30 (limit=13):
		Andrej Škerjanc
		Antonija Jug
		Debora Kanižaj
		Dora Gobin
		Fran Glanz
		Isabel Pavlović
		Ivan Orabović
		LUCIJA RAKO
		Magdalena Prskalo
		Marija Kelić
		Marta Adam
		Marta Furdi
		Tina Matoc
	Petak u 18:30 - 20:0 (limit=13):
		Ana Pavlović
		Antonela Kirin
		Ivana Pehar
		Lucia Levar
		Margareta Strihic
		Marta Posavec
		Monika Vukić
		Moorea Kuvačić
		Nikolina Mohorovic
		Rene Križ
		Saira Omanovic
		Valentina Šalković
		blanka dermit

Otisni postupci:
	Petak u 14:0 - 16:0 (limit=16):
		Adna Vlahovljak
		Ana Barać
		Ana Jukić
		Antonija Jug
		Debora Kanižaj
		Grgo Orlović
		Isabel Pavlović
		Katarina Lelas
		Lucija Šarić
		Luka Paunović
		Marija Razum
		Marta Novak
		Mihovil Škalic
		Milenko Paponja
		Nera Gržetić
		Tina Matoc

Sportske udlage:
	Petak u 17:0 - 18:0 (limit=15):
		Antea Cikojevic
		Danica Cvjetkovic
		Dora istenić
		Fabio Pažin
		Jana Joksimović
		Lea Imamović
		Lucija Ivanac
		Luka Paunović
		Marta Leović
		Matea Bubalo
		Matea Vučetić
		Mihaela Budimir
		Nikolina Akmadža
		Nora Salkičević
		Sara Jindra
	Petak u 18:0 - 19:0 (limit=15):
		Alessia Bučić
		Andrija Segin
		Barbara Ovčarić
		Daria Vuk
		Dora Arhanić
		Dorotea Bralo
		Ena Alagic
		Ena Kulis
		Helena Buljubašić
		Iva Plazonić
		Matea Dželalija
		Nikolina Dernaj
		Sara Kožul
		Valentina Marasovic
		Višnja Šljivac
	Petak u 19:0 - 20:0 (limit=14):
		Anamarija Vidovic
		Anđelika Vučković
		Iva Barišić
		Ivona Barić
		Katarina Grcic
		Katja Lukić
		LUCIJA RAKO
		Lucija Kuštra
		Luka Begonja
		Matija Žanetić
		Maša Matić
		Paula Petrašić
		Paula Vršić
		Stela Salopek

Primjena PRF-a u dentalnoj medicini:
	Petak u 14:0 - 15:15 (limit=15):
		Amra Mahmutović
		Ana Istenić
		Barbara Čulin
		Ela Tomljanović
		Erika Milovec
		Eva Krizmanić
		Frano Visković
		Ida Kuštelega
		Jana Joksimović
		Karla Jurković
		Marija Matijašević
		Matea Čupen
		Nejra Kubat
		Ninib malki
		Petar Ivaniš
	Petak u 15:30 - 17:0 (limit=14):
		Ajla Bašić
		Anamaria Mazarekić
		Andreas Ivanovic
		Anja Ljevar
		Doris Šimac
		Elis Mikac
		Filip Kliček
		Iva Pavičić
		Jure trogrlic
		Mario Gržančić
		Martina Besek
		Nikolina Dernaj
		Svetlana Galić
		Tessa Anastasja Stankic

Forenzika u dentalnoj medicini:
	Petak u 14:0 - 15:30 (limit=15):
		Antonija Jokić
		Danijela Sabljo
		Helena Vidović
		Josip Orešković
		Katja Kovačević
		Matea Dželalija
		Mona Maarouf
		Monika Dragić
		Nedžad Osmanović
		Nina Vovk
		Rebecca Beissmann
		Sara Jakoliš
		Stanija Jovanovic
		Teja Trontelj
		Urška Štajdohar
	Petak u 15:30 - 17:0 (limit=14):
		Admira Selimović
		Ajla Mustafić
		Elizabeta Zorić
		Eva Rožman
		Harun Mašić
		Karla Miškić
		Karolina Boljkovac
		Katja Lukić
		Lucija Herceg
		Marija Dolić
		Marko Ivančević
		Monika Vukić
		Nikolina Mohorovic
		Stipe Ivanda
	Subota u 14:0 - 15:30 (limit=14):
		Ana Maria Dekanić
		Domagoj Ivančić
		Domen Tomič
		Ela Prajz
		Filip Kašner
		Iva Ćorluka
		Ivna Ćavar
		Jure trogrlic
		Luciia Ribić
		Renata Volf
		Tadeja Blagec
		Tina Robič
		Zorana Mavija
		lucija hero
	Subota u 15:30 - 17:0 (limit=15):
		Alba Elezović
		Amra Mahmutović
		Ana Božinović
		Ana Perica
		Anđela Batur
		Božana Knežević
		Dora Habek
		Iva Skvorcov
		Lejla Reizbegović
		Liza Uršič
		Lucija Rakocija
		Marina Gadža
		Nikolina Nazor
		Paula Bašić
		Zinajda Šabić

Određivanje boje:
	Petak u 17:0 - 18:0 (limit=15):
		Adis Trešnjo
		Ana Pavlović
		Ana Čeko
		Katarina Lelas
		Lejla Reizbegović
		Marija Razum
		Marta Posavec
		Mateja Novački
		Mihovil Škalic
		Milan Tejic
		Tadeja Blagec
		Tarik Tahirovic
		Valentina Šalković
	Petak u 18:0 - 19:0 (limit=16):
		Elizabeta Zorić
		Klara Gabriela Penić
		Magdalena Udovičić
		Marija Dolić
		Nikolina Klarić
	Petak u 19:0 - 20:0 (limit=16):
		Ivan Orabović
		Lea Imamović
		Matea Vidov
		Sara El-Sabeh

Estetska analiza i Digital Smile Design:
	Petak u 14:0 - 15:30 (limit=12):
		Amila Balić
		Andrija Segin
		Bozana corluka
		Darko Đukić
		Ena Kulis
		Klara Naka
		Lucija Ratkovski
		Margareta Strihic
		Marta Adam
		Mirna Draganja
		Sabra Čolić
		lucija hero
	Petak u 15:30 - 17:0 (limit=14):
		Antonija Jerković
		Dora Habek
		Edina Memiši
		Filip Kašner
		Ida Kuštelega
		Iva Skvorcov
		Karlo Posavec
		Marija Buotić
		Marina Gadža
		Mihaela Rac
		Nika Kovačević
		Nika Supina
		Tiffany Ocko
		Viktorija Perčinlić
	Petak u 17:0 - 18:30 (limit=9):
		Ana Aračić
		Ana Perica
		Božana Knežević
		Luciia Ribić
		Marija Oreč
		Matea Vidov
		Mia Škaljac
		Milenko Paponja
		Sara Krog

Fotodinamska terapija u parodontologiji:
	Petak u 14:0 - 15:30 (limit=13):
		Andrea Vrankić
		Božena Sorić
		Branimir Ninčević
		Luka Begonja
		Matea Vučetić
		Tijana Botić
	Petak u 15:30 - 17:0 (limit=15):

Estetski kompozitni ispun na prednjim zubima:
	Petak u 14:0 - 15:30 (limit=6):
		Alessia Bučić
		Antea Cikojevic
		Barbara Marušić
		Darko Tevanovic
		Dora Arhanić
		Milan Tejic
	Petak u 15:30 - 17:0 (limit=6):
		Anamarija Vidovic
		Denis Abdihodžić
		Iva Barišić
		Ivona Barić
		Stela Salopek
		blanka dermit

Kirurško šivanje:
	Subota u 14:0 - 15:30 (limit=7):
		Ana Barać
		Klaudija Kaurić
		Lucija Ivanac
		Marta Furdi
		Marta Novak
		Matea Bubalo
		Svetlana Galić
	Subota u 15:30 - 17:0 (limit=8):
		Ajla Mustafić
		Denis Abdihodžić
		Fran Glanz
		Iva Pavičić
		Josip Orešković
		Lucija Kuštra
		Magdalena Jurisic
		Valentina Marasovic

Biopsija uz osnovne šavove:
	Petak u 17:0 - 18:0 (limit=10):
		Anđela Batur
		Barbara Čulin
		Iva Ćorluka
		Katarina Lazar
		Kristina Filipović
		Lea Grzela
		Lucia Levar
		Marta Smok
		Mirna Draganja
		Stela Cerovac
	Petak u 18:0 - 19:0 (limit=10):
		Alba Elezović
		Anja Antea Sokač
		Asja Jerlagic
		Božena Sorić
		Dora Visković
		Inga Jukanović
		Lucija Kovačić
		Nikolina Nazor
		Tessa Anastasja Stankic
		Tiffany Ocko
	Petak u 19:0 - 20:0 (limit=12):
		Ana Aračić
		Branimir Ninčević
		Dejan Prpos
		Dora Alagic
		Ena Popović
		Karlo Vrbanić
		Mihaela Hercigonja
		Petar Ivaniš
		Petra Mihalić
		Sara Krog
		Urška Štajdohar
	Subota u 17:0 - 18:0 (limit=12):
		Ana Radan
		Ante Zelenika
		Emina Dzaferović
		Katarina Lasan
		Nejra Šapčanin
		Nikolina Akmadža
	Subota u 18:0 - 19:0 (limit=10):
		Antonia Milas
		Mia Maretić
	Subota u 19:0 - 20:0 (limit=10):
		Ena Roca
		Lea Patekar
		Petra Lukač

STAMP tehnika:
	Petak u 15:30 - 17:0 (limit=13):
		Ana Domitrović
		Anja Antea Sokač
		Dejana Komlen
		Dora Visković
		Karla Bikić
		Karla Roksandić
		Karla Sabolić
		Klara Gabriela Penić
		Laura Plančak
		Lucija Kovačić
		Marijeta Dadić
		Ninib malki
		Tina Robič

Hall tehnika:
	Petak u 17:0 - 18:0 (limit=14):
		Ajla Brigić
		Ena Popović
		Iva Kordić
		Ivana Čavka
		Katarina Lasan
		Lucija Rakocija
		Lucija Ratkovski
		Marija Mastelić
		Nika Horvat
		Petra Mihalić
		Sara El-Sabeh
		Zinajda Šabić

Traume zuba kod djece- važnost upotrebe splinta:
	Subota u 14:0 - 15:30 (limit=9):
		Ana Brajdić
		Antonija Jokić
		Jelena Đapić
		Karla Roksandić
		Marijeta Dadić
		Martina Besek
		Mirna Radočaj
		Monika Dragić
		Zrinka Jemrić

Brušenje s lupama i izrada silikonskog ključa:
	Subota u 15:30 - 17:0 (limit=11):
		Adna Vlahovljak
		Grgo Orlović
		Katja Kovačević
		Lea Grzela
		Lucija Šarić
		Marija Matijašević
		Marta Leović
		Matea Čupen
		Nera Gržetić
		Nikolina Klarić
		Sara Jindra
	Subota u 17:0 - 18:30 (limit=11):
		Daria Vuk
		Ena Roca
		Filip Kliček
		Lea Patekar
		Mario Gržančić
		Nikola Matijevic
		Paula Vršić
		Petra Lukač
		Sara Kožul
		Teja Trontelj
		Višnja Šljivac
	Subota u 18:30 - 20:0 (limit=10):
		Doris Šimac
		Harun Mašić
		Jurica Nekić
		Karolina Boljkovac
		Katarina Grcic
		Marko Ivančević
		Matija Žanetić
		Nika Fantulin
		Paula Petrašić
		Vladimir Šavija

Adhezivno cementiranje:
	Subota u 14:0 - 15:30 (limit=11):
		Anamaria Mazarekić
		Andreas Ivanovic
		Antonia Vranjković
		Anđelika Vučković
		Dora Gobin
		Eva Rožman
		Karlo Posavec
		Katarina Lazar
		Laura Plančak
		Nika Kovačević
		Rebecca Beissmann

Parodontološko šivanje:
	Subota u 17:0 - 18:30 (limit=11):
		Admira Selimović
		Ajla Bašić
		Anja Ljevar
		Asja Jerlagic
		Eva Krizmanić
		Helena Buljubašić
		Iva Plazonić
		Katarina Pagadur
		Lucija Herceg
		Nina Vovk
		Rasim Trnjanin
	Subota u 18:30 - 20:0 (limit=13):
		Ajla Brigić
		Ana Čeko
		Bozana corluka
		Domen Tomič
		Dora Alagic
		Dorotea Bralo
		Ela Prajz
		Inga Jukanović
		Ivna Ćavar
		Marija Buotić
		Mirna Radočaj
		Nika Supina
		Tijana Botić

CAD/CAM:
	Subota u 17:0 - 18:30 (limit=10):
		Amila Balić
		Ana Jukić
		Antonia Vranjković
		Barbara Marušić
		Ivana Pehar
		Klara Naka
		Magdalena Prskalo
		Nora Salkičević
		Stipe Ivanda
		Zorana Mavija

CBCT:
	Subota u 14:0 - 15:30 (limit=16):
		Adis Trešnjo
		Antonela Kirin
		Danijela Sabljo
		Ena Keser
		Ivan Frka Šeparović
		Ivana Čavka
		Jasmina Alendar
		Karla Bikić
		Karla Sabolić
		Karlo Počanić
		Karlo Vrbanić
		Marija Kelić
		Mia Škaljac
		Mihaela Rac
		Nika Horvat
		Rene Križ
	Subota u 15:30 - 17:0 (limit=16):
		Antonia Milas
		Dora istenić
		Dunja Ilić
		Fran Popić
		Ines Perić
		Iva Kordić
		Marija Mastelić
		Marija Oreč
		Mia Maretić
		Stanija Jovanovic
		Tarik Tahirovic
		Viktorija Perčinlić
	Subota u 17:0 - 18:30 (limit=16):
		Ana Maria Dekanić
		Josipa Rokov
		Renata Volf

Kako dijagnosticirati parodontološkog pacijenta?:
	Petak u 17:0 - 18:30 (limit=15):
		Andrea Vrankić
		Ante Zelenika
		Dejan Prpos
		Emina Dzaferović
		Fran Popić
		Ines Perić
		Karla Ugrin
		Maša Matić
		Paula Bašić
	Petak u 18:30 - 20:0 (limit=14):
		Karla Miškić

Strojna endodoncija:
	Subota u 14:0 - 15:30 (limit=5):
		Ana Domitrović
		Erika Milovec
		Karla Jurković
		Mihaela Hercigonja
		Mona Maarouf
	Subota u 15:30 - 17:0 (limit=6):
		Ela Tomljanović
		Frano Visković
		Helena Vidović
		Marina Jardas
		Nedžad Osmanović
		Nejra Kubat

iTOP:
	Subota u 14:0 - 15:30 (limit=30):
		Dejana Komlen
		Elia Croner
		Josipa Rokov
		Marta Smok
		Nejra Šapčanin
		Nikola Matijevic
		Sabra Čolić
	Subota u 15:30 - 17:0 (limit=29):

Spriječavaju li zaista fluoridni preparati demineralizaciju cakline?:
	Subota u 17:0 - 18:0 (limit=13):
		Ivan Frka Šeparović
		Karla Ugrin
		Klaudija Kaurić
		Mladen Petkovic
		Sara Jakoliš

Primjena lijekova u ordinaciji dentalne medicine:
	Subota u 18:0 - 19:0 (limit=8):
		Antonija Jerković
		Barbara Ovčarić
		Darko Đukić
		Elis Mikac
		Jelena Đapić
		Kristina Filipović
		Magdalena Jurisic
		Stela Cerovac
	Subota u 19:0 - 20:0 (limit=10):
		Ana Božinović
		Ana Brajdić
		Ana Radan
		Darko Tevanovic
		Domagoj Ivančić
		Edina Memiši
		Elia Croner
		Ena Keser
		Karlo Počanić
		Liza Uršič
